<?php
/* @var $this GroupContractController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Group Contracts',
);

$this->menu=array(
	array('label'=>'Create GroupContract', 'url'=>array('create')),
	array('label'=>'Manage GroupContract', 'url'=>array('admin')),
);
?>

<h1>Group Contracts</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
