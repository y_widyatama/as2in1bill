<?php
/* @var $this GroupController */
/* @var $model Group */

$this->breadcrumbs=array(
	'Groups'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List Group', 'url'=>array('index')),
	array('label'=>'Create Group', 'url'=>array('create')),
	array('label'=>'Update Group', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Group', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Group', 'url'=>array('admin')),
);
?>

<h1>View Group #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
	),
)); ?>
<br/>
<h3> Customer Group </h3>
<?php
$custGroup = new CustGroup('search');
$custGroup->unsetAttributes();  // clear any default values
$custGroup->group_id = $model->id;
$this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'cust-group-grid',
    'dataProvider'=>$custGroup->search(),
//    'filter'=>$custGroup,
    'columns'=>array(
//        'id',
        'cust.custnumber',
//        'group_id',
        'startperiod',
        'endperiod',
//        array(
//            'class'=>'CButtonColumn',
//        ),
    ),
)); ?>
<br/>
<h3> Group Contract </h3>
<?php

$groupContract = new GroupContract('search');
$groupContract->unsetAttributes();  // clear any default values
$groupContract->group_id = $model->id;
$this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'group-contract-grid',
    'dataProvider'=>$groupContract->search(),
//    'filter'=>$groupContract,
    'columns'=>array(
//        'id',
//        'group_id',
        'minimum_usage',
        'startperiod',
        'endperiod',
        'initial_grace',
//        array(
//            'class'=>'CButtonColumn',
//        ),
    ),
)); ?>

<br/>
<h3> Group Discount </h3>
<?php
$groupDiscount = new GroupDiscount('search');
$groupDiscount->unsetAttributes();  // clear any default values
$groupDiscount->group_id = $model->id;
$this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'group-discount-grid',
    'dataProvider'=>$groupDiscount->search(),
//    'filter'=>$groupDiscount,
    'columns'=>array(
//        'id',
//        'group_id',
        'percentfactor',
        'code0.itemname',
        'startperiod',
        'endperiod',
//        array(
//            'class'=>'CButtonColumn',
//        ),
    ),
)); ?>
