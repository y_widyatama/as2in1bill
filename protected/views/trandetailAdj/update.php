<?php
/* @var $this TrandetailAdjController */
/* @var $model TrandetailAdj */

$this->breadcrumbs=array(
	'Trandetail Adjs'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List TrandetailAdj', 'url'=>array('index')),
	array('label'=>'Create TrandetailAdj', 'url'=>array('create')),
	array('label'=>'View TrandetailAdj', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage TrandetailAdj', 'url'=>array('admin')),
);
?>

<h1>Update TrandetailAdj <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>