<?php

/**
 * This is the model class for table "cust_bills_2".
 *
 * The followings are the available columns in table 'cust_bills_2':
 * @property integer $id
 * @property integer $account_id
 * @property string $paydate
 * @property string $paytime
 * @property integer $payamount
 * @property string $paymethod
 * @property string $custnumber
 * @property string $paystatus
 * @property integer $cust_id
 * @property integer $batch_id
 * @property integer $billing_id
 *
 * The followings are the available model relations:
 * @property CustProfile $cust
 */
class CustBillsG extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CustBills the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'cust_bills';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('account_id, payamount, cust_id, batch_id, billing_id', 'numerical', 'integerOnly'=>true),
			array('paydate, paytime, paymethod, custnumber, paystatus', 'length', 'max'=>45),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, account_id, paydate, paytime, payamount, paymethod, custnumber, paystatus, cust_id, batch_id, billing_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'cust' => array(self::BELONGS_TO, 'CustProfile', 'cust_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'account_id' => 'Account',
			'paydate' => 'Paydate',
			'paytime' => 'Paytime',
			'payamount' => 'Payamount',
			'paymethod' => 'Paymethod',
			'custnumber' => 'Custnumber',
			'paystatus' => 'Paystatus',
			'cust_id' => 'Cust',
			'batch_id' => 'Batch',
			'billing_id' => 'Billing',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('account_id',$this->account_id);
		$criteria->compare('paydate',$this->paydate,true);
		$criteria->compare('paytime',$this->paytime,true);
		$criteria->compare('payamount',$this->payamount);
		$criteria->compare('paymethod',$this->paymethod,true);
		$criteria->compare('custnumber',$this->custnumber,true);
		$criteria->compare('paystatus',$this->paystatus,true);
		$criteria->compare('cust_id',$this->cust_id);
		$criteria->compare('batch_id',$this->batch_id);
		$criteria->compare('billing_id',$this->billing_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}