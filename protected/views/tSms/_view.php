<?php
/* @var $this TSmsController */
/* @var $data TSms */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('custnumber')); ?>:</b>
	<?php echo CHtml::encode($data->custnumber); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('acode')); ?>:</b>
	<?php echo CHtml::encode($data->acode); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('batch_id')); ?>:</b>
	<?php echo CHtml::encode($data->batch_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('charge_cent')); ?>:</b>
	<?php echo CHtml::encode($data->charge_cent); ?>
	<br />


</div>