<?php
/* @var $this CustPaymentController */
/* @var $model CustPayment */

$this->breadcrumbs=array(
	'Cust Payments'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List CustPayment', 'url'=>array('index')),
	array('label'=>'Create CustPayment', 'url'=>array('create')),
	array('label'=>'View CustPayment', 'url'=>array('view', 'id'=>
	$model->id)),
	array('label'=>'Manage CustPayment', 'url'=>array('admin')),
);
?>

<h1>Update CustPayment <?php echo  $model->id ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>