<?php
/* @var $this TSmsController */
/* @var $model TSms */

$this->breadcrumbs=array(
	'Tsms'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List TSms', 'url'=>array('index')),
	array('label'=>'Manage TSms', 'url'=>array('admin')),
);
?>

<h1>Create TSms</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>