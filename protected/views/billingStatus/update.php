<?php
/* @var $this BillingStatusController */
/* @var $model BillingStatus */

$this->breadcrumbs=array(
	'Billing Statuses'=>array('index'),
	$model->batch_id=>array('view','id'=>$model->batch_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List BillingStatus', 'url'=>array('index')),
	array('label'=>'Create BillingStatus', 'url'=>array('create')),
	array('label'=>'View BillingStatus', 'url'=>array('view', 'id'=>
	$model->batch_id)),
	array('label'=>'Manage BillingStatus', 'url'=>array('admin')),
);
?>

<h1>Update BillingStatus <?php echo  $model->batch_id ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>