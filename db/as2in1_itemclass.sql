CREATE DATABASE  IF NOT EXISTS `as2in1bill` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `as2in1bill`;
-- MySQL dump 10.13  Distrib 5.5.9, for Win32 (x86)
--
-- Host: 127.0.0.1    Database: as2in1bill
-- ------------------------------------------------------
-- Server version	5.1.35-community

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `itemclass`
--

DROP TABLE IF EXISTS `itemclass`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `itemclass` (
  `id` int(11) NOT NULL,
  `acode` varchar(45) NOT NULL,
  `itemname` varchar(128) DEFAULT NULL,
  `groupname` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`acode`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `itemclass`
--

LOCK TABLES `itemclass` WRITE;
/*!40000 ALTER TABLE `itemclass` DISABLE KEYS */;
INSERT INTO `itemclass` VALUES (30,'ADMIN','Admin','Admin'),(19,'ADMIN_1C2N','Admin As2in1','Admin'),(36,'INTER_SMS','SMS to other operator','SMS'),(37,'INTL_SMS','International SMS','SMS'),(38,'INTRA_SMS','SMS to As2in1','SMS'),(20,'MO_1C2N_AT_INDO','Outgoing call As2in1 at ID','Voice'),(21,'MO_1C2N_IDD','Outgoing call As2in1 IDD','Voice'),(22,'MO_1C2N_IDD_AIR_VCE','Outgoing call airtime As2in1 IDD','Voice'),(23,'MO_1C2N_LOCAL','Outgoing call As2in1 local','Voice'),(29,'MO_1C2N_ROAM_3RD_VCE','Roaming outgoing call As2in1 3rd','Voice'),(24,'MO_1C2N_ROAM_HK_VCE','Roaming outgoing call As2in1 HK','Voice'),(25,'MO_1C2N_ROAM_LOCAL_VCE','Roaming outgoing call As2in1 at ID','Voice'),(31,'MO_IDD','Outgoung call (IDD)','Voice'),(32,'MO_IDD_AIR_VCE','Outgoing call airtime (IDD)','Voice'),(33,'MO_LOCAL','Outgoing voice call','Voice'),(35,'MO_ROAM_LOCAL_VCE','Outgoing call local roaming','Voice'),(1,'MO_ROAM_SMS','Roaming - SMS','International Roaming'),(2,'MO_ROAM_VCE','Roaming - Outgoing voice call','International Roaming'),(26,'MT_1C2N_AT_HK','Incoming call As2in1 at HK','Voice'),(27,'MT_1C2N_LOCAL','Incoming call As2in1 at ID','Voice'),(34,'MT_LOCAL','Incoming voice call','Voice'),(3,'MT_ROAM_VCE','Roaming - Incoming voice call','International Roaming'),(28,'ROAMING_1C2N_MT','Roaming incoming call As2in1','Voice'),(39,'ROAMING_SMS','SMS roaming','SMS'),(9,'TXN_BLACKBERRY_RENEW_PLAN','Blackberry Plan renewal','Blackberry'),(7,'TXN_BLACKBERRY_SUB_PLAN','Blackberry Plan','Blackberry'),(10,'TXN_DATA_CHG_DEBIT','Data debit charge','Data'),(11,'TXN_DATA_PAY_PER_USE_CREDIT','Data pay per use credit','Data'),(12,'TXN_DATA_PAY_PER_USE_DEBIT','Data pay per use debit','Data'),(4,'TXN_DATA_RENEW_DATA_PLAN','Data Plan renewal','Data'),(6,'TXN_DATA_SUB_DATA_PLAN','Data Plan','Data'),(5,'TXN_RBT_RBTFEE','Ringbacktone','Services'),(8,'TXN_VAS_RENEW','Value Add Service','VAS');
/*!40000 ALTER TABLE `itemclass` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `bill02`
--

DROP TABLE IF EXISTS `bill02`;
/*!50001 DROP VIEW IF EXISTS `bill02`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `bill02` (
  `id` int(11),
  `account_id` int(11),
  `custnumber` varchar(45),
  `custname` varchar(128),
  `custaddr` longtext,
  `custemail` varchar(64),
  `corporatename` varchar(100),
  `title` varchar(45),
  `cnumber2` varchar(45),
  `createdate` varchar(45),
  `statecode` varchar(45),
  `termdate` varchar(45),
  `batch_id` int(11),
  `itemname` varchar(128),
  `groupname` varchar(128),
  `charge_cent` decimal(35,2)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `v_roaming`
--

DROP TABLE IF EXISTS `v_roaming`;
/*!50001 DROP VIEW IF EXISTS `v_roaming`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `v_roaming` (
  `custnumber` varchar(45),
  `batch_id` int(11),
  `acode` varchar(45),
  `charge_cent` decimal(32,0)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `v_sms`
--

DROP TABLE IF EXISTS `v_sms`;
/*!50001 DROP VIEW IF EXISTS `v_sms`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `v_sms` (
  `custnumber` varchar(45),
  `acode` varchar(45),
  `batch_id` int(11),
  `charge_cent` decimal(32,0)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `v_trandetail`
--

DROP TABLE IF EXISTS `v_trandetail`;
/*!50001 DROP VIEW IF EXISTS `v_trandetail`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `v_trandetail` (
  `custnumber` varchar(45),
  `batch_id` int(11),
  `tcode` varchar(45),
  `charge_cent` decimal(32,0)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `v_voice`
--

DROP TABLE IF EXISTS `v_voice`;
/*!50001 DROP VIEW IF EXISTS `v_voice`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `v_voice` (
  `custnumber` varchar(45),
  `batch_id` int(11),
  `acode` varchar(45),
  `charge_cent` decimal(32,0)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Final view structure for view `bill02`
--

/*!50001 DROP TABLE IF EXISTS `bill02`*/;
/*!50001 DROP VIEW IF EXISTS `bill02`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013  SQL SECURITY DEFINER */
/*!50001 VIEW `bill02` AS select `bill01`.`id` AS `id`,`bill01`.`account_id` AS `account_id`,`bill01`.`custnumber` AS `custnumber`,`bill01`.`custname` AS `custname`,`bill01`.`custaddr` AS `custaddr`,`bill01`.`custemail` AS `custemail`,`bill01`.`corporatename` AS `corporatename`,`bill01`.`title` AS `title`,`bill01`.`cnumber2` AS `cnumber2`,`bill01`.`createdate` AS `createdate`,`bill01`.`statecode` AS `statecode`,`bill01`.`termdate` AS `termdate`,`bill01`.`batch_id` AS `batch_id`,`bill01`.`itemname` AS `itemname`,`bill01`.`groupname` AS `groupname`,(`bill01`.`charge_cent` * 0.01) AS `charge_cent` from `bill01` where (`bill01`.`charge_cent` > 0) order by `bill01`.`batch_id`,`bill01`.`account_id`,`bill01`.`custnumber` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_roaming`
--

/*!50001 DROP TABLE IF EXISTS `v_roaming`*/;
/*!50001 DROP VIEW IF EXISTS `v_roaming`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013  SQL SECURITY DEFINER */
/*!50001 VIEW `v_roaming` AS select `roaming_cdr`.`custnumber` AS `custnumber`,`roaming_cdr`.`batch_id` AS `batch_id`,`roaming_cdr`.`acode` AS `acode`,sum(`roaming_cdr`.`charge_cent`) AS `charge_cent` from `roaming_cdr` group by `roaming_cdr`.`custnumber`,`roaming_cdr`.`batch_id`,`roaming_cdr`.`acode` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_sms`
--

/*!50001 DROP TABLE IF EXISTS `v_sms`*/;
/*!50001 DROP VIEW IF EXISTS `v_sms`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013  SQL SECURITY DEFINER */
/*!50001 VIEW `v_sms` AS select `sms_cdr`.`custnumber` AS `custnumber`,`sms_cdr`.`acode` AS `acode`,`sms_cdr`.`batch_id` AS `batch_id`,sum(`sms_cdr`.`charge_cent`) AS `charge_cent` from `sms_cdr` group by `sms_cdr`.`custnumber`,`sms_cdr`.`acode`,`sms_cdr`.`batch_id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_trandetail`
--

/*!50001 DROP TABLE IF EXISTS `v_trandetail`*/;
/*!50001 DROP VIEW IF EXISTS `v_trandetail`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013  SQL SECURITY DEFINER */
/*!50001 VIEW `v_trandetail` AS select `trandetail_cdr`.`custnumber` AS `custnumber`,`trandetail_cdr`.`batch_id` AS `batch_id`,`trandetail_cdr`.`tcode` AS `tcode`,sum(`trandetail_cdr`.`charge`) AS `charge_cent` from `trandetail_cdr` group by `trandetail_cdr`.`batch_id`,`trandetail_cdr`.`custnumber`,`trandetail_cdr`.`tcode` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_voice`
--

/*!50001 DROP TABLE IF EXISTS `v_voice`*/;
/*!50001 DROP VIEW IF EXISTS `v_voice`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013  SQL SECURITY DEFINER */
/*!50001 VIEW `v_voice` AS select `voice_cdr`.`custnumber` AS `custnumber`,`voice_cdr`.`batch_id` AS `batch_id`,`voice_cdr`.`acode` AS `acode`,sum(`voice_cdr`.`charge_cent`) AS `charge_cent` from `voice_cdr` group by `voice_cdr`.`custnumber`,`voice_cdr`.`batch_id`,`voice_cdr`.`acode` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-06-24 17:20:03
