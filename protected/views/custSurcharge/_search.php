<?php
/* @var $this CustSurchargeController */
/* @var $model CustSurcharge */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'customer_id'); ?>
		<?php echo $form->textField($model,'customer_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'startperiod'); ?>
		<?php echo $form->textField($model,'startperiod'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'monthcount'); ?>
		<?php echo $form->textField($model,'monthcount'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'amount_cent'); ?>
		<?php echo $form->textField($model,'amount_cent'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'permonth_cent'); ?>
		<?php echo $form->textField($model,'permonth_cent'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->