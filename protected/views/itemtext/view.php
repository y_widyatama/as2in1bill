<?php
/* @var $this ItemtextController */
/* @var $model Itemtext */

$this->breadcrumbs=array(
	'Itemtexts'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Itemtext', 'url'=>array('index')),
	array('label'=>'Create Itemtext', 'url'=>array('create')),
	array('label'=>'Update Itemtext', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Itemtext', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Itemtext', 'url'=>array('admin')),
);
?>

<h1>View Itemtext #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'itemname',
	),
)); ?>
