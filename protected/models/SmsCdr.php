<?php

/**
 * This is the model class for table "sms_cdr".
 *
 * The followings are the available columns in table 'sms_cdr':
 * @property integer $id
 * @property string $custnumber
 * @property string $acode
 * @property string $tstamp
 * @property string $destnumber
 * @property integer $charge_cent
 * @property integer $status
 * @property integer $smflag
 * @property integer $batch_id
 */
class SmsCdr extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return SmsCdr the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'sms_cdr';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('charge_cent, status, smflag, batch_id', 'numerical', 'integerOnly'=>true),
			array('custnumber, acode, destnumber', 'length', 'max'=>45),
			array('tstamp', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, custnumber, acode, tstamp, destnumber, charge_cent, status, smflag, batch_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'custnumber0' => array(self::BELONGS_TO, 'NumberToProfile', 'custnumber'),
			'itemclass' => array(self::BELONGS_TO, 'Itemclass', 'acode'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'custnumber' => 'Custnumber',
			'acode' => 'Acode',
			'tstamp' => 'Tstamp',
			'destnumber' => 'Destnumber',
			'charge_cent' => 'Charge Cent',
			'status' => 'Status',
			'smflag' => 'Smflag',
			'batch_id' => 'Batch',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	
	public function search($noPage=false)
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('custnumber',$this->custnumber,true);
		$criteria->compare('acode',$this->acode,true);
		$criteria->compare('tstamp',$this->tstamp,true);
		$criteria->compare('destnumber',$this->destnumber,true);
		$criteria->compare('charge_cent',$this->charge_cent);
		$criteria->compare('status',$this->status);
		$criteria->compare('smflag',$this->smflag);
		$criteria->compare('batch_id',$this->batch_id);
		$p = array(
			'criteria'=>$criteria,
		);
		if ($noPage) $p['pagination'] = array('pageSize'=>100000);
		return new CActiveDataProvider($this, $p);
	}
}