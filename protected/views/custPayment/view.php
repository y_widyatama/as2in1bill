<?php
/* @var $this CustPaymentController */
/* @var $model CustPayment */

$this->breadcrumbs=array(
	'Cust Payments'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List CustPayment', 'url'=>array('index')),
	array('label'=>'Create CustPayment', 'url'=>array('create')),
	array('label'=>'Update CustPayment', 'url'=>array('update', 
	   'id'=>$model->id)),
	array('label'=>'Delete CustPayment', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage CustPayment', 'url'=>array('admin')),
);
?>

<h1>View CustPayment #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'custnumber',
		'billing_id',
		'batch_id',
		'payamount',
		'paydate',
		'customer_id',
		'ismanual',
	),
)); ?>
