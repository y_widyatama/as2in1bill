<?php
/* @var $this TTrandetailController */
/* @var $model TTrandetail */

$this->breadcrumbs=array(
	'Ttrandetails'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List TTrandetail', 'url'=>array('index')),
	array('label'=>'Create TTrandetail', 'url'=>array('create')),
	array('label'=>'Update TTrandetail', 'url'=>array('update', 
	   'id'=>$model->id)),
	array('label'=>'Delete TTrandetail', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage TTrandetail', 'url'=>array('admin')),
);
?>

<h1>View TTrandetail #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'custnumber',
		'batch_id',
		'tcode',
		'charge_cent',
		'id',
	),
)); ?>
