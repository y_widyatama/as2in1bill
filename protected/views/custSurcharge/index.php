<?php
/* @var $this CustSurchargeController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Cust Surcharges',
);

$this->menu=array(
	array('label'=>'Create CustSurcharge', 'url'=>array('create')),
	array('label'=>'Manage CustSurcharge', 'url'=>array('admin')),
);
?>

<h1>Cust Surcharges</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
