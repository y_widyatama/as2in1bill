<?php
/* @var $this CodeFactorController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Code Factors',
);

$this->menu=array(
	array('label'=>'Create CodeFactor', 'url'=>array('create')),
	array('label'=>'Manage CodeFactor', 'url'=>array('admin')),
);
?>

<h1>Code Factors</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
