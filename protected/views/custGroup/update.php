<?php
/* @var $this CustGroupController */
/* @var $model CustGroup */

$this->breadcrumbs=array(
	'Cust Groups'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List CustGroup', 'url'=>array('index')),
	array('label'=>'Create CustGroup', 'url'=>array('create')),
	array('label'=>'View CustGroup', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage CustGroup', 'url'=>array('admin')),
);
?>

<h1>Update CustGroup <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>