<?php

class m130806_020706_add_group_fk extends CDbMigration
{
	public function up()
	{
	    $this->addForeignKey('fk_cust_group_cust','cust_group','cust_id','cust_profile','id');
        $this->addForeignKey('fk_cust_group_group','cust_group','group_id','group','id');
		$this->addForeignKey('fk_group_discount_group','group_discount','group_id','group','id');
		$this->alterColumn('group_discount','code','varchar(45) not null');
        $this->addForeignKey('fk_group_discount_code','group_discount','code','itemclass','acode');
		$this->addForeignKey('fk_group_contract_group','group_contract','group_id','group','id');
	}

	public function down()
	{
		 $this->dropForeignKey('fk_cust_group_cust','cust_group');
        $this->dropForeignKey('fk_cust_group_group','cust_group');
		$this->dropForeignKey('fk_group_discount_group','group_discount');
        $this->dropForeignKey('fk_group_discount_code','group_discount');
		$this->dropForeignKey('fk_group_contract_group','group_contract');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}