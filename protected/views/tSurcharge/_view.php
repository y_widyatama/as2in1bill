<?php
/* @var $this TSurchargeController */
/* @var $data TSurcharge */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('custnumber')); ?>:</b>
	<?php echo CHtml::encode($data->custnumber); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('acode')); ?>:</b>
	<?php echo CHtml::encode($data->acode); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('batch_id')); ?>:</b>
	<?php echo CHtml::encode($data->batch_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('charge_cent')); ?>:</b>
	<?php echo CHtml::encode($data->charge_cent); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('surcharge_id')); ?>:</b>
	<?php echo CHtml::encode($data->surcharge_id); ?>
	<br />


</div>