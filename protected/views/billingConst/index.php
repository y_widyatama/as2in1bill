<?php
/* @var $this BillingConstController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Billing Consts',
);

$this->menu=array(
	array('label'=>'Create BillingConst', 'url'=>array('create')),
	array('label'=>'Manage BillingConst', 'url'=>array('admin')),
);
?>

<h1>Billing Consts</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
