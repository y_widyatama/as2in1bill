<?php
$this->breadcrumbs=array(
	'Cust Discounts'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List CustDiscount', 'url'=>array('index')),
	array('label'=>'Create CustDiscount', 'url'=>array('create')),
	array('label'=>'Update CustDiscount', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete CustDiscount', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage CustDiscount', 'url'=>array('admin')),
);
?>

<h1>View CustDiscount #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'cust_id',
		'percentfactor',
		'code',
		'startperiod',
		'endperiod',
	),
)); ?>
