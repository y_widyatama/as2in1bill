<?php
/* @var $this GroupDiscountController */
/* @var $model GroupDiscount */

$this->breadcrumbs=array(
	'Group Discounts'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List GroupDiscount', 'url'=>array('index')),
	array('label'=>'Create GroupDiscount', 'url'=>array('create')),
	array('label'=>'View GroupDiscount', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage GroupDiscount', 'url'=>array('admin')),
);
?>

<h1>Update GroupDiscount <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>