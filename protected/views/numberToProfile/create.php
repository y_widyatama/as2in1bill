<?php
/* @var $this NumberToProfileController */
/* @var $model NumberToProfile */

$this->breadcrumbs=array(
	'Number To Profiles'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List NumberToProfile', 'url'=>array('index')),
	array('label'=>'Manage NumberToProfile', 'url'=>array('admin')),
);
?>

<h1>Create NumberToProfile</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>