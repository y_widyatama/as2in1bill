<?php
/* @var $this GroupDiscountController */
/* @var $model GroupDiscount */

$this->breadcrumbs=array(
	'Group Discounts'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List GroupDiscount', 'url'=>array('index')),
	array('label'=>'Manage GroupDiscount', 'url'=>array('admin')),
);
?>

<h1>Create GroupDiscount</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>