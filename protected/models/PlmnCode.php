<?php

/**
 * This is the model class for table "plmn_code".
 *
 * The followings are the available columns in table 'plmn_code':
 * @property integer $networkcode
 * @property string $plmn_code
 * @property string $network_name
 * @property string $country
 */
class PlmnCode extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return PlmnCode the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'plmn_code';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('networkcode', 'required'),
			array('networkcode', 'numerical', 'integerOnly'=>true),
			array('plmn_code', 'length', 'max'=>45),
			array('network_name', 'length', 'max'=>160),
			array('country', 'length', 'max'=>65),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('networkcode, plmn_code, network_name, country', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'networkcode' => 'Networkcode',
			'plmn_code' => 'Plmn Code',
			'network_name' => 'Network Name',
			'country' => 'Country',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('networkcode',$this->networkcode);
		$criteria->compare('plmn_code',$this->plmn_code,true);
		$criteria->compare('network_name',$this->network_name,true);
		$criteria->compare('country',$this->country,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}