<?php
/* @var $this BillingStatusController */
/* @var $data BillingStatus */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('batch_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->batch_id), array('view', 'id'=>$data->batch_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('roaming')); ?>:</b>
	<?php echo CHtml::encode($data->roaming); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('voice')); ?>:</b>
	<?php echo CHtml::encode($data->voice); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sms')); ?>:</b>
	<?php echo CHtml::encode($data->sms); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('trans')); ?>:</b>
	<?php echo CHtml::encode($data->trans); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('lastupdated')); ?>:</b>
	<?php echo CHtml::encode($data->lastupdated); ?>
	<br />


</div>