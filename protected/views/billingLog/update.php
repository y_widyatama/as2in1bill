<?php
/* @var $this BillingLogController */
/* @var $model BillingLog */

$this->breadcrumbs=array(
	'Billing Logs'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List BillingLog', 'url'=>array('index')),
	array('label'=>'Create BillingLog', 'url'=>array('create')),
	array('label'=>'View BillingLog', 'url'=>array('view', 'id'=>
	$model->id)),
	array('label'=>'Manage BillingLog', 'url'=>array('admin')),
);
?>

<h1>Update BillingLog <?php echo  $model->id ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>