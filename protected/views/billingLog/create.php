<?php
/* @var $this BillingLogController */
/* @var $model BillingLog */

$this->breadcrumbs=array(
	'Billing Logs'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List BillingLog', 'url'=>array('index')),
	array('label'=>'Manage BillingLog', 'url'=>array('admin')),
);
?>

<h1>Create BillingLog</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>