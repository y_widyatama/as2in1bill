<?php

class m130730_135825_add_discount_flag extends CDbMigration
{
	public function up()
	{
        $this->addColumn('t_voice','discount_flag','integer DEFAULT 0');
	}

	public function down()
	{
		$this->dropColumn('t_voice','discount_flag');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}