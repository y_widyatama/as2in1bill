<?php
/* @var $this ItemtextController */
/* @var $model Itemtext */

$this->breadcrumbs=array(
	'Itemtexts'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Itemtext', 'url'=>array('index')),
	array('label'=>'Manage Itemtext', 'url'=>array('admin')),
);
?>

<h1>Create Itemtext</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>