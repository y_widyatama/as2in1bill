-- --------------------------------------------------------------------------------
-- Routine DDL
-- Note: comments before and after the routine body will not be stored by the server
-- --------------------------------------------------------------------------------
DELIMITER $$

CREATE PROCEDURE `resetdata`()
BEGIN
delete from voice_cdr;
delete from t_voice;
delete from t_sms;
delete from t_roaming;
delete from t_trandetail;
delete from sms_cdr;
delete from roaming_cdr;
delete from trandetail_cdr;
END
$$