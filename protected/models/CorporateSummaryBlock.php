<?php

/**
 * ContactForm class.
 * ContactForm is the data structure for keeping
 * contact form data. It is used by the 'contact' action of 'SiteController'.
 */
class CorporateSummaryBlock extends CFormModel
{
    public $name;
    public $addr;
	public $customerService;
	public $accountManager;
    public $period;
    public $invoiceNo;
    /**
     * @var Account $model
     */
    public $model;
    /**
     * @var CustBills[] $bills
     */
    public $bills;
    public function __construct($scenario='')
    {
        parent::__construct($scenario);
        $this->customerService = 'Plasa GraPARI (2890 1230)';
        $this->accountManager= '63755902';

    }

    public function getAccountId()
    {
        return $this->model->id;
    }
    public function getBills()
    { // TODO: support use case where one profile get suspended and the other continues
        if (!empty($this->bills)) return $this->bills;
        $bills = array();
        $profiles = $this->model->custProfiles;
        foreach ($profiles as $p)
        {
            $c = new CDbCriteria();
            $c->compare('batch_id',$this->period);
            $c->order = 'batch_id DESC';
            $c->compare('custnumber',$p->custnumber);
            $bill= CustBills::model()->find($c);
            if (!empty($bill))
                $bills[] = $bill;
        }
        $this->bills= $bills;
        return $bills;
    }
    public function getPrevBalance()
    {
        $total = 0;
        $this->getBills();
        foreach ($this->bills as $bill)
        {
            $total += $bill->prevbalance;
        }
        return $total * 0.01;
    }
    public function getFormatter()
    {
        $f = new MyFormatter();
        return $f;
    }
    public function getNewBalance()
    {
        $total = 0;
        $this->getBills();
        foreach ($this->bills as $bill)
        {
            $total += $bill->newbalance;
        }
        return $total * 0.01;
    }
    public function getPrevPayment()
    {
        $total = 0;
        $this->getBills();
        foreach ($this->bills as $bill)
        {
            $total += $bill->prevpayment;
        }
        return $total * 0.01;
    }
    public function getCurrentInvoice()
    {
        $total = 0;
        $this->getBills();
        foreach ($this->bills as $bill)
        {
            $total += $bill->payamount;
        }
        return $total * 0.01;
    }
    public function getAcctBill()
    {
        $acctBill = AccountBills::model()->findByAttributes(
            array('batch_id'=>$this->period,
                'account_id'=>$this->model->id,
            )
        );
        return $acctBill;
    }
    public function getPayBefore() {
        $this->getBills();
        if (empty($this->bills)) return "-";
        $bills = $this->bills;
        $firstBill = $bills[0];
        if (empty($firstBill->billdate)) return "-";
        $bd = CDateTimeParser::parse($firstBill->billdate,'yyyy-MM-dd HH:mm:ss');
        $mon = date('n',$bd);
        $dy = 20;
        $yr = date('Y',$bd);
        $paybefore = mktime(0,0,0,$mon,$dy,$yr);
        return Yii::app()->dateFormatter->format('EEE, d MMM yyyy',$paybefore);
    }

    public function getPeriodStr()

    {
        $yr = floor($this->period/ 100.0);
        $mon = $this->period % 100;
        $t = mktime(0,0,0,$mon,1,$yr);
        return date('F Y',$t);
    }
    public function getOddCents()
    {
        $bal = $this->getNewBalance();
        $balr = floor($bal);
        $odd = $balr - $bal;
        return $odd;
    }
    public function getRoundBalance()
    {
        return floor ($this->getNewBalance());
    }
	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
            return array();
   	}

	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels()
	{
		return array(
            'name'=>'Company Name ',
            'addr'=>'Company Address ',
            'customerService'=>'Customer Service ',
            'accountManager' => 'Account Manager ',
            'periodStr' => 'Billing Period ',
            'invoiceNo'=>'Bill Number ',
            'accountId'=>'Account number',
            'prevBalance'=>'Previous balance ',
            'prevPayment'=>'Payment received ',
            'currentInvoice'=>'Charge for current period ',
            'newBalance' => 'Subtotal',
            'oddCents' => 'Odd cents carried forward to next bill',
            'roundBalance'=>'Total amount Due ',
        );
	}
}