<?php
/* @var $this TVoiceController */
/* @var $model TVoice */

$this->breadcrumbs=array(
	'Tvoices'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List TVoice', 'url'=>array('index')),
	array('label'=>'Create TVoice', 'url'=>array('create')),
	array('label'=>'Update TVoice', 'url'=>array('update', 
	   'id'=>$model->id)),
	array('label'=>'Delete TVoice', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage TVoice', 'url'=>array('admin')),
);
?>

<h1>View TVoice #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'custnumber',
		'batch_id',
		'acode',
		'charge_cent',
		'id',
	),
)); ?>
