<?php
/* @var $this CustContractController */
/* @var $model CustContract */

$this->breadcrumbs=array(
	'Cust Contracts'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List CustContract', 'url'=>array('index')),
	array('label'=>'Create CustContract', 'url'=>array('create')),
	array('label'=>'View CustContract', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage CustContract', 'url'=>array('admin')),
);
?>

<h1>Update CustContract <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>