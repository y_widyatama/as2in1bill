<?php
/* @var $this CdrStatusController */
/* @var $model CdrStatus */

$this->breadcrumbs=array(
	'Cdr Statuses'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List CdrStatus', 'url'=>array('index')),
	array('label'=>'Manage CdrStatus', 'url'=>array('admin')),
);
?>

<h1>Create CdrStatus</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>