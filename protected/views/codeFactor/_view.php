<?php
/* @var $this CodeFactorController */
/* @var $data CodeFactor */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('code')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->code), array('view', 'id'=>$data->code)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('factor')); ?>:</b>
	<?php echo CHtml::encode($data->factor); ?>
	<br />


</div>