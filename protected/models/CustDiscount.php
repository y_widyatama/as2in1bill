<?php

/**
 * This is the model class for table "cust_discount".
 *
 * The followings are the available columns in table 'cust_discount':
 * @property integer $id
 * @property integer $cust_id
 * @property double $percentfactor
 * @property string $code
 * @property integer $startperiod
 * @property integer $endperiod
 * @property Itemclass $itemclass
 * @property CustProfile $cust
 */
class CustDiscount extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return CustDiscount the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'cust_discount';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('startperiod, endperiod', 'numerical', 'integerOnly'=>true),
			array('percentfactor', 'numerical'),
			array('code', 'length', 'max'=>255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, cust_id, percentfactor, code, startperiod, endperiod', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'itemclass' => array(self::BELONGS_TO, 'Itemclass', 'code'),
			'cust' => array(self::BELONGS_TO, 'CustProfile', 'cust_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'Id',
			'cust_id' => 'Cust',
			'percentfactor' => 'Percentfactor',
			'code' => 'Code',
			'startperiod' => 'Startperiod',
			'endperiod' => 'Endperiod',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
		$criteria->compare('id',$this->id);
		$criteria->compare('cust_id',$this->cust_id);
		$criteria->compare('percentfactor',$this->percentfactor);
		$criteria->compare('code',$this->code,true);
		$criteria->compare('startperiod',$this->startperiod);
		if ($this->endperiod != 999912) $criteria->compare('endperiod',$this->endperiod);

		return new CActiveDataProvider('CustDiscount', array(
			'criteria'=>$criteria,
		));
	}

    public static function getPeriodList(){
        $startMonth = 'September 2012';
        $endPeriod = date('Ym',strtotime('+6 month'));
        $listPeriods = array();
        $i = 0;
        do{
            $periodStr = $startMonth.' '.$i++.' month';
            $period = date('Ym', strtotime($periodStr));
            $periodLabel = date('Y F',strtotime($periodStr));
            $listPeriods[intval($period)] = $periodLabel;
        }while($period <= $endPeriod);
        $listPeriods[999912] = '9999 December';
        return $listPeriods;
    }
}