<?php
/* @var $this CustProfileController */
/* @var $model CustProfile */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'cust-profile-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'account_id'); ?>
		<?php echo $form->textField($model,'account_id'); ?>
		<?php echo $form->error($model,'account_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'custnumber'); ?>
		<?php echo $form->textField($model,'custnumber',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($model,'custnumber'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'custname'); ?>
		<?php echo $form->textField($model,'custname',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'custname'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'custaddr'); ?>
		<?php echo $form->textField($model,'custaddr',array('size'=>60,'maxlength'=>1000)); ?>
		<?php echo $form->error($model,'custaddr'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'custemail'); ?>
		<?php echo $form->textField($model,'custemail',array('size'=>60,'maxlength'=>64)); ?>
		<?php echo $form->error($model,'custemail'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'corporatename'); ?>
		<?php echo $form->textField($model,'corporatename',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'corporatename'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'title'); ?>
		<?php echo $form->textField($model,'title',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($model,'title'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'cnumber2'); ?>
		<?php echo $form->textField($model,'cnumber2',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($model,'cnumber2'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'createdate'); ?>
		<?php echo $form->textField($model,'createdate',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($model,'createdate'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'statecode'); ?>
		<?php echo $form->textField($model,'statecode',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($model,'statecode'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'termdate'); ?>
		<?php echo $form->textField($model,'termdate',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($model,'termdate'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'actualterm'); ?>
		<?php echo $form->textField($model,'actualterm',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($model,'actualterm'); ?>
	</div>
    <div class="row">
   		<?php echo $form->labelEx($model,'minimum_usage'); ?>
   		<?php echo $form->textField($model,'minimum_usage',array('size'=>45,'maxlength'=>45)); ?>
   		<?php echo $form->error($model,'minimum_usage'); ?>
   	</div>

    
	<div class="row">
		<?php echo $form->labelEx($model,'paymentMode'); ?>
		<?php echo $form->textField($model,'paymentMode',array('size'=>60,'maxlength'=>64)); ?>
		<?php echo $form->error($model,'paymentMode'); ?>
	</div>
    
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->