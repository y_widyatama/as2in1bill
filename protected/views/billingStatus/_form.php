<?php
/* @var $this BillingStatusController */
/* @var $model BillingStatus */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'billing-status-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'batch_id'); ?>
		<?php echo $form->textField($model,'batch_id'); ?>
		<?php echo $form->error($model,'batch_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'roaming'); ?>
		<?php echo $form->textField($model,'roaming',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($model,'roaming'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'voice'); ?>
		<?php echo $form->textField($model,'voice',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($model,'voice'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'sms'); ?>
		<?php echo $form->textField($model,'sms',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($model,'sms'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'trans'); ?>
		<?php echo $form->textField($model,'trans',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($model,'trans'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'lastupdated'); ?>
		<?php echo $form->textField($model,'lastupdated'); ?>
		<?php echo $form->error($model,'lastupdated'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->