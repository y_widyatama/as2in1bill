<?php
/* @var $this CustBillsController */
/* @var $model CustBills */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'cust-bills-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'account_id'); ?>
		<?php echo $form->textField($model,'account_id'); ?>
		<?php echo $form->error($model,'account_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'paydate'); ?>
		<?php echo $form->textField($model,'paydate',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($model,'paydate'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'paytime'); ?>
		<?php echo $form->textField($model,'paytime',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($model,'paytime'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'payamount'); ?>
		<?php echo $form->textField($model,'payamount'); ?>
		<?php echo $form->error($model,'payamount'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'paymethod'); ?>
		<?php echo $form->textField($model,'paymethod',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($model,'paymethod'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'custnumber'); ?>
		<?php echo $form->textField($model,'custnumber',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($model,'custnumber'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'paystatus'); ?>
		<?php echo $form->textField($model,'paystatus',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($model,'paystatus'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'cust_id'); ?>
		<?php echo $form->textField($model,'cust_id'); ?>
		<?php echo $form->error($model,'cust_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'batch_id'); ?>
		<?php echo $form->textField($model,'batch_id'); ?>
		<?php echo $form->error($model,'batch_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'billing_id'); ?>
		<?php echo $form->textField($model,'billing_id'); ?>
		<?php echo $form->error($model,'billing_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'chargeamount'); ?>
		<?php echo $form->textField($model,'chargeamount'); ?>
		<?php echo $form->error($model,'chargeamount'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'billdate'); ?>
		<?php echo $form->textField($model,'billdate'); ?>
		<?php echo $form->error($model,'billdate'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'prevbalance'); ?>
		<?php echo $form->textField($model,'prevbalance'); ?>
		<?php echo $form->error($model,'prevbalance'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'prevpayment'); ?>
		<?php echo $form->textField($model,'prevpayment'); ?>
		<?php echo $form->error($model,'prevpayment'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'newbalance'); ?>
		<?php echo $form->textField($model,'newbalance'); ?>
		<?php echo $form->error($model,'newbalance'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'prevodd'); ?>
		<?php echo $form->textField($model,'prevodd'); ?>
		<?php echo $form->error($model,'prevodd'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'newodd'); ?>
		<?php echo $form->textField($model,'newodd'); ?>
		<?php echo $form->error($model,'newodd'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'invoiceround'); ?>
		<?php echo $form->textField($model,'invoiceround'); ?>
		<?php echo $form->error($model,'invoiceround'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->