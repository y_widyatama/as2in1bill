<?php
/* @var $this GroupDiscountController */
/* @var $model GroupDiscount */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'group-discount-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'group_id'); ?>
		<?php echo $form->textField($model,'group_id'); ?>
		<?php echo $form->error($model,'group_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'percentfactor'); ?>
		<?php echo $form->textField($model,'percentfactor'); ?>
		<?php echo $form->error($model,'percentfactor'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'code'); ?>
		<?php echo $form->textField($model,'code',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($model,'code'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'startperiod'); ?>
		<?php echo $form->textField($model,'startperiod'); ?>
		<?php echo $form->error($model,'startperiod'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'endperiod'); ?>
		<?php echo $form->textField($model,'endperiod'); ?>
		<?php echo $form->error($model,'endperiod'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->