<?php
/* @var $this GroupDiscountController */
/* @var $data GroupDiscount */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('group_id')); ?>:</b>
	<?php echo CHtml::encode($data->group_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('percentfactor')); ?>:</b>
	<?php echo CHtml::encode($data->percentfactor); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('code')); ?>:</b>
	<?php echo CHtml::encode($data->code); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('startperiod')); ?>:</b>
	<?php echo CHtml::encode($data->startperiod); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('endperiod')); ?>:</b>
	<?php echo CHtml::encode($data->endperiod); ?>
	<br />


</div>