<?php
/* @var $this BillingStatusController */
/* @var $model BillingStatus */

$this->breadcrumbs=array(
	'Billing Statuses'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List BillingStatus', 'url'=>array('index')),
	array('label'=>'Manage BillingStatus', 'url'=>array('admin')),
);
?>

<h1>Create BillingStatus</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>