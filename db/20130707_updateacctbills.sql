ALTER TABLE `as2in1bill`.`account_bills` ADD COLUMN `emailstatus` VARCHAR(45) NULL DEFAULT '-'  AFTER `batch_id` ,
 ADD COLUMN `emailsent` DATETIME NULL  AFTER `emailstatus` ;
ALTER TABLE `as2in1bill`.`account_bills` ADD COLUMN `invoicepdfcreated` INT NULL DEFAULT 0  AFTER `emailsent` ;
