<?php

/**
 * This is the model class for table "t_surcharge".
 *
 * The followings are the available columns in table 't_surcharge':
 * @property string $custnumber
 * @property string $acode
 * @property integer $batch_id
 * @property string $charge_cent
 * @property integer $id
 * @property integer $surcharge_id
 *
 * The followings are the available model relations:
 * @property CustSurcharge $surcharge
 */
class TSurcharge extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return TSurcharge the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 't_surcharge';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('batch_id, surcharge_id', 'numerical', 'integerOnly'=>true),
			array('custnumber, acode', 'length', 'max'=>45),
			array('charge_cent', 'length', 'max'=>32),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('custnumber, acode, batch_id, charge_cent, id, surcharge_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'surcharge' => array(self::BELONGS_TO, 'CustSurcharge', 'surcharge_id'),
			'itemclass' => array(self::BELONGS_TO, 'Itemclass', 'acode'),
			'custnumber0' => array(self::BELONGS_TO, 'NumberToProfile', 'custnumber'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'custnumber' => 'Custnumber',
			'acode' => 'Acode',
			'batch_id' => 'Batch',
			'charge_cent' => 'Charge Cent',
			'id' => 'ID',
			'surcharge_id' => 'Surcharge',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('custnumber',$this->custnumber,true);
		$criteria->compare('acode',$this->acode,true);
		$criteria->compare('batch_id',$this->batch_id);
		$criteria->compare('charge_cent',$this->charge_cent,true);
		$criteria->compare('id',$this->id);
		$criteria->compare('surcharge_id',$this->surcharge_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}