<?php

/**
 * This is the model class for table "cust_profile".
 *
 * The followings are the available columns in table 'cust_profile':
 * @property integer $id
 * @property integer $account_id
 * @property float $minimum_usage
 * @property string $custnumber
 * @property string $custname
 * @property string $custaddr
 * @property string $custemail
 * @property string $corporatename
 * @property string $title
 * @property string $cnumber2
 * @property string $createdate
 * @property string $statecode
 * @property string $termdate
 * @property string $paymentMode
 *
 * The followings are the available model relations:
 * @property Numbertoprofile[] $numbertoprofiles
 */
class CustProfile extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CustProfile the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'cust_profile';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('account_id', 'numerical', 'integerOnly'=>true),
			array('custnumber, title, cnumber2, createdate, statecode, termdate, actualterm', 'length', 'max'=>45),
            array('minimum_usage','numerical'),
			array('custname', 'length', 'max'=>128),
			array('custaddr', 'length', 'max'=>1000),
			array('custemail', 'length', 'max'=>64),
            array('paymentMode', 'length', 'max'=>64),
			array('corporatename', 'length', 'max'=>100),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, account_id, custnumber, custname, custaddr, custemail, corporatename, title, cnumber2, createdate, statecode, termdate, paymentMode', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'numbertoprofile' => array(self::HAS_ONE, 'NumberToProfile', 'profile_id'),
			'surcharges' => array(self::HAS_MANY, 'CustSurcharge', 'customer_id'),

		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'account_id' => 'Account',
			'custnumber' => 'Custnumber',
			'custname' => 'Custname',
			'custaddr' => 'Custaddr',
			'custemail' => 'Custemail',
			'corporatename' => 'Corporatename',
			'title' => 'Title',
			'cnumber2' => 'Cnumber2',
			'createdate' => 'Createdate',
			'statecode' => 'Statecode',
			'termdate' => 'Termdate',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('account_id',$this->account_id);
		$criteria->compare('custnumber',$this->custnumber,true);
		$criteria->compare('custname',$this->custname,true);
		$criteria->compare('custaddr',$this->custaddr,true);
		$criteria->compare('custemail',$this->custemail,true);
		$criteria->compare('corporatename',$this->corporatename,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('cnumber2',$this->cnumber2,true);
		$criteria->compare('createdate',$this->createdate,true);
		$criteria->compare('statecode',$this->statecode,true);
		$criteria->compare('termdate',$this->termdate,true);
        $criteria->compare('paymentMode',$this->paymentMode,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
    public function isAutoPay() {
        if ($this->paymentMode == 'AUTO') return true;
        return false;
    }
    public static function getProfileList(){
        return CHtml::listData(self::model()->findAll(),'id','custnumber');
    }
}