<?php

class Timeanddate {
   public static function getCurrentDateTime()
   {
      return  date("Y-m-d H:i:s");  
   }
   
   public static function getCurrentDate()
   {
      return  date("Y-m-d");  
   }
   
   public static function getMonthIndo($month){
		switch ($month){
			case 1 : return "Januari";
			case 2 : return "Februari";
			case 3 : return "Maret";
			case 4 : return "April";
			case 5 : return "Mei";
			case 6 : return "Juni";
			case 7 : return "Juli";
			case 8 : return "Agustus";
			case 9 : return "September";
			case 10 : return "Oktober";
			case 11 : return "November";
			case 12 : return "Desember";
		}
   }
   
   public static function getShortMonthIndo($month){
		switch ($month){
			case 1 : return "Jan";
			case 2 : return "Feb";
			case 3 : return "Mar";
			case 4 : return "Apr";
			case 5 : return "Mei";
			case 6 : return "Juni";
			case 7 : return "Jul";
			case 8 : return "Ags";
			case 9 : return "Sep";
			case 10 : return "Okt";
			case 11 : return "Nov";
			case 12 : return "Des";
		}
   }
   
   public static function getDateIndo($date){
		//$date must in format Y-m-d;
		$item = explode('-', $date);
		if(count($item) == 3){
			return $item[2].' '.Timeanddate::getMonthIndo($item[1]).' '.$item[0];
		}
   }
   
   public static function getShortDateIndo($date){
		//$date must in format Y-m-d;
		$item = explode('-', $date);
		if(count($item) == 3){
			return $item[2].' '.Timeanddate::getShortMonthIndo($item[1]).' '.$item[0];
		}else{
			return "-";
		}
   }
   
   public static function getShortDateIndoFromTime($datetime){
		$item = explode(' ', $datetime);
		if(count($item) == 2){
			return Timeanddate::getShortDateIndo($item[0]);
		}else{
			return "-";
		}
   }
   
	public static function getDateTimeIndo($datetime){
		$item = explode(' ', $datetime);
		if(count($item) == 2){
			return Timeanddate::getShortDateIndo($item[0])." - ".$item[1];
		}else{
			return "-";
		}
   }
}