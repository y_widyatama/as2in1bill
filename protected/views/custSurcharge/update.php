<?php
/* @var $this CustSurchargeController */
/* @var $model CustSurcharge */

$this->breadcrumbs=array(
	'Cust Surcharges'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List CustSurcharge', 'url'=>array('index')),
	array('label'=>'Create CustSurcharge', 'url'=>array('create')),
	array('label'=>'View CustSurcharge', 'url'=>array('view', 'id'=>
	$model->id)),
	array('label'=>'Manage CustSurcharge', 'url'=>array('admin')),
);
?>

<h1>Update CustSurcharge <?php echo  $model->id ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>