<?php

class m130805_192529_add_initial_grace extends CDbMigration
{
	public function up()
	{
	    $this->addColumn('cust_contract','initial_grace','integer DEFAULT 1');	    
		$this->addColumn('group_contract','initial_grace','integer DEFAULT 1');	    
	}

	public function down()
	{
	    $this->dropColumn('cust_contract','initial_grace');	    
	    $this->dropColumn('group_contract','initial_grace');	    
		return true;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}