insert into account select distinct account_id,corporatename from cust_profile;
ALTER TABLE `as2in1bill`.`account_bills` 

  ADD CONSTRAINT `fk_bill2acct`

  FOREIGN KEY (`account_id` )

  REFERENCES `as2in1bill`.`account` (`id` )

  ON DELETE NO ACTION

  ON UPDATE NO ACTION

, ADD INDEX `fk_bill2acct` (`account_id` ASC) ;

ALTER TABLE `as2in1bill`.`cust_bills_2` 

  ADD CONSTRAINT `fk_billtoacct`

  FOREIGN KEY (`account_id` )

  REFERENCES `as2in1bill`.`account` (`id` )

  ON DELETE NO ACTION

  ON UPDATE NO ACTION

, ADD INDEX `fk_billtoacct` (`account_id` ASC) ;

delete from cust_bills_2 where batch_id is null or billing_id is null;
ALTER TABLE `as2in1bill`.`cust_bills_2` ADD COLUMN `billdate` DATETIME NULL  AFTER `chargeamount` , ADD COLUMN `prevpayment` INT(11) NULL  AFTER `billdate` , ADD COLUMN `prevbalance` INT(11) NULL  AFTER `prevpayment` , ADD COLUMN `newbalance` INT(11) NULL  AFTER `prevbalance` ;

create table cust_bills_bkup as select * from cust_bills;
delete from cust_bills where batch_id >=201305;
insert into cust_bills 
  select null id, account_id,paydate,paytime,payamount, paymethod, custnumber,paystatus,cust_id, batch_id, billing_id,
  chargeamount,billdate,prevbalance,prevpayment,newbalance from cust_bills_2 where batch_id >= 201305;
  update cust_bills set billdate = null;
  update cust_bills set billdate='2013-06-20' where batch_id=201305;
ALTER TABLE `as2in1bill`.`cust_bills` ADD COLUMN `prevodd` INT NULL DEFAULT 0  AFTER `newbalance` , ADD COLUMN `newodd` INT NULL DEFAULT 0  AFTER `prevodd` , ADD COLUMN `invoiceround` INT NULL  AFTER `newodd` ;

