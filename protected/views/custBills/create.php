<?php
/* @var $this CustBillsController */
/* @var $model CustBills */

$this->breadcrumbs=array(
	'Cust Bills'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List CustBills', 'url'=>array('index')),
	array('label'=>'Manage CustBills', 'url'=>array('admin')),
);
?>

<h1>Create CustBills</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>