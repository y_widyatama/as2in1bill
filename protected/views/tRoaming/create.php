<?php
/* @var $this TRoamingController */
/* @var $model TRoaming */

$this->breadcrumbs=array(
	'Troamings'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List TRoaming', 'url'=>array('index')),
	array('label'=>'Manage TRoaming', 'url'=>array('admin')),
);
?>

<h1>Create TRoaming</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>