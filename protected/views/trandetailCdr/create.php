<?php
/* @var $this TrandetailCdrController */
/* @var $model TrandetailCdr */

$this->breadcrumbs=array(
	'Trandetail Cdrs'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List TrandetailCdr', 'url'=>array('index')),
	array('label'=>'Manage TrandetailCdr', 'url'=>array('admin')),
);
?>

<h1>Create TrandetailCdr</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>