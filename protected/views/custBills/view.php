<?php
/* @var $this CustBillsController */
/* @var $model CustBills */

$this->breadcrumbs=array(
	'Cust Bills'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List CustBills', 'url'=>array('index')),
	array('label'=>'Create CustBills', 'url'=>array('create')),
	array('label'=>'Update CustBills', 'url'=>array('update', 
	   'id'=>$model->id)),
	array('label'=>'Delete CustBills', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage CustBills', 'url'=>array('admin')),
);
?>

<h1>View CustBills #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'account_id',
		'paydate',
		'paytime',
		'payamount',
		'paymethod',
		'custnumber',
		'paystatus',
		'cust_id',
		'batch_id',
		'billing_id',
		'chargeamount',
		'billdate',
		'prevbalance',
		'prevpayment',
		'newbalance',
		'prevodd',
		'newodd',
		'invoiceround',
	),
)); ?>
