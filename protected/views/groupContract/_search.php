<?php
/* @var $this GroupContractController */
/* @var $model GroupContract */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'group_id'); ?>
		<?php echo $form->textField($model,'group_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'minimum_usage'); ?>
		<?php echo $form->textField($model,'minimum_usage',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'startperiod'); ?>
		<?php echo $form->textField($model,'startperiod'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'endperiod'); ?>
		<?php echo $form->textField($model,'endperiod'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'initial_grace'); ?>
		<?php echo $form->textField($model,'initial_grace'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->