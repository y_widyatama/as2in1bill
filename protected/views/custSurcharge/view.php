<?php
/* @var $this CustSurchargeController */
/* @var $model CustSurcharge */

$this->breadcrumbs=array(
	'Cust Surcharges'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List CustSurcharge', 'url'=>array('index')),
	array('label'=>'Create CustSurcharge', 'url'=>array('create')),
	array('label'=>'Update CustSurcharge', 'url'=>array('update', 
	   'id'=>$model->id)),
	array('label'=>'Delete CustSurcharge', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage CustSurcharge', 'url'=>array('admin')),
);
?>

<h1>View CustSurcharge #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'customer_id',
		'startperiod',
		'monthcount',
		'amount_cent',
		'permonth_cent',
	),
)); ?>
