<?php

/**
 * This is the model class for table "group_contract".
 *
 * The followings are the available columns in table 'group_contract':
 * @property integer $id
 * @property integer $group_id
 * @property string $minimum_usage
 * @property integer $startperiod
 * @property integer $endperiod
 * @property integer $initial_grace
 *
 * The followings are the available model relations:
 * @property Group $group
 */
class GroupContract extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return GroupContract the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'group_contract';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('group_id, startperiod, endperiod, initial_grace', 'numerical', 'integerOnly'=>true),
			array('minimum_usage', 'length', 'max'=>10),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, group_id, minimum_usage, startperiod, endperiod, initial_grace', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'group' => array(self::BELONGS_TO, 'Group', 'group_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'group_id' => 'Group',
			'minimum_usage' => 'Minimum Usage',
			'startperiod' => 'Startperiod',
			'endperiod' => 'Endperiod',
			'initial_grace' => 'Initial Grace',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('group_id',$this->group_id);
		$criteria->compare('minimum_usage',$this->minimum_usage);
		$criteria->compare('startperiod',$this->startperiod);
		$criteria->compare('endperiod',$this->endperiod);
		$criteria->compare('initial_grace',$this->initial_grace);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}