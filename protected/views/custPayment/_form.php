<?php
/* @var $this CustPaymentController */
/* @var $model CustPayment */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'cust-payment-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'custnumber'); ?>
		<?php echo $form->textField($model,'custnumber',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($model,'custnumber'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'billing_id'); ?>
		<?php echo $form->textField($model,'billing_id'); ?>
		<?php echo $form->error($model,'billing_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'batch_id'); ?>
		<?php echo $form->textField($model,'batch_id'); ?>
		<?php echo $form->error($model,'batch_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'payamount'); ?>
		<?php echo $form->textField($model,'payamount'); ?>
		<?php echo $form->error($model,'payamount'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'paydate'); ?>
		<?php echo $form->textField($model,'paydate'); ?>
		<?php echo $form->error($model,'paydate'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'customer_id'); ?>
		<?php echo $form->textField($model,'customer_id'); ?>
		<?php echo $form->error($model,'customer_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ismanual'); ?>
		<?php echo $form->textField($model,'ismanual'); ?>
		<?php echo $form->error($model,'ismanual'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->