<?php
/* @var $this CustContractController */
/* @var $model CustContract */

$this->breadcrumbs=array(
	'Cust Contracts'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List CustContract', 'url'=>array('index')),
	array('label'=>'Create CustContract', 'url'=>array('create')),
	array('label'=>'Update CustContract', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete CustContract', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage CustContract', 'url'=>array('admin')),
);
?>

<h1>View CustContract #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'startperiod',
		'endperiod',
		'minimumpayment',
		'cust_id',
	),
)); ?>
