<?php
/* @var $this TVoiceController */
/* @var $model TVoice */

$this->breadcrumbs=array(
	'Tvoices'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List TVoice', 'url'=>array('index')),
	array('label'=>'Manage TVoice', 'url'=>array('admin')),
);
?>

<h1>Create TVoice</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>