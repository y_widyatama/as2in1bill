<?php
/* @var $this TSurchargeController */
/* @var $model TSurcharge */

$this->breadcrumbs=array(
	'Tsurcharges'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List TSurcharge', 'url'=>array('index')),
	array('label'=>'Create TSurcharge', 'url'=>array('create')),
	array('label'=>'View TSurcharge', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage TSurcharge', 'url'=>array('admin')),
);
?>

<h1>Update TSurcharge <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>