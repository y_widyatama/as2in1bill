<?php

class AccountController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('admin','index','view','viewInvoice'),
				'roles'=>array('viewCustomer'),
			),
            array ('allow',
                'actions'=>array('callDetails'),
                'roles'=>array('viewBilling'),
            ),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','create','update','sendEmail','createMissing'),
				'roles'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
        $model = $this->loadModel($id);
        $profileList = $model->custProfiles;
        $profileListProvider = new CArrayDataProvider($profileList);
		$this->render('view',array(
			'model'=>$model,'profileListProvider'=>$profileListProvider,
		));
	}
    public function actionCreateMissing()
    {
        $c = new CDbCriteria();
        $created = 0;
        $createdId = array();
        $c->select = "account_id";
        $c->distinct = true;
        $allAccountIds = CustProfile::model()->findAll($c);
        foreach ($allAccountIds as $row)
        {
            $account = Account::model()->findByPk($row->account_id);
            if (empty($account))
            {
                $n = new Account();
                $n->id = $row->account_id;
                $n->save();
                $created++;
                $createdId [] = $row->account_id;
            }

        }
        $msg = 'Created '.$created . ' accounts'."\r\n";
        $msg .= implode(',',$createdId);
        $this->render('createMissing',compact('msg'));
    }
    public function actionSendEmail($id,$isPdf = 0,$withDetails = 0)
    {
        $model = $this->loadModel($id);
        $profileList = $model->custProfiles;
        $pmodel = new PeriodForm();
        if (isset($_REQUEST['PeriodForm']))
        {
            $pmodel->attributes = $_REQUEST['PeriodForm'];
            if ($isPdf)
            {
                $this->layout = 'nomenu';
                $this->pageTitle = 'Billing Statement';
                $mPDF1 = Yii::app()->ePdf->mpdf('', 'A4');
            }

            $corporateSummary = new CorporateSummaryBlock();
            $corporateSummary->model = $model;
            $corporateSummary->period = $pmodel->batch_id;
            $acctBill = $corporateSummary->getAcctBill();
            $firstProfile = $profileList[0];
            $corporateSummary->addr = $firstProfile->custaddr;
            $corporateSummary->name = $model->name;
            $corporateSummary->period = $pmodel->batch_id;
            $corporateSummary->invoiceNo = $acctBill->id;
            $bills = $model->custBills($pmodel->batch_id);
            $billsProvider = new CArrayDataProvider($bills, array('pagination'=>array('pageSize'=>100000)));
            $email = new MyEmail();
            $mailer = $email->getMailer();
            $mailer->From = 'noreply-billing@kartutelkom.com';
            $mailer->AddReplyTo('yudhi.widyatama@telin.hk');
            $mailer->AddAddress('yudhi.widyatama@gmail.com');
            $mailer->FromName = 'Kartu As2in1';
            $mailer->CharSet = 'UTF-8';
            $mailer->Subject = 'Kartu As2in1 Monthly Bill for '.$corporateSummary->getPeriodStr();
            $msg = $this->renderPartial('emailInvoice',array('corporateSummary'=>$corporateSummary),true);
            $profileListProvider = new CArrayDataProvider($profileList);

            //$mailer->Body = $message;
            $mailer->MsgHTML($msg);
            if ($isPdf)
            {
                $mPDF1->WriteHTML(
                    $this->render('viewInvoice',array(
                        'corporateSummary' => $corporateSummary,
                        'model'=>$model,'profileListProvider'=>$profileListProvider,
                        'profileList'=>$profileList,'pmodel'=>$pmodel,
                        'bills'=>$bills,
                        'billsProvider'=>$billsProvider,
                        'hidePeriod'=>true,
                        'withDetails'=>$withDetails,
                    ),true)
                );
                $invoiceFilename = Yii::app()->runtimePath. '/invoices/'.$pmodel->batch_id.'/corporateInvoice'.$model->id.'.pdf';
                $mPDF1->Output($invoiceFilename,'F');
                $mailer->AddAttachment($invoiceFilename,'Kartu As2in1 Electronic Bill - '.$pmodel->batch_id.'.pdf');
            }

            $mailer->Send();

        }
        echo "email sent";
    }

    public function actionCallDetails($id,$isPdf = 0)
    {

        $this->layout='nomenu';
//        $this->pageTitle = 'CHARGE DETAILS';
        $pmodel = new PeriodForm();
        if (isset($_REQUEST['PeriodForm']))
        {
                $this->layout = 'nomenu';
                $this->pageTitle = 'Billing Statement';
            $pmodel->attributes = $_REQUEST['PeriodForm'];
        }
        if ($isPdf)
        {
            $mPDF1 = Yii::app()->ePdf->mpdf('', 'A4');
        }

        $model = $this->loadModel($id);
        $profileList = $model->custProfiles;
        if ($isPdf)
        {
            $mPDF1->SetFooter("|Page {PAGENO}|");
            $mPDF1->WriteHTML(
                $this->render('callDetailsOnly',array(
                    'pmodel'=>$pmodel,
                    'profileList'=>$profileList
                    //	'voiceCdrs'=>$voiceCdrs,
                ),true)
            );
            $mPDF1->Output();
            return;
        } else
        //$this->pageTitle = 'Call Details for '.$id.' period '.$pmodel->batch_id;
            $this->render('callDetailsOnly',array(
            'pmodel'=>$pmodel,
            'profileList'=>$profileList
            //	'voiceCdrs'=>$voiceCdrs,
        ));
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionViewInvoice($id,$isPdf =0,$withDetails=0,$setLayout=0)
    {
        $model = $this->loadModel($id);
        $pmodel = new PeriodForm();
        $profileList = $model->custProfiles;
        $profileListProvider = new CArrayDataProvider($profileList);
        if (isset($_REQUEST['PeriodForm']))
        {
            if ($isPdf)
            {
              $this->layout = 'nomenu';
              $this->pageTitle = 'Billing Statement';
              $mPDF1 = Yii::app()->ePdf->mpdf('', 'A4');
            }
            if ($setLayout){
                $this->layout = 'nomenu';
                $this->pageTitle = 'Billing Statement';

            }
            $pmodel->attributes = $_REQUEST['PeriodForm'];

            $corporateSummary = new CorporateSummaryBlock();
            $corporateSummary->model = $model;
            $corporateSummary->period = $pmodel->batch_id;
            $acctBill = $corporateSummary->getAcctBill();
            $firstProfile = $profileList[0];
            $corporateSummary->addr = $firstProfile->custaddr;
            $corporateSummary->name = $model->name;
            $corporateSummary->period = $pmodel->batch_id;
            $corporateSummary->invoiceNo = $acctBill->id;

            $bills = $model->custBills($pmodel->batch_id);
            $profileList = array();
            foreach ($bills as $oneBill)
            {
                $profileList [] = $oneBill->cust;
            }
            //$profileList = $model->custProfiles;
            $profileListProvider = new CArrayDataProvider($profileList);
        
            $billsProvider = new CArrayDataProvider($bills, array('pagination'=>array('pageSize'=>100000)));
            if ($isPdf)
            {
                $mPDF1->SetFooter("|Page {PAGENO}|");
                $mPDF1->WriteHTML(
                    $this->render('viewInvoice',array(
                        'corporateSummary' => $corporateSummary,
                        'model'=>$model,'profileListProvider'=>$profileListProvider,
                        'profileList'=>$profileList,'pmodel'=>$pmodel,
                        'bills'=>$bills,
                        'billsProvider'=>$billsProvider,
                        'hidePeriod'=>true,
                        'withDetails'=>$withDetails,
                    ),true)
                    );
                $mPDF1->Output();
                return;
            } else
            $this->render('viewInvoice',array(
                'corporateSummary' => $corporateSummary,
                'model'=>$model,'profileListProvider'=>$profileListProvider,
                'profileList'=>$profileList,'pmodel'=>$pmodel,
                'bills'=>$bills,
                'billsProvider'=>$billsProvider,
                'hidePeriod'=>($setLayout),
                'withDetails'=>$withDetails,
            ));
        }
        else
        $this->render('viewInvoiceChoosePeriod',array(
            'model'=>$model,'profileListProvider'=>$profileListProvider,
            'profileList'=>$profileList,'pmodel'=>$pmodel,
        ));
    }

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Account;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Account']))
		{
			$model->attributes=$_POST['Account'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Account']))
		{
			$model->attributes=$_POST['Account'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Account');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Account('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Account']))
			$model->attributes=$_GET['Account'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Account the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		try
		{
			$model=Account::model()->findByPk($id);
			if($model===null)
				throw new CHttpException(404,'The requested page does not exist.');
		}
		catch(CDbException $e)
		{
			throw new CHttpException(404,'The requested page does not exist.');
		}
		return $model;	
	}

	/**
	 * Performs the AJAX validation.
	 * @param Account $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='account-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
