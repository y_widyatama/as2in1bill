<?php
/* @var $this TRoamingController */
/* @var $model TRoaming */

$this->breadcrumbs=array(
	'Troamings'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List TRoaming', 'url'=>array('index')),
	array('label'=>'Create TRoaming', 'url'=>array('create')),
	array('label'=>'Update TRoaming', 'url'=>array('update', 
	   'id'=>$model->id)),
	array('label'=>'Delete TRoaming', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage TRoaming', 'url'=>array('admin')),
);
?>

<h1>View TRoaming #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'custnumber',
		'batch_id',
		'acode',
		'charge_cent',
		'id',
	),
)); ?>
