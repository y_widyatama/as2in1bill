CREATE  TABLE `cust_contract` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `startperiod` INT NULL ,
  `endperiod` INT NULL ,
  `minimumpayment` INT NULL ,
  `cust_id` INT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `cust_contract_to_profile` (`cust_id` ASC) ,
  CONSTRAINT `cust_contract_to_profile`
    FOREIGN KEY (`cust_id` )
    REFERENCES `as2in1bill`.`cust_profile` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;