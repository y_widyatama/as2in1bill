ALTER TABLE `as2in1bill`.`cust_payment` ADD COLUMN `ismanual` INT NULL DEFAULT 0  AFTER `customer_id` ;
ALTER TABLE `as2in1bill`.`cust_bills` ADD COLUMN `chargeamount` INT NULL  AFTER `billing_id` ;
ALTER TABLE `as2in1bill`.`cust_bills` ADD COLUMN `billdate` DATETIME NULL  AFTER `chargeamount` , ADD COLUMN `prevbalance` INT NULL  AFTER `billdate` , ADD COLUMN `prevpayment` INT NULL  AFTER `prevbalance` , ADD COLUMN `newbalance` INT NULL  AFTER `prevpayment` ;

CREATE  TABLE `as2in1bill`.`account_bills` (

  `id` INT NOT NULL AUTO_INCREMENT ,

  `billdate` DATETIME NULL ,

  `account_id` INT NULL ,

  `totalinvoice` INT NULL ,

  PRIMARY KEY (`id`) );

ALTER TABLE `as2in1bill`.`account_bills` ADD COLUMN `batch_id` INT NULL  AFTER `totalinvoice` ;

insert into account_bills 
 select distinct billing_id,NULL,account_id,0,batch_id from cust_bills where batch_id<201306;
 
ALTER TABLE `as2in1bill`.`account_bills` 
  ADD UNIQUE INDEX `uniqacct` (`account_id` ASC, `batch_id` ASC) ;

