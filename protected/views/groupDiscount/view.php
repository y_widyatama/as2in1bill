<?php
/* @var $this GroupDiscountController */
/* @var $model GroupDiscount */

$this->breadcrumbs=array(
	'Group Discounts'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List GroupDiscount', 'url'=>array('index')),
	array('label'=>'Create GroupDiscount', 'url'=>array('create')),
	array('label'=>'Update GroupDiscount', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete GroupDiscount', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage GroupDiscount', 'url'=>array('admin')),
);
?>

<h1>View GroupDiscount #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
//		'id',
		'group.name',
		'percentfactor',
		'code0.itemname',
		'startperiod',
		'endperiod',
	),
)); ?>
