<?php

class AccountBillsController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';
    public $mailCc = 'postpaid@telin.hk';
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('admin','view','index'),
				'roles'=>array('viewBilling'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','delete','sendEmail','createPdf','sendBlast'),
				'roles'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
    public function sendPersonalEmail($accountBillsModel, $custBill)
    {
        /**
         * @var CustBills $custBill
         * @var AccountBills $accountBillsModel
         */
        $email = new MyEmail();
        $mailer = $email->getMailer();
        $mailer->From = 'noreply-billing@mail.kartutelkom.com';
//        $mailer->AddReplyTo('yudhi.widyatama@telin.hk');
 //       $mailer->AddAddress('yudhi.widyatama@gmail.com');
        $mailer->AddBCC('yudhi.widyatama@telin.hk');
        $mailer->AddCC($this->mailCc);
        $mailer->AddCustomHeader('Precedence','bulk');
        $mailer->AddCustomHeader('Auto-Submitted','auto-generated');
        if (strpos($custBill->cust->custemail,';')===FALSE)
        {
            $mailAddress = $custBill->cust->custemail;
            $name = $custBill->cust->custname;
            $mailer->AddAddress($mailAddress, $name);
        } else
        {   $mails = explode(";",$custBill->cust->custemail);
            $name = $custBill->cust->custname;
            foreach ($mails as $mailAddress)
                $mailer->AddAddress($mailAddress,$name);
        }
        $mailer->FromName = 'Kartu As2in1';
        $mailer->SMTPDebug=false;
        $mailer->CharSet = 'UTF-8';
        $mailer->Subject = 'Kartu As2in1 Monthly Bill for Account '.$accountBillsModel->account_id.' - '.$custBill->getPeriodStr();
        $msg = $this->renderPartial('/number2profile/emailInvoice',array('custBill'=>$custBill),true);

        //$mailer->Body = $message;
        $mailer->MsgHTML($msg);
        $invoiceFilename = $this->createInvoiceFilename(false,$accountBillsModel->batch_id,$custBill->custnumber);
        $mailer->AddAttachment($invoiceFilename,'Kartu As2in1 Electronic Bill - '.$accountBillsModel->batch_id.'_'.
            $custBill->custnumber .
            '.pdf');

        if ($mailer->Send())
        {
            $accountBillsModel->emailsent = date('Y-m-d H:i:s');
            $accountBillsModel->emailstatus = 'SENT';
            $accountBillsModel->save();
        } else
        {
            $accountBillsModel->emailsent = date('Y-m-d H:i:s');
            $accountBillsModel->emailstatus = 'FAILED';
            $accountBillsModel->save();
        }
        Yii::app()->user->setFlash('notice'.$this->i,"Email for invoice $custBill->billing_id has been sent with status ".
            $accountBillsModel->emailstatus);
        $this->i = $this->i + 1;

    }
    public function actionSendBlast($dest = 'regina.chow@telin.hk')
    {
            $email = new MyEmail();
            $mailer = $email->getBulkMailer();
            $mailer->From = 'regina.chow@telin.hk';
            //$mailer->AddReplyTo('yudhi.widyatama@telin.hk');
            //$mailer->AddAddress('yudhi.widyatama@gmail.com');
            $mailer->FromName = 'Regina';
            $mailer->AddBCC('yudhi.widyatama@telin.hk');
            $mailer->AddCC('regina.chow@telin.hk');
            $mailer->AddCustomHeader('Precedence','bulk');
            $mailer->AddCustomHeader('Auto-Submitted','auto-generated');
            $mailer->AddAddress($dest);
            $mailer->SMTPDebug=false;
            $mailer->CharSet = 'UTF-8';
            $mailer->Subject = 'Invitation Bintang Asik';
            $msg = "Dear our valuable customer,<br>We would like to say :<br><img src='cid:newyear'/>Regina<br>Mobile: 63755902<br>Email: regina.chow@telin.hk";
			$msg = '<div>
<div style="font-size: 13px;">Kpd pelanggan Yth,&nbsp;</div>
<div style="font-size: 13px;">&nbsp;</div>
<div style="font-size: 13px;">Dengan bangga kami perkenalkan Internet Roaming unlimited di Indonesia sekarang tersedia.</div>
</div>
<div style="font-size: 13px;">&nbsp;</div>
<div>
<table style="border-collapse: collapse; width: 346pt;" border="0" width="346" cellspacing="0" cellpadding="0"><colgroup><col style="width: 103pt;" width="103" /> <col style="width: 118pt;" width="118" /> <col style="width: 125pt;" width="125" /> </colgroup>
<tbody>
<tr style="height: 15pt;">
<td style="height: 15pt; width: 221pt;" colspan="2" width="221" height="15">Paket Internet&nbsp;</td>
<td style="border-left-style: none; width: 125pt;" width="125">Pemakaian&nbsp;</td>
</tr>
<tr style="height: 15pt;">
<td style="height: 15pt; border-top-style: none;" height="15">Roaming Harian&nbsp;</td>
<td style="border-top-style: none; border-left-style: none;">$ 10/ Hari&nbsp;</td>
<td style="border-top-style: none; border-left-style: none;">1 hari unlimited</td>
</tr>
<tr style="height: 15pt;">
<td style="height: 15pt; border-top-style: none;" height="15">Roaming Mingguan</td>
<td style="border-top-style: none; border-left-style: none;">$ 60/ Minggu</td>
<td style="border-top-style: none; border-left-style: none;">7 hari unlimited</td>
</tr>
<tr style="height: 15pt;">
<td style="height: 15pt; border-top-style: none;" height="15">Roaming Bulanan</td>
<td style="border-top-style: none; border-left-style: none;">$ 200/ Bulan</td>
<td style="border-top-style: none; border-left-style: none;">30 hari unlimited</td>
</tr>
</tbody>
</table>
</div>
<div>Cara Aktivasi Internet Roaming :</div>
<div>1. Pastikan Mobile Data --&gt; ON, Data Roaming --&gt; ON, Network Operator --&gt; Telkomsel, APN --&gt; ON</div>
<div>2. Hubungi *134*2*4# , di halaman ini anda dapat memilih paket, membatalkan paket, stop perpanjangan otomatis dan cek pemakaian.&nbsp;</div>
<div>3. Setting APN (tulis) Telkom</div>
<div>&nbsp;</div>
<div>Terima kasih atas perhatiannya.</div>
<div>&nbsp;</div>
<div>Dear our valuable customer,&nbsp;</div>
<div>&nbsp;</div>
<div>We are proudly to announce that Unlimited Roaming Data in Indonesia available now.</div>
<div>&nbsp;</div>
<div>
<table style="border-collapse: collapse; width: 346pt;" border="0" width="346" cellspacing="0" cellpadding="0"><colgroup><col style="width: 103pt;" width="103" /> <col style="width: 118pt;" width="118" /> <col style="width: 125pt;" width="125" /> </colgroup>
<tbody>
<tr style="height: 15pt;">
<td style="height: 15pt; width: 221pt;" colspan="2" width="221" height="15">Internet Package&nbsp;</td>
<td style="border-left-style: none; width: 125pt;" width="125">Usage</td>
</tr>
<tr style="height: 15pt;">
<td style="height: 15pt; border-top-style: none;" height="15">Roaming per Day&nbsp;</td>
<td style="border-top-style: none; border-left-style: none;">$ 10/ Day</td>
<td style="border-top-style: none; border-left-style: none;">1 day unlimited</td>
</tr>
<tr style="height: 15pt;">
<td style="height: 15pt; border-top-style: none;" height="15">Roaming per Week</td>
<td style="border-top-style: none; border-left-style: none;">$ 60/ Week</td>
<td style="border-top-style: none; border-left-style: none;">7 days unlimited</td>
</tr>
<tr style="height: 15pt;">
<td style="height: 15pt; border-top-style: none;" height="15">Roaming per Month&nbsp;</td>
<td style="border-top-style: none; border-left-style: none;">$ 200/ Month</td>
<td style="border-top-style: none; border-left-style: none;">30 days unlimited</td>
</tr>
</tbody>
</table>
</div>
<div>To activated the Internet Roaming :&nbsp;</div>
<div>1. Make sure the Mobile Data --&gt; ON, Data Roaming --&gt; ON, Network Operator --&gt; Telkomsel, APN --&gt; ON</div>
<div>2. Call *134*2*4#, in this page, you can choose the package, cancel the package, stop the auto renewal and check the usage.&nbsp;</div>
<div>3. Setting APN ( write ) Telkom&nbsp;</div>
<div>&nbsp;</div>
<div>Thank you for your attention.&nbsp;</div><br>
Best Regards<br>
Regina Chow<br>
Enterprise Account Manager<br>
Mobile    : 6375 5902<br>
Office     : 3102 3373<br>
Fax         : 3102 3039<br>
Email      : regina.chow@telin.hk<br>
Website  : www.kartuas2in1.com';
            $mailer->Subject = 'Happy Idul Fitri 1437H from Telin HK';
            $msg = '<h2>Minal \'Aidin wal-Faidzin</h2><br><img alt="Happy Idul Fitri 1437H from Telin HK" src="cid:idulfitri">';
            //$mailer->Body = $message;
            $mailer->MsgHTML($msg);
            //$mailer->AddAttachment('attach/sample-2.jpg','sample-2.jpg');
            //$mailer->AddAttachment('attach/Direct Debit Authorisation Form-7-3.pdf','Direct Debit Authorisation Form-7-3.pdf');
            $mailer->AddEmbeddedImage('images/idulfitri2016.jpg', 'idulfitri');
            if ($mailer->Send())
                echo 'Send success';
            else
                echo 'Send failed';
            
    }
    public function actionSendBlast2($id,$v2)
    {
            $email = new MyEmail();
            $mailer = $email->getBulkMailer();
            $mailer->From = 'regina.chow@telin.hk';
            //$mailer->AddReplyTo('yudhi.widyatama@telin.hk');
            //$mailer->AddAddress('yudhi.widyatama@gmail.com');
            $mailer->FromName = 'Regina';
            $mailer->AddBCC('yudhi.widyatama@telin.hk');
            $mailer->AddCC('regina.chow@telin.hk');
            $mailer->AddCustomHeader('Precedence','bulk');
            $mailer->AddCustomHeader('Auto-Submitted','auto-generated');
            $accountBillsModel = $this->loadModel($id);        
            $userstr = '';
            if ($accountBillsModel->account_id > 20000000) // personal
            {
                $bills = $accountBillsModel->bills;
                $custBill = $bills[0];
                if (strpos($custBill->cust->custemail,';')===FALSE)
                {
                    $mailAddress = $custBill->cust->custemail;
                    $name = $custBill->cust->custname;
                    $mailer->AddAddress($mailAddress, $name);
                    $userstr .= $mailAddress .';';
                } else
                {   $mails = explode(";",$custBill->cust->custemail);
                    $name = $custBill->cust->custname;
                    foreach ($mails as $mailAddress)
                    {
                        $mailer->AddAddress($mailAddress,$name);
                        $userstr .= $mailAddress  .';';
                    }
                }
            } else {
            $model = $accountBillsModel->account;
            $profileList = $model->custProfiles;            
            $destinations = $model->gatherEmails($accountBillsModel->batch_id);
            foreach ($destinations as $name => $mailAddress)
                $mailer->AddAddress($mailAddress, $name);
                $userstr .= $mailAddress  .';';
            }
            $mailer->SMTPDebug=false;
            $mailer->CharSet = 'UTF-8';
            $mailer->Subject = 'Happy Idul Fitri 1437H from Telin HK';
            $msg = '<h2>Minal \'Aidin wal-Faidzin</h2><br><img alt="Happy Idul Fitri 1437H from Telin HK" src="cid:idulfitri">';
            //$mailer->Body = $message;
            $mailer->MsgHTML($msg);
            //$mailer->AddAttachment('attach/sample-2.jpg','sample-2.jpg');
            //$mailer->AddAttachment('attach/Direct Debit Authorisation Form-7-3.pdf','Direct Debit Authorisation Form-7-3.pdf');
            $mailer->AddEmbeddedImage('images/idulfitri2016.jpg', 'idulfitri');
            //$mailer->AddEmbeddedImage('images/newyear.jpg', 'newyear');
            if ($mailer->Send())
            {
                $stat = 'SUCCEED';                
            }
            else
            {
                $stat = 'FAILED';
            }
        Yii::app()->user->setFlash('notice'.$this->i,"Blast  for user $userstr has been sent with status ".
                $accountBillsModel->emailstatus);
            $this->i = $this->i + 1;
            
    }
    public function actionSendEmail($id,$nodisplay=0)
    {
        $accountBillsModel = $this->loadModel($id);
        if (!$accountBillsModel->invoicepdfcreated) {
            echo "pdf file not created yet for accountBill # $id <br>";
            return;
        }
        if ($accountBillsModel->account_id > 20000000) // personal
        {
            $bills = $accountBillsModel->bills;
            $firstBill = $bills[0];
            $this->sendPersonalEmail($accountBillsModel,$firstBill);
            return;
        }


        $model = $accountBillsModel->account;
        $profileList = $model->custProfiles;
        $pmodel = new PeriodForm();
        $pmodel->batch_id = $accountBillsModel->batch_id;
        $accountBillsModel->emailstatus = 'RUN';
        $accountBillsModel->save();
            $corporateSummary = new CorporateSummaryBlock();
            $corporateSummary->model = $model;
            $corporateSummary->period = $pmodel->batch_id;
            $acctBill = $corporateSummary->getAcctBill();
            $firstProfile = $profileList[0];
            $corporateSummary->addr = $firstProfile->custaddr;
            $corporateSummary->name = $model->name;
            $corporateSummary->period = $pmodel->batch_id;
            $corporateSummary->invoiceNo = $acctBill->id;
            $bills = $model->custBills($pmodel->batch_id);
            $billsProvider = new CArrayDataProvider($bills, array('pagination'=>array('pageSize'=>100000)));
            $email = new MyEmail();
            $mailer = $email->getMailer();
            $mailer->From = 'noreply-billing@mail.kartutelkom.com';
            //$mailer->AddReplyTo('yudhi.widyatama@telin.hk');
            //$mailer->AddAddress('yudhi.widyatama@gmail.com');
            $mailer->FromName = 'Kartu As2in1';
            $mailer->AddBCC('yudhi.widyatama@telin.hk');
            $mailer->AddCC($this->mailCc);
            $mailer->AddCustomHeader('Precedence','bulk');
            $mailer->AddCustomHeader('Auto-Submitted','auto-generated');

            $destinations = $model->gatherEmails($pmodel->batch_id);
            foreach ($destinations as $name => $mailAddress)
                $mailer->AddAddress($mailAddress, $name);
            $mailer->SMTPDebug=false;
            $mailer->CharSet = 'UTF-8';
            $mailer->Subject = 'Kartu As2in1 Monthly Bill for Account '.$accountBillsModel->account_id.' Period '.$corporateSummary->getPeriodStr();
            $msg = $this->renderPartial('/account/emailInvoice',array('corporateSummary'=>$corporateSummary),true);
            $profileListProvider = new CArrayDataProvider($profileList);

            //$mailer->Body = $message;
            $mailer->MsgHTML($msg);
            $invoiceFilename = $this->createInvoiceFilename(true,$pmodel->batch_id,$model->id);
            $mailer->AddAttachment($invoiceFilename,'Kartu As2in1 Electronic Bill - '.$pmodel->batch_id.'.pdf');

            if ($mailer->Send())
            {
                $accountBillsModel->emailsent = date('Y-m-d H:i:s');
                $accountBillsModel->emailstatus = 'SENT';
                $accountBillsModel->save();
            } else
            {
                $accountBillsModel->emailsent = date('Y-m-d H:i:s');
                $accountBillsModel->emailstatus = 'FAILED';
                $accountBillsModel->save();
            }
        if ($nodisplay)
        {
            Yii::app()->user->setFlash('notice'.$this->i,"Email for invoice $id has been sent with status ".
                $accountBillsModel->emailstatus);
            $this->i = $this->i + 1;
        } else
        {
            Yii::app()->user->setFlash('notice',"Email for invoice $id has been sent with status ".
                $accountBillsModel->emailstatus);
            $this->redirect(array('/accountBills/view','id'=>$id));
        }
    }
    function renderPersonalPdf(&$mPDF1, $custNumber, $batchId, $withDetails=0)
    {
        $this->layout='nomenu';
        $numbertoprofilemodel = NumberToProfile::model()->findByPk($custNumber);
        $mPDF1->WriteHTML(
            $this->render('/numberToProfile/viewInvoice2',array(
                'model'=>$numbertoprofilemodel,
                'batchId'=>$batchId,
                'withDetails'=>$withDetails,
            ),true));    
    }
    function renderCorporatePdf(&$mPDF1, $accountBillsModel, $withDetails=false, $summaryOnly=false)
    {
        $model = $accountBillsModel->account;
        $profileList = $model->custProfiles;
        $profileListProvider = new CArrayDataProvider($profileList);
        $pmodel = new PeriodForm();
        $pmodel->batch_id = $accountBillsModel->batch_id;
        $this->layout = 'nomenu';
        $this->pageTitle = 'Billing Statement';
        
        $corporateSummary = new CorporateSummaryBlock();
        $corporateSummary->model = $model;
        $corporateSummary->period = $pmodel->batch_id;
        $acctBill = $corporateSummary->getAcctBill();
        $firstProfile = $profileList[0];
        $corporateSummary->addr = $firstProfile->custaddr;
        $corporateSummary->name = $model->name;
        $corporateSummary->period = $pmodel->batch_id;
        $corporateSummary->invoiceNo = $acctBill->id;

        $bills = $model->custBills($pmodel->batch_id);
        $billsProvider = new CArrayDataProvider($bills, array('pagination'=>array('pageSize'=>100000)));
        $mPDF1->WriteHTML(
            $this->render('/account/viewInvoice',array(
                'corporateSummary' => $corporateSummary,
                'model'=>$model,'profileListProvider'=>$profileListProvider,
                'profileList'=>$profileList,'pmodel'=>$pmodel,
                'bills'=>$bills,
                'billsProvider'=>$billsProvider,
                'hidePeriod'=>true,
                'withDetails'=>$withDetails,
				'summaryOnly'=>$summaryOnly,
            ),true)
        );
    }
    function createPersonalPdf($custnumber, $batchId)
    {
        $id = $custnumber;
        $this->layout='nomenu';
        $mPDF1 = Yii::app()->ePdf->mpdf();

        # You can easily override default constructor's params
        $this->pageTitle = 'Billing Statement';
        $numbertoprofilemodel = NumberToProfile::model()->findByPk($id);
        $mPDF1 = Yii::app()->ePdf->mpdf('', 'A4');
        $mPDF1->WriteHTML(
            $this->render('/numberToProfile/viewInvoice2',array(
                'model'=>$numbertoprofilemodel,
                'batchId'=>$batchId,
            ),true));
        $invoiceFilename = $this->createInvoiceFilename(false,$batchId,$id);
        $mPDF1->Output($invoiceFilename,'F');
        return $invoiceFilename;
    }
    function createInvoiceFilename($iscorporate,$batch_id,$id)
    {
        if (!$iscorporate)
          return Yii::app()->runtimePath. '/invoices/'.$batch_id.
             '_personalInvoice_'.$id.'.pdf';
        return Yii::app()->runtimePath. '/invoices/'.$batch_id.
            '_corporateInvoice_'.$id.'.pdf';

    }

    public function actionCreatePdf($id,$nodisplay=0)
    {
        $accountBillsModel = $this->loadModel($id);
        $accountBillsModel->emailstatus = 'PDF';
        $accountBillsModel->save();
        if ($accountBillsModel->account_id > 20000000) // personal
        {
            $bills = $accountBillsModel->bills;
            if (empty($bills)) return;
            $firstBill = $bills[0];
            $invoiceFilename = $this->createPersonalPdf($firstBill->custnumber,$accountBillsModel->batch_id);
            $accountBillsModel->invoicepdfcreated = 1;
            $accountBillsModel->emailstatus = '-';
            $accountBillsModel->save();
            if ($nodisplay)
            {
                Yii::app()->user->setFlash('notice'.$this->i,"PDF $invoiceFilename created.");
                $this->i = $this->i + 1;
            } else
            {
                Yii::app()->user->setFlash('notice',"PDF $invoiceFilename created.");
                $this->redirect(array('/accountBills/view','id'=>$id));
            }

            return;
        }
        $model = $accountBillsModel->account;
        $profileList = $model->custProfiles;
        $profileListProvider = new CArrayDataProvider($profileList);
        $pmodel = new PeriodForm();
        $pmodel->batch_id = $accountBillsModel->batch_id;
        $this->layout = 'nomenu';
        $this->pageTitle = 'Billing Statement';
        $mPDF1 = Yii::app()->ePdf->mpdf('', 'A4');

        $corporateSummary = new CorporateSummaryBlock();
        $corporateSummary->model = $model;
        $corporateSummary->period = $pmodel->batch_id;
        $acctBill = $corporateSummary->getAcctBill();
        $firstProfile = $profileList[0];
        $corporateSummary->addr = $firstProfile->custaddr;
        $corporateSummary->name = $model->name;
        $corporateSummary->period = $pmodel->batch_id;
        $corporateSummary->invoiceNo = $acctBill->id;

        $bills = $model->custBills($pmodel->batch_id);
        $billsProvider = new CArrayDataProvider($bills, array('pagination'=>array('pageSize'=>100000)));
        $mPDF1->WriteHTML(
            $this->render('/account/viewInvoice',array(
                'corporateSummary' => $corporateSummary,
                'model'=>$model,'profileListProvider'=>$profileListProvider,
                'profileList'=>$profileList,'pmodel'=>$pmodel,
                'bills'=>$bills,
                'billsProvider'=>$billsProvider,
                'hidePeriod'=>true,
                'withDetails'=>true,
            ),true)
        );
        $invoiceFilename = $this->createInvoiceFilename(true,$pmodel->batch_id,$model->id);
        $mPDF1->Output($invoiceFilename,'F');
        $accountBillsModel->invoicepdfcreated = 1;
        $accountBillsModel->emailstatus = '-';

        $accountBillsModel->save();
        if ($nodisplay)
        {
           Yii::app()->user->setFlash('notice'.$this->i,"PDF $invoiceFilename created.");
           $this->i = $this->i + 1;
        } else
        {
            Yii::app()->user->setFlash('notice',"PDF $invoiceFilename created.");

            $this->redirect(array('/accountBills/view','id'=>$id));
        }
    }
    public $i=1;
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new AccountBills;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['AccountBills']))
		{
			$model->attributes=$_POST['AccountBills'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['AccountBills']))
		{
			$model->attributes=$_POST['AccountBills'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('AccountBills');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new AccountBills('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['AccountBills']))
			$model->attributes=$_GET['AccountBills'];
        if (isset($_POST))
        {
            if (isset($_POST['AccountBills']))
            {
                $model->attributes=$_POST['AccountBills'];
            }
            if (isset($_POST['PeriodForm']))
            {
                $p = new PeriodForm();
                $p->attributes = $_POST['PeriodForm'];
                $model->batch_id = $p->batch_id;
                $this->redirect(array('/accountBills/admin','AccountBills'=>$model->attributes));
                return;
            }
            if (isset($_POST['actmode'])&& $_POST['actmode']=='createPdf')
            {
                if (!Yii::app()->user->checkAccess('createPdf'))
                    throw new CHttpException('403','Access denied to create pdf');
                set_time_limit(0); // this will take a while
                $ids = $_POST['autoId'];
                foreach ($ids as $id)
                {
                    $this->actionCreatePdf($id,1);
                }

            }
            if (isset($_POST['actmode'])&& $_POST['actmode']=='sendEmail')
            {
                if (!Yii::app()->user->checkAccess('createPdf'))
                    throw new CHttpException('403','Access denied to send email');
                set_time_limit(0); // this will take a while
                $ids = $_POST['autoId'];
                foreach ($ids as $id)
                {
                    $this->actionSendEmail($id,1);
                }

            }
            if (isset($_POST['actmode'])&& $_POST['actmode']=='sendGreeting')
            {
                if (!Yii::app()->user->checkAccess('createPdf'))
                    throw new CHttpException('403','Access denied to send email');
                set_time_limit(0); // this will take a while
                $ids = $_POST['autoId'];
                foreach ($ids as $id)
                {
                    $this->actionSendBlast2($id,1);
                }

            }
            if (isset($_POST['actmode'])&& $_POST['actmode']=='printAllSelected')
            {
                if (!Yii::app()->user->checkAccess('createPdf'))
                    throw new CHttpException('403','Access denied to create pdf');
                set_time_limit(0); // this will take a while
                $ids = $_POST['autoId'];
                //$dbCriteria = new CDbCriteria(); $dbCriteria->compare('batch_id',$model->batch_id);
                $dbCriteria = new CDbCriteria(); $dbCriteria->addInCondition('id',$ids);
                $accountBills = AccountBills::model()->findAll($dbCriteria);
                $this->layout = 'nomenu';
                $this->pageTitle = 'Billing Statement';

                $mPDF1 = Yii::app()->ePdf->mpdf('', 'A4');
                $mPDF1->SetFooter("|Page {PAGENO}|");
                $cntr = 0; $maxC = 1000;
                foreach ($accountBills as $accountBillsModel)
                {
                    $cntr++; if ($cntr>=$maxC) break;
                    if ($accountBillsModel->account_id > 20000000) // personal
                    {
                        
                        $bills = $accountBillsModel->bills;
                        if (empty($bills)) continue;
                        $firstBill = $bills[0];
                        $this->renderPersonalPdf($mPDF1, $firstBill->custnumber,$accountBillsModel->batch_id);
                    }
                    else
                        $this->renderCorporatePdf($mPDF1, $accountBillsModel,false,true);
                    $mPDF1->WriteHTML('<pagebreak resetpagenum="1" pagenumstyle="1" suppress="off" />');
                    
                }
                $mPDF1->Output();

            }
            $this->layout = '//layouts/column2';
            $this->pageTitle = 'Account Bills batch processing';

        }

        $this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return AccountBills the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		try
		{
			$model=AccountBills::model()->findByPk($id);
			if($model===null)
				throw new CHttpException(404,'The requested page does not exist.');
		}
		catch(CDbException $e)
		{
			throw new CHttpException(404,'The requested page does not exist.');
		}
		return $model;	
	}

	/**
	 * Performs the AJAX validation.
	 * @param AccountBills $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='account-bills-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
