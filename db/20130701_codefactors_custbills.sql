delimiter $$

CREATE TABLE `code_factor` (
  `code` varchar(45) NOT NULL,
  `factor` decimal(9,2) NOT NULL DEFAULT '1.00',
  PRIMARY KEY (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1$$

CREATE TABLE `cust_bills` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) DEFAULT NULL,
  `paydate` varchar(45) DEFAULT NULL,
  `paytime` varchar(45) DEFAULT NULL,
  `payamount` int(11) DEFAULT NULL,
  `paymethod` varchar(45) DEFAULT NULL,
  `custnumber` varchar(45) DEFAULT NULL,
  `paystatus` varchar(45) DEFAULT NULL,
  `cust_id` int(11) DEFAULT NULL,
  `batch_id` int(11) DEFAULT NULL,
  `billing_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=466 DEFAULT CHARSET=latin1$$

CREATE TABLE `cust_bills_2` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) DEFAULT NULL,
  `paydate` varchar(45) DEFAULT NULL,
  `paytime` varchar(45) DEFAULT NULL,
  `payamount` int(11) DEFAULT NULL,
  `paymethod` varchar(45) DEFAULT NULL,
  `custnumber` varchar(45) DEFAULT NULL,
  `paystatus` varchar(45) DEFAULT NULL,
  `cust_id` int(11) DEFAULT NULL,
  `batch_id` int(11) DEFAULT NULL,
  `billing_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1603 DEFAULT CHARSET=latin1$$


CREATE TABLE `billing_const` (
  `id` varchar(12) NOT NULL,
  `numvalue` decimal(9,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1$$


INSERT INTO `as2in1bill`.`code_factor`
(`code`,
`factor`)
VALUES
(
'TXN_DATA_PAY_PER_USE_CREDIT',
-1
)$$

INSERT INTO `as2in1bill`.`billing_const`
(`id`,
`numvalue`)
VALUES
(
'INTLROAMFACT',
1.25
)$$



