<?php
/* @var $this TRoamingController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Troamings',
);

$this->menu=array(
	array('label'=>'Create TRoaming', 'url'=>array('create')),
	array('label'=>'Manage TRoaming', 'url'=>array('admin')),
);
?>

<h1>Troamings</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
