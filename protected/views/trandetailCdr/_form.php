<?php
/* @var $this TrandetailCdrController */
/* @var $model TrandetailCdr */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'trandetail-cdr-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'batch_id'); ?>
		<?php echo $form->textField($model,'batch_id'); ?>
		<?php echo $form->error($model,'batch_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'custnumber'); ?>
		<?php echo $form->textField($model,'custnumber',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($model,'custnumber'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tcode'); ?>
		<?php echo $form->textField($model,'tcode',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($model,'tcode'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'charge'); ?>
		<?php echo $form->textField($model,'charge'); ?>
		<?php echo $form->error($model,'charge'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tstamp'); ?>
		<?php echo $form->textField($model,'tstamp'); ?>
		<?php echo $form->error($model,'tstamp'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ttype'); ?>
		<?php echo $form->textField($model,'ttype'); ?>
		<?php echo $form->error($model,'ttype'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'cust_id'); ?>
		<?php echo $form->textField($model,'cust_id'); ?>
		<?php echo $form->error($model,'cust_id'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->