<?php
/* @var $this NumberToProfileController */
/* @var $model NumberToProfile */

$this->breadcrumbs=array(
	'Number To Profiles'=>array('index'),
	$model->custnumber=>array('view','id'=>$model->custnumber),
	'Update',
);

$this->menu=array(
	array('label'=>'List NumberToProfile', 'url'=>array('index')),
	array('label'=>'Create NumberToProfile', 'url'=>array('create')),
	array('label'=>'View NumberToProfile', 'url'=>array('view', 'id'=>
	$model->custnumber)),
	array('label'=>'Manage NumberToProfile', 'url'=>array('admin')),
);
?>

<h1>Update NumberToProfile <?php echo  $model->custnumber ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>