<?php
/* @var $this TrandetailAdjController */
/* @var $model TrandetailAdj */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'batch_id'); ?>
		<?php echo $form->textField($model,'batch_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'custnumber'); ?>
		<?php echo $form->textField($model,'custnumber',array('size'=>45,'maxlength'=>45)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tcode'); ?>
		<?php echo $form->textField($model,'tcode',array('size'=>45,'maxlength'=>45)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'charge'); ?>
		<?php echo $form->textField($model,'charge'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tstamp'); ?>
		<?php echo $form->textField($model,'tstamp'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ttype'); ?>
		<?php echo $form->textField($model,'ttype'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'cust_id'); ?>
		<?php echo $form->textField($model,'cust_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'itemtext_id'); ?>
		<?php echo $form->textField($model,'itemtext_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'discount_flag'); ?>
		<?php echo $form->textField($model,'discount_flag'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->