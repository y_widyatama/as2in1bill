<?php

/**
 * This is the model class for table "numbertoprofile".
 *
 * The followings are the available columns in table 'numbertoprofile':
 * @property string $custnumber
 * @property integer $profile_id
 *
 * The followings are the available model relations:
 * @property CustProfile $profile
 * @property TRoaming[] $tRoamings
 * @property TTrandetail[] $tTrandetails
 * @property TVoice[] $tVoices
 */
class NumberToProfile extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return NumberToProfile the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'numbertoprofile';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('custnumber', 'required'),
			array('profile_id', 'numerical', 'integerOnly'=>true),
			array('custnumber', 'length', 'max'=>45),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('custnumber, profile_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'profile' => array(self::BELONGS_TO, 'CustProfile', 'profile_id'),
			'tRoamings' => array(self::HAS_MANY, 'TRoaming', 'custnumber'),
			'tSurcharges' => array(self::HAS_MANY, 'TSurcharge', 'custnumber'),
			'tAdjustments' => array(self::HAS_MANY, 'TAdjustment', 'custnumber'),

			'tTrandetails' => array(self::HAS_MANY, 'TTrandetail', 'custnumber'),

			'tVoices' => array(self::HAS_MANY, 'TVoice', 'custnumber', 'order'=>'ACODE asc,discount_flag'),
			'tSmses' => array(self::HAS_MANY, 'TSms', 'custnumber'),
		);
	}

	public function getInvoiceList($batchId)
	{
		$r = array();
	    foreach ($this->tVoices as $voiceItem)
		{
			if ($voiceItem->batch_id != $batchId) continue;
			$row  = new InvoiceItem();
			$row->charge = $voiceItem->charge_cent * 0.01;
			$row->itemname = $voiceItem->itemclass->itemname;
            if ($voiceItem->discount_flag) $row->itemname .= ' (discount) ';
			$row->groupname = $voiceItem->itemclass->groupname;
            if ($voiceItem->itemclass->hideme && ($voiceItem->charge_cent==0))
                continue;
			$r[] = $row;
			
		}
		foreach ($this->tRoamings as $voiceItem)
		{
			if ($voiceItem->batch_id != $batchId) continue;
			$row  = new InvoiceItem();
			$row->charge = $voiceItem->charge_cent * 0.01;
			$row->itemname = $voiceItem->itemclass->itemname;
			$row->groupname = $voiceItem->itemclass->groupname;
			$r[] = $row;
			
		}
		foreach ($this->tTrandetails as $voiceItem)
		{
			if ($voiceItem->batch_id != $batchId) continue;
			$row  = new InvoiceItem();
			$row->charge = $voiceItem->charge_cent * 0.01;
			if (!empty($voiceItem->itemtext_id)) {
				$row->itemname = $voiceItem->itemtext->itemname;
			} else
			$row->itemname = $voiceItem->itemclass->itemname;
            if ($voiceItem->discount_flag) $row->itemname .= ' (discount) ';
            $row->groupname = $voiceItem->itemclass->groupname;
            if ($voiceItem->itemclass->hideme && ($voiceItem->charge_cent==0))
                continue;
			$r[] = $row;
			
		}
		foreach ($this->tSmses as $voiceItem)
		{
			if ($voiceItem->batch_id != $batchId) continue;
			$row  = new InvoiceItem();
			$row->charge = $voiceItem->charge_cent * 0.01;
			$row->itemname = $voiceItem->itemclass->itemname;
			$row->groupname = $voiceItem->itemclass->groupname;
            if ($voiceItem->itemclass->hideme && ($voiceItem->charge_cent==0))
                continue;
			$r[] = $row;
			
		}
		foreach ($this->tSurcharges as $voiceItem)
		{
			if ($voiceItem->batch_id != $batchId) continue;
			$row  = new InvoiceItem();
			$row->charge = $voiceItem->charge_cent * 0.01;
			$row->itemname = $voiceItem->itemclass->itemname;
			$row->groupname = $voiceItem->itemclass->groupname;
			$r[] = $row;
			
		}
		foreach ($this->tAdjustments as $voiceItem)
		{
			if ($voiceItem->batch_id != $batchId) continue;
			$row  = new InvoiceItem();
			$row->charge = $voiceItem->charge_cent * 0.01;
			if (!empty($voiceItem->itemtext_id)) {
				$row->itemname = $voiceItem->itemtext->itemname;
			} else
			$row->itemname = $voiceItem->itemclass->itemname;
            if ($voiceItem->discount_flag) $row->itemname .= ' (discount) ';
            $row->groupname = $voiceItem->itemclass->groupname;
            if ($voiceItem->itemclass->hideme && ($voiceItem->charge_cent==0))
                continue;
			$r[] = $row;
			
		}
		return $r;
	}
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'custnumber' => 'Custnumber',
			'profile_id' => 'Profile',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('custnumber',$this->custnumber,true);
		$criteria->compare('profile_id',$this->profile_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    public function getCustBill($batchId)
    {
        return
        CustBills::model()->findByAttributes(
            array('batch_id'=>$batchId, 'custnumber'=>$this->custnumber)
        );
    }

    public function getMininumPayment(){
        $custContract = CustContract::model()->findByAttributes(array('cust_id'=>$this->profile_id));
        $custGroup = CustGroup::model()->findByAttributes(array('cust_id'=>$this->profile_id));
        if (!empty($custGroup)) $groupContract = GroupContract::model()->findByAttributes(array('group_id'=>$custGroup->group_id));
        if(!empty($custContract)) return $custContract->minimumpayment;
        elseif( !empty($custGroup) && !empty($groupContract)) return $groupContract->minimum_usage;
        return 100;
    }
}
