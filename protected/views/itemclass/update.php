<?php
/* @var $this ItemclassController */
/* @var $model Itemclass */

$this->breadcrumbs=array(
	'Itemclasses'=>array('index'),
	$model->acode=>array('view','id'=>$model->acode),
	'Update',
);

$this->menu=array(
	array('label'=>'List Itemclass', 'url'=>array('index')),
	array('label'=>'Create Itemclass', 'url'=>array('create')),
	array('label'=>'View Itemclass', 'url'=>array('view', 'id'=>
	$model->acode)),
	array('label'=>'Manage Itemclass', 'url'=>array('admin')),
);
?>

<h1>Update Itemclass <?php echo  $model->acode ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>