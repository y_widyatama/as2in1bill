<?php
/* @var $this TTrandetailController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Ttrandetails',
);

$this->menu=array(
	array('label'=>'Create TTrandetail', 'url'=>array('create')),
	array('label'=>'Manage TTrandetail', 'url'=>array('admin')),
);
?>

<h1>Ttrandetails</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
