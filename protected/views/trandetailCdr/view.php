<?php
/* @var $this TrandetailCdrController */
/* @var $model TrandetailCdr */

$this->breadcrumbs=array(
	'Trandetail Cdrs'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List TrandetailCdr', 'url'=>array('index')),
	array('label'=>'Create TrandetailCdr', 'url'=>array('create')),
	array('label'=>'Update TrandetailCdr', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete TrandetailCdr', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage TrandetailCdr', 'url'=>array('admin')),
);
?>

<h1>View TrandetailCdr #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'batch_id',
		'custnumber',
		'tcode',
		'charge',
		'tstamp',
		'ttype',
		'cust_id',
	),
)); ?>
