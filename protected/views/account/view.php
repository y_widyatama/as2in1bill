<?php
/* @var $this AccountController */
/* @var $model Account */

$this->breadcrumbs=array(
	'Accounts'=>array('index'),
	$model->name,
);

$this->menu=array(
    array('label'=>'Billing for Account', 'url'=>array('viewInvoice','id'=>$model->id)),
	array('label'=>'Create Account', 'url'=>array('create')),
    array('label'=>'Update Account', 'url'=>array('update',
	   'id'=>$model->id)),
	array('label'=>'Delete Account', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Account', 'url'=>array('admin')),
);
?>

<h1>View Account #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
	),
)); ?>
<br><br>
<h3>Customers</h3>
<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'cust-profile-grid',
    'dataProvider'=>$profileListProvider,
//    'filter'=>$model,
    'columns'=>array(
        'id',
        'custnumber',
        'custname',
        'custaddr',
        'custemail',
        'createdate',
//        'corporatename',
        /*
		'title',
		'cnumber2',
		'createdate',
		'statecode',
		'termdate',
		*/
        array(
            'class'=>'CButtonColumn',
            'template'=>'{view}','viewButtonUrl' =>'array("custProfile/view","id"=>$data->id)'
        ),
    ),
));