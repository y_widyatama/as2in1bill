<?php
/* @var $this CustBillsController */
/* @var $model CustBills */

$this->breadcrumbs=array(
	'Cust Bills'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List CustBills', 'url'=>array('index')),
//	array('label'=>'Create CustBills', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#cust-bills-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Cust Bills</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'cust-bills-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'account_id',
        'custnumber',
        'billing_id',
        'chargeamount',
		'payamount',
		'paystatus',
        'batch_id',
		/*
		'custnumber',
		'paystatus',
		'cust_id',
		'batch_id',
		'billdate',
		'prevbalance',
		'prevpayment',
		'newbalance',
		'prevodd',
		'newodd',
		'invoiceround',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
