<?php
/* @var $this TrandetailAdjController */
/* @var $model TrandetailAdj */

$this->breadcrumbs=array(
	'Trandetail Adjs'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List TrandetailAdj', 'url'=>array('index')),
	array('label'=>'Manage TrandetailAdj', 'url'=>array('admin')),
);
?>

<h1>Create TrandetailAdj</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>