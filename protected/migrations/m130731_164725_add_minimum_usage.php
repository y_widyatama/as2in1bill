<?php

class m130731_164725_add_minimum_usage extends CDbMigration
{
	public function up()
	{
        $this->addColumn('cust_profile','minimum_usage','decimal DEFAULT 100');
	}

	public function down()
	{
        $this->dropColumn('cust_profile','minimum_usage');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}