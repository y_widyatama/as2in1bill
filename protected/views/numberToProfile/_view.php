<?php
/* @var $this NumberToProfileController */
/* @var $data NumberToProfile */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('custnumber')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->custnumber), array('view', 'id'=>$data->custnumber)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('profile_id')); ?>:</b>
	<?php echo CHtml::encode($data->profile_id); ?>
	<br />


</div>