<?php
/* @var $this ReportController */

$this->breadcrumbs=array(
	'Account'=>array('/account'),
	$model->name,
);
?>
<h1><?php echo $this->id . '/' . $this->action->id; ?></h1>
<?php $this->widget('zii.widgets.CDetailView', array(
    'data'=>$model,
    'attributes'=>array(
        'id',
        'name',
    ),
)); ?>
<br><br>

<?php


$this->renderPartial('/periodform',
    array('model'=>$pmodel,'actionUrl'=>array('','id'=>$model->id),
        'method'=>'get','autosubmit'=>false,'withAll'=>false));
?>

<h3>Customers</h3>
<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'cust-profile-grid',
    'dataProvider'=>$profileListProvider,
//    'filter'=>$model,
    'columns'=>array(
        'id',
        'custnumber',
        'custname',
        'custaddr',
        'custemail',
        'createdate',
//        'corporatename',
        /*
		'title',
		'cnumber2',
		'createdate',
		'statecode',
		'termdate',
		*/
        array(
            'class'=>'CButtonColumn',
            'template'=>'{view}','viewButtonUrl' =>'array("custProfile/view","id"=>$data->id)'
        ),
    ),
)); ?>