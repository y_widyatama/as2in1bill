<?php
/* @var $this TrandetailAdjController */
/* @var $model TrandetailAdj */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'trandetail-adj-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'batch_id'); ?>
		<?php echo $form->textField($model,'batch_id'); ?>
		<?php echo $form->error($model,'batch_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'custnumber'); ?>
		<?php echo $form->textField($model,'custnumber',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($model,'custnumber'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tcode'); ?>
		<?php echo $form->dropDownList($model,'tcode',Itemclass::getCombo()); ?>
		<?php echo $form->error($model,'tcode'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'charge'); ?>
		<?php echo $form->textField($model,'charge'); ?>
		<?php echo $form->error($model,'charge'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tstamp'); ?>
		<?php echo $form->textField($model,'tstamp'); ?>
		<?php echo $form->error($model,'tstamp'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ttype'); ?>
		<?php echo $form->textField($model,'ttype'); ?>
		<?php echo $form->error($model,'ttype'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'cust_id'); ?>
		<?php echo $form->textField($model,'cust_id'); ?>
		<?php echo $form->error($model,'cust_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'itemtext_id'); ?>
		<?php echo $form->textField($model,'itemtext_id'); ?>
		<?php echo $form->error($model,'itemtext_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'discount_flag'); ?>
		<?php echo $form->textField($model,'discount_flag'); ?>
		<?php echo $form->error($model,'discount_flag'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
