<?php
/* @var $this TrandetailAdjController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Trandetail Adjs',
);

$this->menu=array(
	array('label'=>'Create TrandetailAdj', 'url'=>array('create')),
	array('label'=>'Manage TrandetailAdj', 'url'=>array('admin')),
);
?>

<h1>Trandetail Adjs</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
