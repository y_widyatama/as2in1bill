<?php
$this->breadcrumbs=array(
	'Cust Discounts',
);

$this->menu=array(
	array('label'=>'Create CustDiscount', 'url'=>array('create')),
	array('label'=>'Manage CustDiscount', 'url'=>array('admin')),
);
?>

<h1>Cust Discounts</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
