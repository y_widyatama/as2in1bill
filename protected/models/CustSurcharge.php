<?php

/**
 * This is the model class for table "cust_surcharge".
 *
 * The followings are the available columns in table 'cust_surcharge':
 * @property integer $id
 * @property integer $customer_id
 * @property integer $startperiod
 * @property integer $monthcount
 * @property string $amount
 * @property string $permonth
 *
 * The followings are the available model relations:
 * @property CustProfile $customer
 * @property TSurcharge[] $tSurcharges
 */
class CustSurcharge extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CustSurcharge the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'cust_surcharge';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('customer_id, startperiod, monthcount', 'numerical', 'integerOnly'=>true),
			array('amount, permonth', 'length', 'max'=>9),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, customer_id, startperiod, monthcount, amount, permonth', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'customer' => array(self::BELONGS_TO, 'CustProfile', 'customer_id'),
			'tSurcharges' => array(self::HAS_MANY, 'TSurcharge', 'surcharge_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'customer_id' => 'Customer',
			'startperiod' => 'Startperiod',
			'monthcount' => 'Monthcount',
			'amount' => 'Amount',
			'permonth' => 'Permonth',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('customer_id',$this->customer_id);
		$criteria->compare('startperiod',$this->startperiod);
		$criteria->compare('monthcount',$this->monthcount);
		$criteria->compare('amount',$this->amount,true);
		$criteria->compare('permonth',$this->permonth,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	protected function beforeSave()
	{
	
		$flag = parent::beforeSave();
		if ($flag) {
			if (empty($this->permonth)) $this->permonth = $this->amount * 1.0 / ($this->monthcount);
			$listsurcharges = $this->tSurcharges;
			if (!empty($listsurcharges))
			foreach ($listsurcharges as $tsurcharge)
			{
				$tsurcharge->delete();
			}
		}
		return $flag;
	}
	protected function afterSave()
	{
		parent::afterSave();
		$cnt = $this->monthcount;
		$currentperiod = $this->startperiod;
		for ($i=0; $i<$cnt; $i++)
		{
			$t = new TSurcharge();
			$t->custnumber = $this->customer->custnumber;
			$t->charge_cent = floor($this->permonth * 100);
			$t->batch_id = $currentperiod;
			$t->acode = 'SURCHARGE';
            if ($t->charge_cent<0) $t->acode = 'REFUND';
			$t->surcharge_id = $this->id;
			$t->save();
			$currentperiod = $currentperiod + 1;
			if (($currentperiod % 100) == 13)
			{
			   $currentperiod = $currentperiod - 13 + 101;
			}
		}
	}
}