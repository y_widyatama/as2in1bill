<?php
/* @var $this CustSurchargeController */
/* @var $model CustSurcharge */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'cust-surcharge-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'customer_id'); ?>
		<?php echo $form->textField($model,'customer_id'); ?>
		<?php echo $form->error($model,'customer_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'startperiod'); ?>
		<?php echo $form->textField($model,'startperiod'); ?>
		<?php echo $form->error($model,'startperiod'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'monthcount'); ?>
		<?php echo $form->textField($model,'monthcount'); ?>
		<?php echo $form->error($model,'monthcount'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'amount'); ?>
		<?php echo $form->textField($model,'amount',array('size'=>9,'maxlength'=>9)); ?>
		<?php echo $form->error($model,'amount'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'permonth'); ?>
		<?php echo $form->textField($model,'permonth',array('size'=>9,'maxlength'=>9)); ?>
		<?php echo $form->error($model,'permonth'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->