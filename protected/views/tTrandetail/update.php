<?php
/* @var $this TTrandetailController */
/* @var $model TTrandetail */

$this->breadcrumbs=array(
	'Ttrandetails'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List TTrandetail', 'url'=>array('index')),
	array('label'=>'Create TTrandetail', 'url'=>array('create')),
	array('label'=>'View TTrandetail', 'url'=>array('view', 'id'=>
	$model->id)),
	array('label'=>'Manage TTrandetail', 'url'=>array('admin')),
);
?>

<h1>Update TTrandetail <?php echo  $model->id ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>