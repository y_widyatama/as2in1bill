<?php
/* @var $this TSurchargeController */
/* @var $model TSurcharge */

$this->breadcrumbs=array(
	'Tsurcharges'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List TSurcharge', 'url'=>array('index')),
	array('label'=>'Manage TSurcharge', 'url'=>array('admin')),
);
?>

<h1>Create TSurcharge</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>