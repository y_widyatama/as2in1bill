<?php
/* @var $this TSurchargeController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Tsurcharges',
);

$this->menu=array(
	array('label'=>'Create TSurcharge', 'url'=>array('create')),
	array('label'=>'Manage TSurcharge', 'url'=>array('admin')),
);
?>

<h1>Tsurcharges</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
