<?php
/* @var $this AccountBillsController */
/* @var $model AccountBills */

$this->breadcrumbs=array(
	'Account Bills'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List AccountBills', 'url'=>array('index')),
	array('label'=>'Create AccountBills', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#account-bills-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Account Bills</h1>


<?php
$pmodel = new PeriodForm();
$pmodel->batch_id = $model->batch_id;

$this->renderPartial('/periodform',array('model'=>$pmodel,'method'=>'post','actionUrl'=>array('/accountBills/admin',
    'AccountBills'=>$model->attributes)));
?>
<?php
foreach(Yii::app()->user->getFlashes() as $key => $message) {
    echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
}
?>

<?php
/* @var $form CActiveForm */

    $form=$this->beginWidget('CActiveForm', array(
    'action'=>array('/accountBills/admin','AccountBills'=>$model->attributes),
    'method'=>'post',
));

    $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'account-bills-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
    'ajaxUpdate'=>false,
	'columns'=>array(
		'id',
        array(
            'id'=>'autoId',
            'class'=>'CCheckBoxColumn',
            'selectableRows' => '50',
        ),
		'emailstatus',
        'invoicepdfcreated',
        'emailsent',
		'account_id',
        'account.name',
		'invoiceAmount',
		'batch_id',
		array(
			'class'=>'CButtonColumn',
            'template'=>'{view}',
		),
        array(
            'class'=>'CLinkColumn',
            'label'=>'PDF',
            'urlExpression'=>'$data->getInvoiceURL()',
        ),

        array(
            'class'=>'CLinkColumn',
            'label'=>'Bill Summary',
            'urlExpression'=>'$data->getInvoiceURL2()',
        ),

        array(
            'class'=>'CLinkColumn',
            'label'=>'Charge Details',
            'urlExpression'=>'$data->getInvoiceURL3()',
        ),
	),
));
echo CHtml::submitButton('OK',array('id'=>'actmode','name'=>'actmode'));
echo CHtml::submitButton('createPdf',array('id'=>'actmode','name'=>'actmode'));
echo CHtml::submitButton('sendEmail',array('id'=>'actmode','name'=>'actmode'));
echo CHtml::submitButton('printAllSelected',array('id'=>'actmode','name'=>'actmode'));
echo CHtml::submitButton('sendGreeting',array('id'=>'actmode','name'=>'actmode'));

$this->endWidget();

?>
