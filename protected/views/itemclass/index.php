<?php
/* @var $this ItemclassController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Itemclasses',
);

$this->menu=array(
	array('label'=>'Create Itemclass', 'url'=>array('create')),
	array('label'=>'Manage Itemclass', 'url'=>array('admin')),
);
?>

<h1>Itemclasses</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
