<?php
/* @var $this TSurchargeController */
/* @var $model TSurcharge */

$this->breadcrumbs=array(
	'Tsurcharges'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List TSurcharge', 'url'=>array('index')),
	array('label'=>'Create TSurcharge', 'url'=>array('create')),
	array('label'=>'Update TSurcharge', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete TSurcharge', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage TSurcharge', 'url'=>array('admin')),
);
?>

<h1>View TSurcharge #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'custnumber',
		'acode',
		'batch_id',
		'charge_cent',
		'id',
		'surcharge_id',
	),
)); ?>
