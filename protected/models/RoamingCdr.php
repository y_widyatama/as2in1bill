<?php

/**
 * This is the model class for table "roaming_cdr".
 *
 * The followings are the available columns in table 'roaming_cdr':
 * @property integer $id
 * @property string $tstamp
 * @property string $acode
 * @property string $custnumber
 * @property integer $duration
 * @property integer $charge_cent
 * @property string $loccode
 * @property integer $batch_id
 */
class RoamingCdr extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return RoamingCdr the static model class
	 */
	 
	private $actual_charge_cent;
	
	public function setActual_charge_cent()
	{
		//NULL
	}
	
	public function getActual_charge_cent()
	{
	  $this->saveUpChargeCent();
	  //return round($this->charge_cent*1.25,0);
	  return $this->up_charge_cent;
	}
	
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'roaming_cdr';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('duration, charge_cent, batch_id', 'numerical', 'integerOnly'=>true),
			array('acode, custnumber, loccode', 'length', 'max'=>45),
			array('tstamp', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, tstamp, acode, custnumber, duration, charge_cent, loccode, batch_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'custnumber0' => array(self::BELONGS_TO, 'NumberToProfile', 'custnumber'),
			'itemclass' => array(self::BELONGS_TO, 'Itemclass', 'acode'),
			'plmn' => array(self::BELONGS_TO, 'PlmnCode', 'loccode'),

		);
	}
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'tstamp' => 'Tstamp',
			'acode' => 'Acode',
			'custnumber' => 'Custnumber',
			'duration' => 'Duration',
			'charge_cent' => 'Charge Cent',
			'loccode' => 'Loccode',
			'batch_id' => 'Batch',
			
			'actual_charge_cent' => 'Charge Cent',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search($noPage=false)
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('tstamp',$this->tstamp,true);
		$criteria->compare('acode',$this->acode,true);
		$criteria->compare('custnumber',$this->custnumber,true);
		$criteria->compare('duration',$this->duration);
		$criteria->compare('charge_cent',$this->charge_cent);
		$criteria->compare('loccode',$this->loccode,true);
		$criteria->compare('batch_id',$this->batch_id);

		$p = array(
			'criteria'=>$criteria,
		);
		if ($noPage) $p['pagination'] = array('pageSize'=>100000);
		return new CActiveDataProvider($this, $p);
	}
	
	public function getSingle($ID_OBJECT){
		$model = new MovobjectClient();
		$datas = $model->findAllByAttributes(array('ID_OBJECT'=>$ID_OBJECT));
		foreach($datas as $data){
			return $data;
		}
		
		return null;
	}
	
	public function saveUpChargeCent(){		

		$this->up_charge_cent = round($this->charge_cent*1.25,0);
		$this->up_charge_cent_percent = 1.25;
		$this->up_charge_cent_by = Yii::app()->user->id;
		$this->up_charge_cent_by = Timeanddate::getCurrentDateTime();
		
		if($this->validate()){
			if($this->save()){
				//echo "save success";
				return true;
			}else{
				//echo "save failed";
				return false;
			}
		}else{
			//echo "validate fail";
			//echo CHtml::errorSummary($this);
			$errMsg[1] =  CHtml::errorSummary($this);
			Yii::app()->user->setFlash('errMsg',$errMsg);
			return false;
		}
	}
}