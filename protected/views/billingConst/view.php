<?php
/* @var $this BillingConstController */
/* @var $model BillingConst */

$this->breadcrumbs=array(
	'Billing Consts'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List BillingConst', 'url'=>array('index')),
	array('label'=>'Create BillingConst', 'url'=>array('create')),
	array('label'=>'Update BillingConst', 'url'=>array('update', 
	   'id'=>$model->id)),
	array('label'=>'Delete BillingConst', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage BillingConst', 'url'=>array('admin')),
);
?>

<h1>View BillingConst #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'numvalue',
	),
)); ?>
