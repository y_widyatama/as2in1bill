<?php
/* @var $this CustContractController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Cust Contracts',
);

$this->menu=array(
	array('label'=>'Create CustContract', 'url'=>array('create')),
	array('label'=>'Manage CustContract', 'url'=>array('admin')),
);
?>

<h1>Cust Contracts</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
