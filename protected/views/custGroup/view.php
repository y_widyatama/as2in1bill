<?php
/* @var $this CustGroupController */
/* @var $model CustGroup */

$this->breadcrumbs=array(
	'Cust Groups'=>array('index'),
	$model->id,
);

$this->menu=array(
//	array('label'=>'List CustGroup', 'url'=>array('index')),
//	array('label'=>'Create CustGroup', 'url'=>array('create')),
	array('label'=>'Update CustGroup', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete CustGroup', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage CustGroup', 'url'=>array('admin')),
	array('label'=>'Group', 'url'=>array('Group/view','id'=>$model->group_id)),
);
?>

<h1>View CustGroup #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
//		'id',
		'cust.custnumber',
		'group.name',
		'startperiod',
		'endperiod',
	),
)); ?>

<br/>
<h3>Customer Profile</h3>
<?php $this->widget('zii.widgets.CDetailView', array(
    'data'=>$model->cust,
    'attributes'=>array(
//        'id',
        'account_id',
        'custnumber',
        'custname',
        'custaddr',
        'custemail',
        'corporatename',
		'title',
		'cnumber2',
		'createdate',
		'statecode',
		'termdate',
        array(
            'class'=>'CButtonColumn',
        ),
    ),
)); ?>