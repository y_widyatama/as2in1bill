<?php
/* @var $this BillingStatusController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Billing Statuses',
);

$this->menu=array(
	array('label'=>'Create BillingStatus', 'url'=>array('create')),
	array('label'=>'Manage BillingStatus', 'url'=>array('admin')),
);
?>

<h1>Billing Statuses</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
