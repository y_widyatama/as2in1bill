<?php
/* @var $this GroupContractController */
/* @var $model GroupContract */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'group-contract-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'group_id'); ?>
		<?php echo $form->textField($model,'group_id'); ?>
		<?php echo $form->error($model,'group_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'minimum_usage'); ?>
		<?php echo $form->textField($model,'minimum_usage',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'minimum_usage'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'startperiod'); ?>
		<?php echo $form->textField($model,'startperiod'); ?>
		<?php echo $form->error($model,'startperiod'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'endperiod'); ?>
		<?php echo $form->textField($model,'endperiod'); ?>
		<?php echo $form->error($model,'endperiod'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'initial_grace'); ?>
		<?php echo $form->textField($model,'initial_grace'); ?>
		<?php echo $form->error($model,'initial_grace'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->