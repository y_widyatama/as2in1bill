<?php
/* @var $this CustPaymentController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Cust Payments',
);

$this->menu=array(
	array('label'=>'Create CustPayment', 'url'=>array('create')),
	array('label'=>'Manage CustPayment', 'url'=>array('admin')),
);
?>

<h1>Cust Payments</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
