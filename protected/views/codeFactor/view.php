<?php
/* @var $this CodeFactorController */
/* @var $model CodeFactor */

$this->breadcrumbs=array(
	'Code Factors'=>array('index'),
	$model->code,
);

$this->menu=array(
	array('label'=>'List CodeFactor', 'url'=>array('index')),
	array('label'=>'Create CodeFactor', 'url'=>array('create')),
	array('label'=>'Update CodeFactor', 'url'=>array('update', 
	   'id'=>$model->code)),
	array('label'=>'Delete CodeFactor', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->code),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage CodeFactor', 'url'=>array('admin')),
);
?>

<h1>View CodeFactor #<?php echo $model->code; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'code',
		'factor',
	),
)); ?>
