<?php
/* @var $this BillingConstController */
/* @var $data BillingConst */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('numvalue')); ?>:</b>
	<?php echo CHtml::encode($data->numvalue); ?>
	<br />


</div>