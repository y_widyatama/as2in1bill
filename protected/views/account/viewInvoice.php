<?php
/* @var $this AccountController */

$this->breadcrumbs=array(
	'Account'=>array('/account'),
	$model->name,
);
$this->menu=array(
    array('label'=>'Preview PDF', 'url'=>array('viewInvoice','id'=>$model->id,
        'PeriodForm'=>$pmodel->attributes,'isPdf'=>1)),
    array('label'=>'Send email', 'url'=>array('sendEmail','id'=>$model->id,
        'PeriodForm'=>$pmodel->attributes,'isPdf'=>1)),
    array('label'=>'Send email +Details', 'url'=>array('sendEmail','id'=>$model->id,
        'PeriodForm'=>$pmodel->attributes,'isPdf'=>1,'withDetails'=>1)),
);

if (empty($hidePeriod))
{

    $this->renderPartial('/periodform',
        array('model'=>$pmodel,'actionUrl'=>array('','id'=>$model->id),
            'method'=>'get','autosubmit'=>false,'withAll'=>false));
}
?>
<style type="text/css">
    .rightalign {text-align: right; }
    tr.rightalign td {text-align: right; }

    .grid-view table.items th { color: white; background: #6a6a7a no-repeat; }

</style>

<?php
$this->widget('zii.widgets.CDetailView', array(
    'data'=>$corporateSummary,
    'attributes'=>array(
        'invoiceNo',
        'accountId',
        'name',
        'addr',
    )
    ));
?>
<br>
<?php
$this->widget('zii.widgets.CDetailView', array(
    'data'=>$corporateSummary,
    'htmlOptions' => array('class'=>'zz'),
    'itemTemplate' =>"<tr class=\"{class}\"><th style='text-align:left; font-weight: normal;'>{label}</th><td style='width:10px'>:</td><td>{value}</td></tr>\n",
    'cssFile'=>false,
    'attributes'=>array(
        'periodStr',
        'customerService',
        'accountManager',

    ),
)); ?>
<h3>Statement Summary</h3>
<?php
$total = 0;
foreach ($bills as $bill)
{ $total += $bill->payamount; }
$total = $total * 0.01;

    $this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'bills-grid',
    'dataProvider'=>$billsProvider,
    'formatter'=>new MyFormatter(),
    'summaryText'=>'',
//    'filter'=>$model,
    'columns'=>array(
        array(
            'name'=>'cust.custnumber',
            'type'=>'text',
            'header'=>'Phone number',
            'footer'=>'Total for this period',
            ),
        'cust.custname:text:Name',
        array(
            //'name'=>'payamount',
            'header'=>'Amount',
            'value' => 'number_format($data->payamount * 0.01,2)',
            'footer'=>number_format($total,2),
            'footerHtmlOptions'=>array('style'=>'text-align: right; font-weight: bold'),
            'cssClassExpression'=>'"rightalign"',
        ),
        array(
            'name'=>'prevbalance',
            'header'=>'Prev Balance',
            'type'=>'cent',
            'cssClassExpression'=>'"rightalign"',
        ),
        array(
            'name'=>'prevpayment',
            'header'=>'Prev Payment',
            'type'=>'cent',
            'cssClassExpression'=>'"rightalign"',
        ),
        array(
            'name'=>'newbalance',
            'header'=>'New balance',
            'type'=>'cent',
            'cssClassExpression'=>'"rightalign"',
        ),

//        'corporatename',
    ),
)); ?>
<br/>

<?php
    $this->widget('zii.widgets.CDetailView', array(
    'data'=>$corporateSummary,
    'formatter'=>$corporateSummary->getFormatter(),
    'itemTemplate' =>"<tr class=\"{class}\"><th style='width: 250px;'>{label}</th><td>{value}</td></tr>\n",

        'attributes'=>array(
        array('name'=>'prevBalance',
            'type'=>'number',
            'cssClass'=>'rightalign',
        ),
        array('name'=>'prevPayment',
            'type'=>'number',
            'cssClass'=>'rightalign',
        ),
        array('name'=>'currentInvoice',
            'type'=>'number',
            'cssClass'=>'rightalign',
        ),

        array('name'=>'newBalance',
            'type'=>'number',
            'cssClass'=>'rightalign',
        ),
        array('name'=>
            'oddCents',
            'type'=>'number',
            'cssClass'=>'rightalign'
        ),
        array ('name'=>
        'roundBalance',
            'type'=>'number',
            'cssClass'=>'rightalign'
        ),
        array ('name'=>
        'payBefore',
            'cssClass'=>'rightalign'
            ),


    ),
));
?>
    <br/>
    <br/>
    <?php
$this->renderPartial('/numberToProfile/invoiceClosing');

?>
<?php
if (empty($summaryOnly) || ($summaryOnly==false) ) {
?>

<h3>CHARGE ITEMS</h3>
<?php
foreach ($profileList as $profile)
{
    $m = $profile->numbertoprofile;
    $this->renderPartial('/account/invoiceItemSummary',array('model'=>$m,'batchId'=>$pmodel->batch_id));
}

if (!empty($withDetails))
{
?>

<h3>CHARGE DETAILS</h3>
<?php
foreach ($profileList as $profile)
{
    $m = $profile->numbertoprofile;
    $this->renderPartial('/account/invoiceCallDetails',array('model'=>$m,'batchId'=>$pmodel->batch_id));
}
$this->renderPartial('/numberToProfile/invoiceClosing');
}
}
?>
<b>Reminder:</b><br>
Your Kartu As 2in1 card can roam in every GSM network operator world wide and the normal voice and sms roaming rates will applied.
For roaming in Indonesia, please make sure to manually set your network to TELKOMSEL in order to avoid high charge roaming from other local network operators.