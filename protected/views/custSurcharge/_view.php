<?php
/* @var $this CustSurchargeController */
/* @var $data CustSurcharge */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('customer_id')); ?>:</b>
	<?php echo CHtml::encode($data->customer_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('startperiod')); ?>:</b>
	<?php echo CHtml::encode($data->startperiod); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('monthcount')); ?>:</b>
	<?php echo CHtml::encode($data->monthcount); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('amount_cent')); ?>:</b>
	<?php echo CHtml::encode($data->amount_cent); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('permonth_cent')); ?>:</b>
	<?php echo CHtml::encode($data->permonth_cent); ?>
	<br />


</div>