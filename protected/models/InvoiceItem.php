<?php

/**
 * This is the model class for table "t_voice".
 *
 * The followings are the available columns in table 't_voice':
 * @property string $custnumber
 * @property integer $batch_id
 * @property string $acode
 * @property string $charge_cent
 * @property integer $id
 *
 * The followings are the available model relations:
 * @property Numbertoprofile $custnumber0
 */
class InvoiceItem extends CFormModel
{
	public $charge;
	public $itemname;
	public $groupname;
	public $id;
	
	static $idctr = 0;
	public function hasAttribute($name) 
	{
		$attrs = $this->attributeNames();
		if (array_search($name,$attrs)) return true; 
		return false;
	}
	public function getAttribute($name)
	{
	   if ($name == 'chargeStr') return $this->getChargeStr();
	   return $this->$name;
	}
	public function __construct()
	{
	   InvoiceItem::$idctr = InvoiceItem::$idctr+1;
	   $this->id = InvoiceItem::$idctr;
	}
	public function getChargeStr()
	{
		return number_format($this->charge,2);
	}
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('charge', 'numerical'),
			array('itemname, groupname', 'length', 'max'=>128),
			array('itemname, groupname', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'charge' => 'Charge',
			'itemname' => 'Item name',
			'groupname' => 'Group',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('groupname',$this->groupname,true);
		$criteria->compare('itemname',$this->itemname);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}