<?php
/* @var $this ItemclassController */
/* @var $model Itemclass */

$this->breadcrumbs=array(
	'Itemclasses'=>array('index'),
	$model->acode,
);

$this->menu=array(
	array('label'=>'List Itemclass', 'url'=>array('index')),
	array('label'=>'Create Itemclass', 'url'=>array('create')),
	array('label'=>'Update Itemclass', 'url'=>array('update', 
	   'id'=>$model->acode)),
	array('label'=>'Delete Itemclass', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->acode),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Itemclass', 'url'=>array('admin')),
);
?>

<h1>View Itemclass #<?php echo $model->acode; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'acode',
		'itemname',
		'groupname',
	),
)); ?>
