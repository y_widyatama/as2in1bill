<?php
/* @var $this ItemclassController */
/* @var $data Itemclass */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('acode')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->acode), array('view', 'id'=>$data->acode)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::encode($data->id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('itemname')); ?>:</b>
	<?php echo CHtml::encode($data->itemname); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('groupname')); ?>:</b>
	<?php echo CHtml::encode($data->groupname); ?>
	<br />


</div>