<?php

/**
 * This is the model class for table "group_discount".
 *
 * The followings are the available columns in table 'group_discount':
 * @property integer $id
 * @property integer $group_id
 * @property double $percentfactor
 * @property string $code
 * @property integer $startperiod
 * @property integer $endperiod
 *
 * The followings are the available model relations:
 * @property Itemclass $code0
 * @property Group $group
 */
class GroupDiscount extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return GroupDiscount the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'group_discount';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('code', 'required'),
			array('group_id, startperiod, endperiod', 'numerical', 'integerOnly'=>true),
			array('percentfactor', 'numerical'),
			array('code', 'length', 'max'=>45),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, group_id, percentfactor, code, startperiod, endperiod', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'code0' => array(self::BELONGS_TO, 'Itemclass', 'code'),
			'group' => array(self::BELONGS_TO, 'Group', 'group_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'group_id' => 'Group',
			'percentfactor' => 'Percentfactor',
			'code' => 'Code',
			'startperiod' => 'Startperiod',
			'endperiod' => 'Endperiod',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('group_id',$this->group_id);
		$criteria->compare('percentfactor',$this->percentfactor);
		$criteria->compare('code',$this->code,true);
		$criteria->compare('startperiod',$this->startperiod);
		$criteria->compare('endperiod',$this->endperiod);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}