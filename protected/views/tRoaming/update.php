<?php
/* @var $this TRoamingController */
/* @var $model TRoaming */

$this->breadcrumbs=array(
	'Troamings'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List TRoaming', 'url'=>array('index')),
	array('label'=>'Create TRoaming', 'url'=>array('create')),
	array('label'=>'View TRoaming', 'url'=>array('view', 'id'=>
	$model->id)),
	array('label'=>'Manage TRoaming', 'url'=>array('admin')),
);
?>

<h1>Update TRoaming <?php echo  $model->id ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>