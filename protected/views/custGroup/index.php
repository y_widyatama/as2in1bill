<?php
/* @var $this CustGroupController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Cust Groups',
);

$this->menu=array(
	array('label'=>'Create CustGroup', 'url'=>array('create')),
	array('label'=>'Manage CustGroup', 'url'=>array('admin')),
);
?>

<h1>Cust Groups</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
