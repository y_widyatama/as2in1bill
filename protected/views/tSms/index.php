<?php
/* @var $this TSmsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Tsms',
);

$this->menu=array(
	array('label'=>'Create TSms', 'url'=>array('create')),
	array('label'=>'Manage TSms', 'url'=>array('admin')),
);
?>

<h1>Tsms</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
