<?php
/* @var $this CustDiscountController */
/* @var $model CustDiscount */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'cust-discount-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
        <?php $this->widget('zii.widgets.CDetailView', array(
        	'data'=>$model->cust,
        	'attributes'=>array(
        		'account_id',
        		'custnumber',
        		'custname',
        		'custaddr',
        		'custemail',
        		'corporatename',
        		'title',
        	),
        ));
?>
		<?php echo $form->labelEx($model,'cust_id'); ?>
		<?php echo $form->textField($model,'cust_id'); ?>

		<?php echo $form->error($model,'cust_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'percentfactor'); ?>
		<?php echo $form->textField($model,'percentfactor'); ?>
		<?php echo $form->error($model,'percentfactor'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'code'); ?>
		<?php echo $form->dropDownList($model,'code',Itemclass::getCombo()); ?>
		<?php echo $form->error($model,'code'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'startperiod'); ?>
        <?php echo $form->dropDownList($model,'startperiod',PeriodForm::getPeriodList()); ?>
        <?php echo $form->error($model,'startperiod'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'endperiod'); ?>
        <?php echo $form->dropDownList($model,'endperiod',PeriodForm::getPeriodList()); ?>
        <?php echo $form->error($model,'endperiod'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->