<?php
/* @var $this TVoiceController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Tvoices',
);

$this->menu=array(
	array('label'=>'Create TVoice', 'url'=>array('create')),
	array('label'=>'Manage TVoice', 'url'=>array('admin')),
);
?>

<h1>Tvoices</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
