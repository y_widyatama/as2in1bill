<?php
/* @var $this AuthassignmentController */
/* @var $model Authassignment */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'authassignment-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'userid'); ?>
		<?php echo $form->textField($model,'userid',array('size'=>60,'value'=>$userid ,'maxlength'=>64, 'readonly'=>true)); ?>
		<?php echo $form->error($model,'userid'); ?>
	</div>
	
	<div class="row">
		<?php echo "<label>Authority</label>" ?>
		<?php echo CHtml::activeDropDownList($model, 'itemname', CHtml::listData(Authitem::model()->findAll(array( 'order' => 'name ASC')), 'name', 'name')); ?>
		<?php echo $form->error($model,'itemname'); ?>
	</div>



	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Add' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->