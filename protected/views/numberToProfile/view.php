<?php
/* @var $this NumberToProfileController */
/* @var $model NumberToProfile */

$this->breadcrumbs=array(
	'Number To Profiles'=>array('index'),
	$model->custnumber,
);
$pmodel = new PeriodForm();
if (isset($_REQUEST['PeriodForm']))
	$pmodel->attributes = $_REQUEST['PeriodForm'];

$this->menu=array(
	array('label'=>'Manage NumberToProfile', 'url'=>array('admin')),
	array('label'=>'Invoice', 'url'=>
	    array('numberToProfile/viewInvoice',
	    'id'=>$model->custnumber,
		'batchId'=>$pmodel->batch_id,
		)),
	array('label'=>'Invoice PDF', 'url'=>
	    array('numberToProfile/viewInvoicePdf',
	    'id'=>$model->custnumber,
		'batchId'=>$pmodel->batch_id,
		)),
	array('label'=>'Invoice Call details', 'url'=>
	    array('numberToProfile/viewInvoiceCallDetails',
	    'id'=>$model->custnumber,
		'batchId'=>$pmodel->batch_id,
		)),
	array('label'=>'Invoice Call details PDF', 'url'=>
	    array('numberToProfile/viewInvoiceCallDetailsPdf',
	    'id'=>$model->custnumber,
		'batchId'=>$pmodel->batch_id,
		)),
);
?>

<h1>View NumberToProfile #<?php echo $model->custnumber; ?></h1>
<?php
$this->renderPartial('/periodform',array('model'=>$pmodel,'actionUrl'=>array('/numberToProfile/view','id'=>$model->custnumber)));
?>
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'custnumber',
		'profile_id',
		'profile.custname',
		'profile.custaddr',
		'profile.corporatename',
		
	),
)); ?>
<h1>Billing Records</h1>
<?php
  $modelvoice = new TVoice();
  $modelvoice->custnumber = $model->custnumber; 
  if ($pmodel->batch_id) $modelvoice->batch_id = $pmodel->batch_id;
  ?>
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'tvoice-grid',
	'dataProvider'=>$modelvoice->search(),
	'filter'=>$modelvoice,
	'columns'=>array(
		'custnumber',
		'batch_id',
		'charge_cent',
		'itemclass.itemname',

	),
)); ?>

<?php
  $modelt = new TTrandetail();
  $modelt->custnumber = $model->custnumber; 
  if ($pmodel->batch_id) $modelt->batch_id = $pmodel->batch_id;

  ?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'ttrandetail-grid',
	'dataProvider'=>$modelt->search(),
	'filter'=>$modelt,
	'columns'=>array(
		'custnumber',
		'batch_id',
		'itemclass.itemname',
		'charge_cent',
		

	),
)); ?>

<?php

  $modelr = new TRoaming();
  $modelr->custnumber = $model->custnumber; 
  if ($pmodel->batch_id) $modelr->batch_id = $pmodel->batch_id;

  ?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'troaming-grid',
	'dataProvider'=>$modelr->search(),
	'filter'=>$modelr,
	'columns'=>array(
		'custnumber',
		'batch_id',
		'acode',
		'charge_cent',

	),
)); 
?>
<?php

  $models = new TSms();
  $models->custnumber = $model->custnumber; 
  if ($pmodel->batch_id) $models->batch_id = $pmodel->batch_id;

  ?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'tsms-grid',
	'dataProvider'=>$models->search(),
	'filter'=>$models,
	'columns'=>array(
		'custnumber',
		'itemclass.itemname',
		'batch_id',
		'charge_cent:number',
		
	),
)); ?>
<?php

   $total = 0;
   $d = $modelr->search();
   $d1 = $d->getData();
   foreach ($d1 as $roamingrecord)
     $total += $roamingrecord->charge_cent;
	
   $d = $modelt->search();
   $d1 = $d->getData();
   foreach ($d1 as $trecord)
     $total += $trecord->charge_cent;
		
   $d = $modelvoice->search();
   $d1 = $d->getData();
   foreach ($d1 as $vrecord)
     $total += $vrecord->charge_cent;

   $d = $models->search();
   $d1 = $d->getData();
   foreach ($d1 as $vrecord)
     $total += $vrecord->charge_cent;

?>
