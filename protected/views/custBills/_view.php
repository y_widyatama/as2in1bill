<?php
/* @var $this CustBillsController */
/* @var $data CustBills */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('account_id')); ?>:</b>
	<?php echo CHtml::encode($data->account_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('paydate')); ?>:</b>
	<?php echo CHtml::encode($data->paydate); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('paytime')); ?>:</b>
	<?php echo CHtml::encode($data->paytime); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('payamount')); ?>:</b>
	<?php echo CHtml::encode($data->payamount); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('paymethod')); ?>:</b>
	<?php echo CHtml::encode($data->paymethod); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('custnumber')); ?>:</b>
	<?php echo CHtml::encode($data->custnumber); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('paystatus')); ?>:</b>
	<?php echo CHtml::encode($data->paystatus); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cust_id')); ?>:</b>
	<?php echo CHtml::encode($data->cust_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('batch_id')); ?>:</b>
	<?php echo CHtml::encode($data->batch_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('billing_id')); ?>:</b>
	<?php echo CHtml::encode($data->billing_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('chargeamount')); ?>:</b>
	<?php echo CHtml::encode($data->chargeamount); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('billdate')); ?>:</b>
	<?php echo CHtml::encode($data->billdate); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('prevbalance')); ?>:</b>
	<?php echo CHtml::encode($data->prevbalance); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('prevpayment')); ?>:</b>
	<?php echo CHtml::encode($data->prevpayment); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('newbalance')); ?>:</b>
	<?php echo CHtml::encode($data->newbalance); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('prevodd')); ?>:</b>
	<?php echo CHtml::encode($data->prevodd); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('newodd')); ?>:</b>
	<?php echo CHtml::encode($data->newodd); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('invoiceround')); ?>:</b>
	<?php echo CHtml::encode($data->invoiceround); ?>
	<br />

	*/ ?>

</div>