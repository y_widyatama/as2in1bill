<?php
/* @var $this TSmsController */
/* @var $model TSms */

$this->breadcrumbs=array(
	'Tsms'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List TSms', 'url'=>array('index')),
	array('label'=>'Create TSms', 'url'=>array('create')),
	array('label'=>'View TSms', 'url'=>array('view', 'id'=>
	$model->id)),
	array('label'=>'Manage TSms', 'url'=>array('admin')),
);
?>

<h1>Update TSms <?php echo  $model->id ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>