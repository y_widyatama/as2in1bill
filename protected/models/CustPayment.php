<?php

/**
 * This is the model class for table "cust_payment".
 *
 * The followings are the available columns in table 'cust_payment':
 * @property integer $id
 * @property string $custnumber
 * @property integer $billing_id
 * @property integer $batch_id
 * @property integer $payamount
 * @property string $paydate
 * @property integer $customer_id
 *
 * The followings are the available model relations:
 * @property CustProfile $customer
 * @property Numbertoprofile $custnumber0
 */
class CustPayment extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CustPayment the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'cust_payment';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('billing_id, batch_id, payamount, customer_id, ismanual', 'numerical', 'integerOnly'=>true),
			array('custnumber', 'length', 'max'=>45),
			array('paydate', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, custnumber, billing_id, batch_id, payamount, paydate, customer_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'customer' => array(self::BELONGS_TO, 'CustProfile', 'customer_id'),
			'custnumber0' => array(self::BELONGS_TO, 'Numbertoprofile', 'custnumber'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'custnumber' => 'Custnumber',
			'billing_id' => 'Billing',
			'batch_id' => 'Batch',
			'payamount' => 'Payamount',
			'paydate' => 'Paydate',
			'customer_id' => 'Customer',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('custnumber',$this->custnumber,true);
		$criteria->compare('billing_id',$this->billing_id);
		$criteria->compare('batch_id',$this->batch_id);
		$criteria->compare('payamount',$this->payamount);
		$criteria->compare('paydate',$this->paydate,true);
		$criteria->compare('customer_id',$this->customer_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}