<?php
/* @var $this CustSurchargeController */
/* @var $model CustSurcharge */

$this->breadcrumbs=array(
	'Cust Surcharges'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List CustSurcharge', 'url'=>array('index')),
	array('label'=>'Manage CustSurcharge', 'url'=>array('admin')),
);
?>

<h1>Create CustSurcharge for <?php echo $model->customer->custname; ?> </h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>