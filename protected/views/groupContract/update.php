<?php
/* @var $this GroupContractController */
/* @var $model GroupContract */

$this->breadcrumbs=array(
	'Group Contracts'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List GroupContract', 'url'=>array('index')),
	array('label'=>'Create GroupContract', 'url'=>array('create')),
	array('label'=>'View GroupContract', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage GroupContract', 'url'=>array('admin')),
);
?>

<h1>Update GroupContract <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>