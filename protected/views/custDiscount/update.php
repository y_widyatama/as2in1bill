<?php
$this->breadcrumbs=array(
	'Cust Discounts'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List CustDiscount', 'url'=>array('index')),
	array('label'=>'Create CustDiscount', 'url'=>array('create')),
	array('label'=>'View CustDiscount', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage CustDiscount', 'url'=>array('admin')),
);
?>

<h1>Update CustDiscount <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>