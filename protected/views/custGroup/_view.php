<?php
/* @var $this CustGroupController */
/* @var $data CustGroup */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cust_id')); ?>:</b>
	<?php echo CHtml::encode($data->cust_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('group_id')); ?>:</b>
	<?php echo CHtml::encode($data->group_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('startperiod')); ?>:</b>
	<?php echo CHtml::encode($data->startperiod); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('endperiod')); ?>:</b>
	<?php echo CHtml::encode($data->endperiod); ?>
	<br />


</div>