<?php
/* @var $this AccountBillsController */
/* @var $model AccountBills */

$this->breadcrumbs=array(
	'Account Bills'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List AccountBills', 'url'=>array('index')),
	array('label'=>'Manage AccountBills', 'url'=>array('admin')),
);
?>

<h1>Create AccountBills</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>