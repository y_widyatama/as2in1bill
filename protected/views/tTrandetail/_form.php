<?php
/* @var $this TTrandetailController */
/* @var $model TTrandetail */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'ttrandetail-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'custnumber'); ?>
		<?php echo $form->textField($model,'custnumber',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($model,'custnumber'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'batch_id'); ?>
		<?php echo $form->textField($model,'batch_id'); ?>
		<?php echo $form->error($model,'batch_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tcode'); ?>
		<?php echo $form->textField($model,'tcode',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($model,'tcode'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'charge_cent'); ?>
		<?php echo $form->textField($model,'charge_cent',array('size'=>32,'maxlength'=>32)); ?>
		<?php echo $form->error($model,'charge_cent'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->