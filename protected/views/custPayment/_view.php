<?php
/* @var $this CustPaymentController */
/* @var $data CustPayment */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('custnumber')); ?>:</b>
	<?php echo CHtml::encode($data->custnumber); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('billing_id')); ?>:</b>
	<?php echo CHtml::encode($data->billing_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('batch_id')); ?>:</b>
	<?php echo CHtml::encode($data->batch_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('payamount')); ?>:</b>
	<?php echo CHtml::encode($data->payamount); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('paydate')); ?>:</b>
	<?php echo CHtml::encode($data->paydate); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('customer_id')); ?>:</b>
	<?php echo CHtml::encode($data->customer_id); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('ismanual')); ?>:</b>
	<?php echo CHtml::encode($data->ismanual); ?>
	<br />

	*/ ?>

</div>