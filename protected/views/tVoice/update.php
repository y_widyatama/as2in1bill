<?php
/* @var $this TVoiceController */
/* @var $model TVoice */

$this->breadcrumbs=array(
	'Tvoices'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List TVoice', 'url'=>array('index')),
	array('label'=>'Create TVoice', 'url'=>array('create')),
	array('label'=>'View TVoice', 'url'=>array('view', 'id'=>
	$model->id)),
	array('label'=>'Manage TVoice', 'url'=>array('admin')),
);
?>

<h1>Update TVoice <?php echo  $model->id ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>