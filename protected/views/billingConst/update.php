<?php
/* @var $this BillingConstController */
/* @var $model BillingConst */

$this->breadcrumbs=array(
	'Billing Consts'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List BillingConst', 'url'=>array('index')),
	array('label'=>'Create BillingConst', 'url'=>array('create')),
	array('label'=>'View BillingConst', 'url'=>array('view', 'id'=>
	$model->id)),
	array('label'=>'Manage BillingConst', 'url'=>array('admin')),
);
?>

<h1>Update BillingConst <?php echo  $model->id ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>