<?php
/* @var $this CustContractController */
/* @var $model CustContract */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'cust-contract-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'startperiod'); ?>
		<?php echo $form->dropDownList($model,'startperiod',CustContract::getPeriodList()); ?>
		<?php echo $form->error($model,'startperiod'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'endperiod'); ?>
		<?php echo $form->dropDownList($model,'endperiod',CustContract::getPeriodList()); ?>
		<?php echo $form->error($model,'endperiod'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'minimumpayment'); ?>
		<?php echo $form->textField($model,'minimumpayment'); ?>
		<?php echo $form->error($model,'minimumpayment'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'cust_id'); ?>
		<?php echo $form->dropDownList($model,'cust_id', CustProfile::getProfileList()); ?>
		<?php echo $form->error($model,'cust_id'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->