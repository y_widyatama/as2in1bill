-- MySQL dump 10.13  Distrib 5.5.9, for Win32 (x86)
--
-- Host: 127.0.0.1    Database: as2in1bill
-- ------------------------------------------------------
-- Server version	5.1.35-community

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `as2in1bill`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `as2in1bill` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `as2in1bill`;

--
-- Table structure for table `bill01`
--

DROP TABLE IF EXISTS `bill01`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bill01` (
  `id` int(11) NOT NULL DEFAULT '0',
  `account_id` int(11) DEFAULT NULL,
  `custnumber` varchar(45) DEFAULT NULL,
  `custname` varchar(128) DEFAULT NULL,
  `custaddr` longtext,
  `custemail` varchar(64) DEFAULT NULL,
  `corporatename` varchar(100) DEFAULT NULL,
  `title` varchar(45) DEFAULT NULL,
  `cnumber2` varchar(45) DEFAULT NULL,
  `createdate` varchar(45) DEFAULT NULL,
  `statecode` varchar(45) DEFAULT NULL,
  `termdate` varchar(45) DEFAULT NULL,
  `batch_id` int(11) DEFAULT NULL,
  `itemname` varchar(128) DEFAULT NULL,
  `groupname` varchar(128) DEFAULT NULL,
  `charge_cent` decimal(32,0) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary table structure for view `bill02`
--

DROP TABLE IF EXISTS `bill02`;
/*!50001 DROP VIEW IF EXISTS `bill02`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `bill02` (
  `id` int(11),
  `account_id` int(11),
  `custnumber` varchar(45),
  `custname` varchar(128),
  `custaddr` longtext,
  `custemail` varchar(64),
  `corporatename` varchar(100),
  `title` varchar(45),
  `cnumber2` varchar(45),
  `createdate` varchar(45),
  `statecode` varchar(45),
  `termdate` varchar(45),
  `batch_id` int(11),
  `itemname` varchar(128),
  `groupname` varchar(128),
  `charge_cent` decimal(35,2)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `cust_profile`
--

DROP TABLE IF EXISTS `cust_profile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cust_profile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) DEFAULT NULL,
  `custnumber` varchar(45) DEFAULT NULL,
  `custname` varchar(128) DEFAULT NULL,
  `custaddr` varchar(1000) DEFAULT NULL,
  `custemail` varchar(64) DEFAULT NULL,
  `corporatename` varchar(100) DEFAULT NULL,
  `title` varchar(45) DEFAULT NULL,
  `cnumber2` varchar(45) DEFAULT NULL,
  `createdate` varchar(45) DEFAULT NULL,
  `statecode` varchar(45) DEFAULT NULL,
  `termdate` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1000 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `events_cdr`
--

DROP TABLE IF EXISTS `events_cdr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `events_cdr` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `batch_id` int(11) DEFAULT NULL,
  `file_id` int(11) DEFAULT NULL,
  `upload_type` int(11) DEFAULT NULL,
  `cust_id` int(11) DEFAULT NULL,
  `itemclass_id` int(11) DEFAULT NULL,
  `charge` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `itemclass`
--

DROP TABLE IF EXISTS `itemclass`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `itemclass` (
  `id` int(11) NOT NULL,
  `acode` varchar(45) NOT NULL,
  `itemname` varchar(128) DEFAULT NULL,
  `groupname` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`acode`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `numbertoprofile`
--

DROP TABLE IF EXISTS `numbertoprofile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `numbertoprofile` (
  `custnumber` varchar(45) NOT NULL,
  `profile_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`custnumber`),
  KEY `fk_001` (`profile_id`),
  CONSTRAINT `fk_001` FOREIGN KEY (`profile_id`) REFERENCES `cust_profile` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `roaming_cdr`
--

DROP TABLE IF EXISTS `roaming_cdr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roaming_cdr` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tstamp` datetime DEFAULT NULL,
  `acode` varchar(45) DEFAULT NULL,
  `custnumber` varchar(45) DEFAULT NULL,
  `duration` int(11) DEFAULT NULL,
  `charge_cent` int(11) DEFAULT NULL,
  `loccode` varchar(45) DEFAULT NULL,
  `batch_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=277 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sms_cdr`
--

DROP TABLE IF EXISTS `sms_cdr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sms_cdr` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `custnumber` varchar(45) DEFAULT NULL,
  `acode` varchar(45) DEFAULT NULL,
  `tstamp` datetime DEFAULT NULL,
  `destnumber` varchar(45) DEFAULT NULL,
  `charge_cent` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `smflag` int(11) DEFAULT NULL,
  `batch_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18020 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `t_roaming`
--

DROP TABLE IF EXISTS `t_roaming`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_roaming` (
  `custnumber` varchar(45) DEFAULT NULL,
  `batch_id` int(11) DEFAULT NULL,
  `acode` varchar(45) DEFAULT NULL,
  `charge_cent` decimal(32,0) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  KEY `fk_003` (`custnumber`),
  CONSTRAINT `fk_003` FOREIGN KEY (`custnumber`) REFERENCES `numbertoprofile` (`custnumber`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `t_sms`
--

DROP TABLE IF EXISTS `t_sms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_sms` (
  `custnumber` varchar(45) DEFAULT NULL,
  `acode` varchar(45) DEFAULT NULL,
  `batch_id` int(11) DEFAULT NULL,
  `charge_cent` decimal(32,0) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  KEY `fk_009` (`custnumber`),
  CONSTRAINT `fk_009` FOREIGN KEY (`custnumber`) REFERENCES `numbertoprofile` (`custnumber`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=685 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `t_trandetail`
--

DROP TABLE IF EXISTS `t_trandetail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_trandetail` (
  `custnumber` varchar(45) DEFAULT NULL,
  `batch_id` int(11) DEFAULT NULL,
  `tcode` varchar(45) DEFAULT NULL,
  `charge_cent` decimal(32,0) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  KEY `fk_002` (`custnumber`),
  CONSTRAINT `fk_002` FOREIGN KEY (`custnumber`) REFERENCES `numbertoprofile` (`custnumber`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=319 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `t_voice`
--

DROP TABLE IF EXISTS `t_voice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_voice` (
  `custnumber` varchar(45) DEFAULT NULL,
  `batch_id` int(11) DEFAULT NULL,
  `acode` varchar(45) DEFAULT NULL,
  `charge_cent` decimal(32,0) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  KEY `fk_number` (`custnumber`),
  CONSTRAINT `fk_number` FOREIGN KEY (`custnumber`) REFERENCES `numbertoprofile` (`custnumber`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1776 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `trandetail_cdr`
--

DROP TABLE IF EXISTS `trandetail_cdr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trandetail_cdr` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `batch_id` int(11) DEFAULT NULL,
  `custnumber` varchar(45) DEFAULT NULL,
  `tcode` varchar(45) DEFAULT NULL,
  `charge` int(11) DEFAULT NULL,
  `tstamp` datetime DEFAULT NULL,
  `ttype` int(11) DEFAULT NULL,
  `cust_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=548 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary table structure for view `v_roaming`
--

DROP TABLE IF EXISTS `v_roaming`;
/*!50001 DROP VIEW IF EXISTS `v_roaming`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `v_roaming` (
  `custnumber` varchar(45),
  `batch_id` int(11),
  `acode` varchar(45),
  `charge_cent` decimal(32,0)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `v_sms`
--

DROP TABLE IF EXISTS `v_sms`;
/*!50001 DROP VIEW IF EXISTS `v_sms`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `v_sms` (
  `custnumber` varchar(45),
  `acode` varchar(45),
  `batch_id` int(11),
  `charge_cent` decimal(32,0)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `v_trandetail`
--

DROP TABLE IF EXISTS `v_trandetail`;
/*!50001 DROP VIEW IF EXISTS `v_trandetail`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `v_trandetail` (
  `custnumber` varchar(45),
  `batch_id` int(11),
  `tcode` varchar(45),
  `charge_cent` decimal(32,0)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `v_voice`
--

DROP TABLE IF EXISTS `v_voice`;
/*!50001 DROP VIEW IF EXISTS `v_voice`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `v_voice` (
  `custnumber` varchar(45),
  `batch_id` int(11),
  `acode` varchar(45),
  `charge_cent` decimal(32,0)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `voice_cdr`
--

DROP TABLE IF EXISTS `voice_cdr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `voice_cdr` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `batch_id` int(11) DEFAULT NULL,
  `start_time` datetime DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  `acode` varchar(45) DEFAULT NULL,
  `custnumber` varchar(45) DEFAULT NULL,
  `peernumber` varchar(45) DEFAULT NULL,
  `duration` int(11) DEFAULT NULL,
  `charge_cent` int(11) DEFAULT NULL,
  `chargerate_cent` int(11) DEFAULT NULL,
  `calltype_id` int(11) DEFAULT NULL,
  `cust_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=54028 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Current Database: `as2in1bill`
--

USE `as2in1bill`;

--
-- Final view structure for view `bill02`
--

/*!50001 DROP TABLE IF EXISTS `bill02`*/;
/*!50001 DROP VIEW IF EXISTS `bill02`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `bill02` AS select `bill01`.`id` AS `id`,`bill01`.`account_id` AS `account_id`,`bill01`.`custnumber` AS `custnumber`,`bill01`.`custname` AS `custname`,`bill01`.`custaddr` AS `custaddr`,`bill01`.`custemail` AS `custemail`,`bill01`.`corporatename` AS `corporatename`,`bill01`.`title` AS `title`,`bill01`.`cnumber2` AS `cnumber2`,`bill01`.`createdate` AS `createdate`,`bill01`.`statecode` AS `statecode`,`bill01`.`termdate` AS `termdate`,`bill01`.`batch_id` AS `batch_id`,`bill01`.`itemname` AS `itemname`,`bill01`.`groupname` AS `groupname`,(`bill01`.`charge_cent` * 0.01) AS `charge_cent` from `bill01` where (`bill01`.`charge_cent` > 0) order by `bill01`.`batch_id`,`bill01`.`account_id`,`bill01`.`custnumber` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_roaming`
--

/*!50001 DROP TABLE IF EXISTS `v_roaming`*/;
/*!50001 DROP VIEW IF EXISTS `v_roaming`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `v_roaming` AS select `roaming_cdr`.`custnumber` AS `custnumber`,`roaming_cdr`.`batch_id` AS `batch_id`,`roaming_cdr`.`acode` AS `acode`,sum(`roaming_cdr`.`charge_cent`) AS `charge_cent` from `roaming_cdr` group by `roaming_cdr`.`custnumber`,`roaming_cdr`.`batch_id`,`roaming_cdr`.`acode` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_sms`
--

/*!50001 DROP TABLE IF EXISTS `v_sms`*/;
/*!50001 DROP VIEW IF EXISTS `v_sms`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `v_sms` AS select `sms_cdr`.`custnumber` AS `custnumber`,`sms_cdr`.`acode` AS `acode`,`sms_cdr`.`batch_id` AS `batch_id`,sum(`sms_cdr`.`charge_cent`) AS `charge_cent` from `sms_cdr` group by `sms_cdr`.`custnumber`,`sms_cdr`.`acode`,`sms_cdr`.`batch_id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_trandetail`
--

/*!50001 DROP TABLE IF EXISTS `v_trandetail`*/;
/*!50001 DROP VIEW IF EXISTS `v_trandetail`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `v_trandetail` AS select `trandetail_cdr`.`custnumber` AS `custnumber`,`trandetail_cdr`.`batch_id` AS `batch_id`,`trandetail_cdr`.`tcode` AS `tcode`,sum(`trandetail_cdr`.`charge`) AS `charge_cent` from `trandetail_cdr` group by `trandetail_cdr`.`batch_id`,`trandetail_cdr`.`custnumber`,`trandetail_cdr`.`tcode` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_voice`
--

/*!50001 DROP TABLE IF EXISTS `v_voice`*/;
/*!50001 DROP VIEW IF EXISTS `v_voice`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `v_voice` AS select `voice_cdr`.`custnumber` AS `custnumber`,`voice_cdr`.`batch_id` AS `batch_id`,`voice_cdr`.`acode` AS `acode`,sum(`voice_cdr`.`charge_cent`) AS `charge_cent` from `voice_cdr` group by `voice_cdr`.`custnumber`,`voice_cdr`.`batch_id`,`voice_cdr`.`acode` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-06-24 17:19:29
