<?php
/* @var $this CustProfileController */
/* @var $data CustProfile */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('account_id')); ?>:</b>
	<?php echo CHtml::encode($data->account_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('custnumber')); ?>:</b>
	<?php echo CHtml::encode($data->custnumber); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('custname')); ?>:</b>
	<?php echo CHtml::encode($data->custname); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('custaddr')); ?>:</b>
	<?php echo CHtml::encode($data->custaddr); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('custemail')); ?>:</b>
	<?php echo CHtml::encode($data->custemail); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('corporatename')); ?>:</b>
	<?php echo CHtml::encode($data->corporatename); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('title')); ?>:</b>
	<?php echo CHtml::encode($data->title); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cnumber2')); ?>:</b>
	<?php echo CHtml::encode($data->cnumber2); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('createdate')); ?>:</b>
	<?php echo CHtml::encode($data->createdate); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('statecode')); ?>:</b>
	<?php echo CHtml::encode($data->statecode); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('termdate')); ?>:</b>
	<?php echo CHtml::encode($data->termdate); ?>
	<br />

	*/ ?>

</div>