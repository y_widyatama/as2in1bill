<?php
/* @var $this NumberToProfileController */
/* @var $model NumberToProfile */
?>
<?php 
$pmodel = new PeriodForm();
$pmodel->batch_id = $batchId;
$this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'custnumber:text:Phone number',
		'profile.corporatename:text:Company/Group',
		'profile.title',
		'profile.custname:text:Name',
		'profile.custaddr:text:Address',
		'profile.custemail:text:Email',
		
	),
)); ?>
<style type="text/css">
.rightalign {text-align: right; }
.grid-view table.items th { color: white; background: #6a6a7a no-repeat; }
</style>

<?php
  $modelvoice = new VoiceCdr();
  $modelvoice->custnumber = $model->custnumber; 
  if ($pmodel->batch_id) $modelvoice->batch_id = $pmodel->batch_id;
  ?>
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'tvoice-grid',
	'dataProvider'=>$modelvoice->search(true),
	//'filter'=>$modelvoice,
	'enablePagination'=>false,
	'enableSorting'=>false,
	'summaryText'=>'Voice Call Records',
	'columns'=>array(
		'start_time','end_time','duration::Duration(s)',
		'itemclass.itemname','peernumber','charge_cent',
	),
)); ?>
<?php
  $modelsms = new SmsCdr();
  $modelsms->custnumber = $model->custnumber; 
  if ($pmodel->batch_id) $modelsms->batch_id = $pmodel->batch_id;
  ?>
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'tsms-grid',
	'dataProvider'=>$modelsms->search(true),
	//'filter'=>$modelvoice,
	'enablePagination'=>false,
	'enableSorting'=>false,
	'summaryText'=>'SMS Records',
	'columns'=>array(
		'tstamp',
		'itemclass.itemname','destnumber','charge_cent',
	),
)); ?>

<?php
  $modeltrans = new TrandetailCdr();
  $modeltrans->custnumber = $model->custnumber; 
  if ($pmodel->batch_id) $modeltrans->batch_id = $pmodel->batch_id;
  ?>
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'trans-grid',
	'dataProvider'=>$modeltrans->search(true),
	//'filter'=>$modelvoice,
	'enablePagination'=>false,
	'enableSorting'=>false,
	'summaryText'=>'Transaction Records',
	'columns'=>array(
		'tstamp',
		'itemclass.itemname','charge',
	),
)); ?>


<?php
  $modelroaming = new RoamingCdr();
  $modelroaming->custnumber = $model->custnumber; 
  if ($pmodel->batch_id) $modelroaming->batch_id = $pmodel->batch_id;
  ?>
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'roam-grid',
	'dataProvider'=>$modelroaming->search(true),
	//'filter'=>$modelvoice,
	'enablePagination'=>false,
	'enableSorting'=>false,
	'summaryText'=>'International Roaming Records',
	'columns'=>array(
		'tstamp','duration::Duration (s)','plmn.country','plmn.network_name',
		'itemclass.itemname','charge_cent',
	),
)); ?>