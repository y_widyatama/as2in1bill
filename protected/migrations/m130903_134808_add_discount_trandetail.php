<?php

class m130903_134808_add_discount_trandetail extends CDbMigration
{
	public function up()
	{
        $this->addColumn('t_trandetail','discount_flag','integer DEFAULT 0');
	}

	public function down()
	{
        $this->dropColumn('t_trandetail','discount_flag');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}