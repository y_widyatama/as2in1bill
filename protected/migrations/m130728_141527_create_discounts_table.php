<?php

class m130728_141527_create_discounts_table extends CDbMigration
{
	public function up()
	{
        $this->createTable('cust_discount',array('id'=>'pk','cust_id'=>'integer','percentfactor'=>'float',
            'code'=>'string','startperiod'=>'integer','endperiod'=>'integer DEFAULT 999912'));
	}

	public function down()
	{
        //$this->dropTable('cust_discount');
        echo "not allowed to destroy table cust_discount";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}