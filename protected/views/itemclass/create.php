<?php
/* @var $this ItemclassController */
/* @var $model Itemclass */

$this->breadcrumbs=array(
	'Itemclasses'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Itemclass', 'url'=>array('index')),
	array('label'=>'Manage Itemclass', 'url'=>array('admin')),
);
?>

<h1>Create Itemclass</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>