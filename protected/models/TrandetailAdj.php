<?php

/**
 * This is the model class for table "trandetail_adj".
 *
 * The followings are the available columns in table 'trandetail_adj':
 * @property integer $id
 * @property integer $batch_id
 * @property string $custnumber
 * @property string $tcode
 * @property integer $charge
 * @property string $tstamp
 * @property integer $ttype
 * @property integer $cust_id
 * @property integer $itemtext_id
 * @property integer $discount_flag
 *
 * The followings are the available model relations:
 * @property Itemclass $itemClass
 * @property Numbertoprofile $custnumber0
 */
class TrandetailAdj extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return TrandetailAdj the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'trandetail_adj';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('batch_id, charge, ttype, cust_id, itemtext_id, discount_flag', 'numerical', 'integerOnly'=>true),
			array('custnumber, tcode', 'length', 'max'=>45),
			array('tstamp', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, batch_id, custnumber, tcode, charge, tstamp, ttype, cust_id, itemtext_id, discount_flag', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'itemClass' => array(self::BELONGS_TO, 'Itemclass', 'tcode'),
			'custnumber0' => array(self::BELONGS_TO, 'Numbertoprofile', 'custnumber'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'batch_id' => 'Batch',
			'custnumber' => 'Custnumber',
			'tcode' => 'Tcode',
			'charge' => 'Charge',
			'tstamp' => 'Tstamp',
			'ttype' => 'Ttype',
			'cust_id' => 'Cust',
			'itemtext_id' => 'Itemtext',
			'discount_flag' => 'Discount Flag',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('batch_id',$this->batch_id);
		$criteria->compare('custnumber',$this->custnumber,true);
		$criteria->compare('tcode',$this->tcode,true);
		$criteria->compare('charge',$this->charge);
		$criteria->compare('tstamp',$this->tstamp,true);
		$criteria->compare('ttype',$this->ttype);
		$criteria->compare('cust_id',$this->cust_id);
		$criteria->compare('itemtext_id',$this->itemtext_id);
		$criteria->compare('discount_flag',$this->discount_flag);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}
