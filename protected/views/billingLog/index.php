<?php
/* @var $this BillingLogController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Billing Logs',
);

$this->menu=array(
	array('label'=>'Create BillingLog', 'url'=>array('create')),
	array('label'=>'Manage BillingLog', 'url'=>array('admin')),
);
?>

<h1>Billing Logs</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
