<?php
/* @var $this TrandetailCdrController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Trandetail Cdrs',
);

$this->menu=array(
	array('label'=>'Create TrandetailCdr', 'url'=>array('create')),
	array('label'=>'Manage TrandetailCdr', 'url'=>array('admin')),
);
?>

<h1>Trandetail Cdrs</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
