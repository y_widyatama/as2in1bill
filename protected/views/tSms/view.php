<?php
/* @var $this TSmsController */
/* @var $model TSms */

$this->breadcrumbs=array(
	'Tsms'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List TSms', 'url'=>array('index')),
	array('label'=>'Create TSms', 'url'=>array('create')),
	array('label'=>'Update TSms', 'url'=>array('update', 
	   'id'=>$model->id)),
	array('label'=>'Delete TSms', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage TSms', 'url'=>array('admin')),
);
?>

<h1>View TSms #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'custnumber',
		'acode',
		'batch_id',
		'charge_cent',
		'id',
	),
)); ?>
