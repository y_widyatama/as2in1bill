<?php
/* @var $this CustBillsController */
/* @var $model CustBills */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'account_id'); ?>
		<?php echo $form->textField($model,'account_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'paydate'); ?>
		<?php echo $form->textField($model,'paydate',array('size'=>45,'maxlength'=>45)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'paytime'); ?>
		<?php echo $form->textField($model,'paytime',array('size'=>45,'maxlength'=>45)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'payamount'); ?>
		<?php echo $form->textField($model,'payamount'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'paymethod'); ?>
		<?php echo $form->textField($model,'paymethod',array('size'=>45,'maxlength'=>45)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'custnumber'); ?>
		<?php echo $form->textField($model,'custnumber',array('size'=>45,'maxlength'=>45)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'paystatus'); ?>
		<?php echo $form->textField($model,'paystatus',array('size'=>45,'maxlength'=>45)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'cust_id'); ?>
		<?php echo $form->textField($model,'cust_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'batch_id'); ?>
		<?php echo $form->textField($model,'batch_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'billing_id'); ?>
		<?php echo $form->textField($model,'billing_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'chargeamount'); ?>
		<?php echo $form->textField($model,'chargeamount'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'billdate'); ?>
		<?php echo $form->textField($model,'billdate'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'prevbalance'); ?>
		<?php echo $form->textField($model,'prevbalance'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'prevpayment'); ?>
		<?php echo $form->textField($model,'prevpayment'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'newbalance'); ?>
		<?php echo $form->textField($model,'newbalance'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'prevodd'); ?>
		<?php echo $form->textField($model,'prevodd'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'newodd'); ?>
		<?php echo $form->textField($model,'newodd'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'invoiceround'); ?>
		<?php echo $form->textField($model,'invoiceround'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->