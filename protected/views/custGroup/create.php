<?php
/* @var $this CustGroupController */
/* @var $model CustGroup */

$this->breadcrumbs=array(
	'Cust Groups'=>array('index'),
	'Create',
);

$this->menu=array(
//	array('label'=>'List CustGroup', 'url'=>array('index')),
	array('label'=>'Manage CustGroup', 'url'=>array('admin')),
);
?>

<h1>Create CustGroup</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>