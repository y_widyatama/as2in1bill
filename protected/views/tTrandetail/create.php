<?php
/* @var $this TTrandetailController */
/* @var $model TTrandetail */

$this->breadcrumbs=array(
	'Ttrandetails'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List TTrandetail', 'url'=>array('index')),
	array('label'=>'Manage TTrandetail', 'url'=>array('admin')),
);
?>

<h1>Create TTrandetail</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>