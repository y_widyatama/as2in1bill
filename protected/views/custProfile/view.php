<?php
/* @var $this CustProfileController */
/* @var $model CustProfile */

$this->breadcrumbs=array(
	'Cust Profiles'=>array('index'),
	$model->title,
);

$this->menu=array(
	array('label'=>'List CustProfile', 'url'=>array('index')),
	array('label'=>'Create CustProfile', 'url'=>array('create')),
	array('label'=>'Billing For Customer', 'url'=>array('numberToProfile/view','id'=>$model->numbertoprofile->custnumber)),
	array('label'=>'Create surcharge', 'url'=>array('custSurcharge/create','customer_id'=>$model->id)),
    array('label'=>'Create discount', 'url'=>array('custDiscount/create','customer_id'=>$model->id)),
	array('label'=>'Cust Group', 'url'=>array('custGroup/view','id'=>$model->id)),
    array('label'=>'Create Cust Group', 'url'=>array('custGroup/create','customer_id'=>$model->id)),
	array('label'=>'Update CustProfile', 'url'=>array('update', 
	   'id'=>$model->id)),
	array('label'=>'Delete CustProfile', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage CustProfile', 'url'=>array('admin')),
);
?>

<h1>View CustProfile #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'account_id',
		'custnumber',
		'custname',
		'custaddr',
		'custemail',
		'corporatename',
		'title',
		'cnumber2',
		'createdate',
		'statecode',
		'termdate',
		'actualterm',
        'minimum_usage',
        'paymentMode',
	),
)); 

$surch = new CustSurcharge();
$surch->customer_id = $model->id;
?>
<h2>Surcharges</h2>
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'cust-surcharge-grid',
	'dataProvider'=>$surch->search(),
	'columns'=>array(
		'startperiod',
		'monthcount',
		'amount',
		'permonth',
		array(
			'class'=>'CButtonColumn',
			'viewButtonUrl'=>'array("custSurcharge/view","id"=>$data->id)',
            'updateButtonUrl'=>'array("custSurcharge/update","id"=>$data->id)',
            'deleteButtonUrl'=>'array("custSurcharge/delete","id"=>$data->id)',

		),
	),
)); ?>
<br>
<h2>Discounts</h2>
<?php
$dmodel = new CustDiscount();
$dmodel->cust_id = $model->id;
$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'cust-discount-grid',
	'dataProvider'=>$dmodel->search(),
	'columns'=>array(
		'percentfactor',
		'code',
        'itemclass.itemname',
        'itemclass.groupname',
		'startperiod',
		'endperiod',
        array(
      			'class'=>'CButtonColumn',
      			'viewButtonUrl'=>'array("custDiscount/view","id"=>$data->id)',
                'updateButtonUrl'=>'array("custDiscount/update","id"=>$data->id)',
                'deleteButtonUrl'=>'array("custDiscount/delete","id"=>$data->id)',

      		),

	),
)); ?>
