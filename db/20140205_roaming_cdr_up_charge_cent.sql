ALTER TABLE  `roaming_cdr` ADD  `up_charge_cent` INT NOT NULL ,
ADD  `up_charge_cent_by` VARCHAR( 24 ) NOT NULL ,
ADD  `up_charge_cent_time` DATETIME NOT NULL;

ALTER TABLE  `roaming_cdr` ADD  `up_charge_cent_percent` DOUBLE( 4, 2 ) NOT NULL AFTER  `up_charge_cent`;