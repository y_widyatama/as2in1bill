<?php
/* @var $this BillingStatusController */
/* @var $model BillingStatus */

$this->breadcrumbs=array(
	'Billing Statuses'=>array('index'),
	$model->batch_id,
);

$this->menu=array(
	array('label'=>'List BillingStatus', 'url'=>array('index')),
	array('label'=>'Create BillingStatus', 'url'=>array('create')),
	array('label'=>'Update BillingStatus', 'url'=>array('update', 
	   'id'=>$model->batch_id)),
	array('label'=>'Delete BillingStatus', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->batch_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage BillingStatus', 'url'=>array('admin')),
);
?>

<h1>View BillingStatus #<?php echo $model->batch_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'batch_id',
		'roaming',
		'voice',
		'sms',
		'trans',
		'lastupdated',
	),
)); ?>
