<?php

class m151107_093601_add_cust_paymentMode extends CDbMigration
{
	public function up()
	{
        $this->addColumn('cust_profile','paymentMode','string');
	}

	public function down()
	{
        $this->dropColumn('cust_profile','paymentMode');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}