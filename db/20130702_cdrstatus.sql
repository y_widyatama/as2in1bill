delimiter $$

CREATE TABLE `cdr_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `year` int(11) DEFAULT NULL,
  `month` int(11) DEFAULT NULL,
  `day` int(11) DEFAULT NULL,
  `status` varchar(12) DEFAULT NULL,
  `type` varchar(12) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `batch_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq` (`year`,`month`,`day`,`type`,`batch_id`)
) ENGINE=InnoDB AUTO_INCREMENT=748 DEFAULT CHARSET=latin1$$

