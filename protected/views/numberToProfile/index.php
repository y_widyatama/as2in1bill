<?php
/* @var $this NumberToProfileController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Number To Profiles',
);

$this->menu=array(
	array('label'=>'Create NumberToProfile', 'url'=>array('create')),
	array('label'=>'Manage NumberToProfile', 'url'=>array('admin')),
);
?>

<h1>Number To Profiles</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
