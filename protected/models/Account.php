<?php

/**
 * This is the model class for table "account".
 *
 * The followings are the available columns in table 'account':
 * @property integer $id
 * @property string $name
 *
 * The followings are the available model relations:
 * @property AccountBills[] $accountBills
 * @property CustProfile[] $custProfiles
 */
class Account extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Account the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'account';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id', 'required'),
			array('id', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>128),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name', 'safe', 'on'=>'search'),
		);
	}

    public function custBills($batch_id)
    {
        $c= new CDbCriteria();
        $c->compare('batch_id',$batch_id);
        $c->compare('account_id',$this->id);
        return CustBills::model()->findAll($c);

    }
    public function gatherEmails($batch_id)
    {
        $c= new CDbCriteria();
        $c->compare('batch_id',$batch_id);
        $c->compare('account_id',$this->id);
        $emails = array();
        $custBills = CustBills::model()->findAll($c);
        foreach ($custBills as $c)
        {
            /**
             * @var CustBills $c
             */
           if (!empty($c->cust))
            if (!empty($c->cust->custemail))
            {
                $emailStr = $c->cust->custemail;
                if (strpos($emailStr,';') === FALSE) {
                    $email = $emailStr;
                    if (!array_search($email,$emails))
					{
						if (isset($emails [$c->cust->custname]))
						{
						   $no = 2;
						   $name = $c->cust->custname . ' '.$no;
						   while (isset($emails[$name])) $no++;
						   $emails [$name] = $email;
						} else
                        $emails [$c->cust->custname] = $email;
					}
                }
                else {
                    $emailArr = explode(";",$emailStr);
                    foreach ($emailArr as $email)
                        if (!array_search($email,$emails))
						{
							if (isset($emails [$c->cust->custname]))
							{
							   $no = 2;
							   $name = $c->cust->custname . ' '.$no;
							   while (isset($emails[$name])) $no++;
							   $emails [$name] = $email;
							} else							
                            $emails [$c->cust->custname] = $email;
						}
                }
            }
        }
        return $emails;
    }
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'accountBills' => array(self::HAS_MANY, 'AccountBills', 'account_id'),
			'custProfiles' => array(self::HAS_MANY, 'CustProfile', 'account_id','order'=>'custProfiles.custnumber'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}