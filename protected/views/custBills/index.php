<?php
/* @var $this CustBillsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Cust Bills',
);

$this->menu=array(
	array('label'=>'Create CustBills', 'url'=>array('create')),
	array('label'=>'Manage CustBills', 'url'=>array('admin')),
);
?>

<h1>Cust Bills</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
