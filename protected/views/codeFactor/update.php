<?php
/* @var $this CodeFactorController */
/* @var $model CodeFactor */

$this->breadcrumbs=array(
	'Code Factors'=>array('index'),
	$model->code=>array('view','id'=>$model->code),
	'Update',
);

$this->menu=array(
	array('label'=>'List CodeFactor', 'url'=>array('index')),
	array('label'=>'Create CodeFactor', 'url'=>array('create')),
	array('label'=>'View CodeFactor', 'url'=>array('view', 'id'=>
	$model->code)),
	array('label'=>'Manage CodeFactor', 'url'=>array('admin')),
);
?>

<h1>Update CodeFactor <?php echo  $model->code ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>