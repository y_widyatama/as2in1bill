<?php
/* @var $this CustProfileController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Cust Profiles',
);

$this->menu=array(
	array('label'=>'Create CustProfile', 'url'=>array('create')),
	array('label'=>'Manage CustProfile', 'url'=>array('admin')),
);
?>

<h1>Cust Profiles</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
