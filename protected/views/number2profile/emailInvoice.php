<?php
/* @var $this AccountBillsController */

?>
<style type="text/css">
    td.rightalign {text-align: right; }

    tr.odd {
        background-color: #E5F1F4;
    }
    tr.even {
        background-color: #F8F8F8;
    }
    .grid-view table.items th { color: white; background-color: #6a6a7a; }
</style>

Dear Our loyal Kartu As2in1 customer,
<br/>
Thank you for using Kartu As2in1. Your latest bill is now ready. The email attachment contains the detail of your Kartu As2in1 usage.
<br/>
<?php
/**
 * @var CustBills $custBills
 */

/*$this->widget('zii.widgets.CDetailView', array(
    'data'=>$custBill,
    'formatter'=> new MyFormatter(),
    'itemTemplate'=>"<tr class=\"{class}\" ><th style='text-align: left'>{label}</th><td class=\"{class}\">{value}</td></tr>\n",
    'attributes'=>array(
     )
    )
);*/
$this->widget('zii.widgets.CDetailView', array(
    'data'=>$custBill,
    'formatter'=> new MyFormatter(),
    'itemTemplate'=>"<tr class=\"{class}\"><th style='text-align: left'>{label}</th><td class=\"{class}\" style=\"text-align:left\">{value}</td></tr>\n",
    'attributes'=>array(
        'cust.custname:text:Name',
        'cust.custaddr:text:Address',
        'custnumber:text:Phone number',
        'billing_id:text:Invoice No',
        'invoiceAmountStrHKD',
        'roundBalanceStrHKD',
        'payBefore'
    ),
)); ?>
<br/>
For any inquiries, please contact our account manager at 63755902.
<br/>
<br/>
--------------------------------------------------------<br/>
Telekomunikasi Indonesia International (HongKong) Limited
