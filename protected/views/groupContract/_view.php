<?php
/* @var $this GroupContractController */
/* @var $data GroupContract */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('group_id')); ?>:</b>
	<?php echo CHtml::encode($data->group_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('minimum_usage')); ?>:</b>
	<?php echo CHtml::encode($data->minimum_usage); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('startperiod')); ?>:</b>
	<?php echo CHtml::encode($data->startperiod); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('endperiod')); ?>:</b>
	<?php echo CHtml::encode($data->endperiod); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('initial_grace')); ?>:</b>
	<?php echo CHtml::encode($data->initial_grace); ?>
	<br />


</div>