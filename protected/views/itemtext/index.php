<?php
/* @var $this ItemtextController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Itemtexts',
);

$this->menu=array(
	array('label'=>'Create Itemtext', 'url'=>array('create')),
	array('label'=>'Manage Itemtext', 'url'=>array('admin')),
);
?>

<h1>Itemtexts</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
