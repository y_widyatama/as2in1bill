<?php
/* @var $this CustProfileController */
/* @var $model CustProfile */

$this->breadcrumbs=array(
	'Cust Profiles'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List CustProfile', 'url'=>array('index')),
	array('label'=>'Manage CustProfile', 'url'=>array('admin')),
);
?>

<h1>Create CustProfile</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>