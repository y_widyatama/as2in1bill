<?php

/**
 * This is the model class for table "cust_bills_2".
 * billing_id is soft-foreign key (non-enforced by DB) to AccountBills table
 * The followings are the available columns in table 'cust_bills_2':
 * @property integer $id
 * @property integer $account_id
 * @property string $paydate
 * @property string $paytime
 * @property integer $payamount
 * @property string $paymethod
 * @property string $custnumber
 * @property string $paystatus
 * @property integer $cust_id
 * @property integer $batch_id
 * @property integer $billing_id
 * @property integer $newbalance
 * @property integer $prevbalance
 * @property integer $prevpayment
 *
 *
 * The followings are the available model relations:
 * @property CustProfile $cust
 */
class CustBills extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CustBills the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'cust_bills';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('account_id, payamount, cust_id, batch_id, billing_id, prevpayment, prevbalance, newbalance', 'numerical', 'integerOnly'=>true),
			array('paydate, paytime, paymethod, custnumber, paystatus', 'length', 'max'=>45),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, account_id, paydate, paytime, payamount, paymethod, custnumber, paystatus, cust_id, batch_id, billing_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'cust' => array(self::BELONGS_TO, 'CustProfile', 'cust_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'account_id' => 'Account',
			'paydate' => 'Paydate',
			'paytime' => 'Paytime',
			'invoiceAmountStr' => 'Charges for current period',
			'invoiceAmountStrHKD' => 'Charges for current period',
            'prevBalanceStr' => 'Previous balance',
            'prevPaymentStr' => 'Payment received',
            'newBalanceStr' => 'Subtotal',
            'roundBalanceStr' => 'Total amount due',
			'roundBalanceStrHKD' => 'Total amount due',
            'paymethod' => 'Paymethod',
			'custnumber' => 'Custnumber',
			'paystatus' => 'Paystatus',
			'cust_id' => 'Cust',
			'batch_id' => 'Batch',
			'billing_id' => 'Bill Number',
            'account_id' => "Account Number",
            'oddCents'=>'Odd cents carried forward to next bill',
			'payBefore'=> 'Due date/Pay before'
		);
	}
    public function getOddCents() {
        return number_format(( $this->newbalance % 100) * -0.01,2);
    }
    public function getPrevBalanceStr() {
        return number_format($this->prevbalance * 0.01,2);
    }
    public function getPrevPaymentStr() {
        return number_format($this->prevpayment * 0.01,2);
    }
    public function getNewBalanceStr() {
        return number_format($this->newbalance * 0.01,2);
    }
    public function getRoundBalanceStr() {
        return number_format(floor($this->newbalance * 0.01),2);
    }
    public function getRoundBalanceStrHKD() {
        return 'HK$'.number_format(floor($this->newbalance * 0.01),2);
    }
    public function getInvoiceAmountStr() {
        return number_format($this->payamount * 0.01,2);
    }
    public function getInvoiceAmountStrHKD() {
        return 'HK$'.number_format($this->payamount * 0.01,2);
    }
    public function getPayBefore() {
        if (empty($this->billdate)) return "-";
        $bd = CDateTimeParser::parse($this->billdate,'yyyy-MM-dd HH:mm:ss');
        $mon = date('n',$bd);
        $dy = 20; if ($this->cust->isAutoPay())
            $dy = 15;
        $yr = date('Y',$bd);
        $paybefore = mktime(0,0,0,$mon,$dy,$yr);
        return Yii::app()->dateFormatter->format('EEE, d MMM yyyy',$paybefore);
    }
    public function getPeriodStr()
    {
        $batchId = $this->batch_id;
        $yr = floor($batchId / 100.0);
        $mon = $batchId % 100;
        $t = mktime(0,0,0,$mon,1,$yr);
        return date('F Y',$t);
    }
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('account_id',$this->account_id);
		$criteria->compare('paydate',$this->paydate,true);
		$criteria->compare('paytime',$this->paytime,true);
		$criteria->compare('payamount',$this->payamount);
		$criteria->compare('paymethod',$this->paymethod,true);
		$criteria->compare('custnumber',$this->custnumber,true);
		$criteria->compare('paystatus',$this->paystatus,true);
		$criteria->compare('cust_id',$this->cust_id);
		$criteria->compare('batch_id',$this->batch_id);
		$criteria->compare('billing_id',$this->billing_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
}
