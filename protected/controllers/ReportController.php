<?php

class ReportController extends Controller
{
/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('index','reportBillSummary','reportInvoiceDetail','reportInvoiceItems'),
				'roles'=>array('viewBilling'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('delete','create','update'),
				'roles'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actionIndex()
	{
		$this->layout='//layouts/column2';
		$this->render('index');
	}
	public function actionReportBillSummary()
	{
		$pmodel = new PeriodForm();
		if (isset($_REQUEST['PeriodForm']))
			$pmodel->attributes = $_REQUEST['PeriodForm'];
		else
		{
			$this->layout='//layouts/column2';
			$this->render("reportBillSummary");
			return;
		}
		$c = new CDbCriteria();
		$filename = 'billsummary_all.csv';

		if ($pmodel->batch_id) 
		{
			$c->compare('batch_id',$pmodel->batch_id);
            $filename = 'billsummary_'.$pmodel->batch_id.'.csv';
		}
	  	header("Content-Type: text/csv; charset=utf-8");
		header("Pragma: public");
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Content-Type: application/force-download");
		
		header("Content-Type: application/download");;
		header("Content-Disposition: attachment;filename=$filename ");
		header("Content-Transfer-Encoding: binary ");
		$rows = CustBills::model()->with('cust')->findAll($c);
		$S = ",";
		echo "Name $S Number $S Corporate $S AccountId $S Invoice Number $S InvoiceAmount $S Amount $S Previous Balance $S".
        " Payment Received $S\" New Balance(Total Due) \"$S Period\r\n";
		foreach ($rows as $row)
		{
            /**
             * @var CustBills $row
             */
		   echo '"'.$row->cust->custname.'"';
		   echo $S;
		   echo $row->custnumber;
		   echo $S;
		   echo '"'.$row->cust->corporatename.'"';
		   echo $S;
		   echo $row->account_id;
		   echo $S;
           echo $row->billing_id;
           echo $S;
		   echo $row->payamount * 0.01;
		   echo $S;
		   echo $row->chargeamount * 0.01;
		   echo $S;
           echo $row->prevbalance * 0.01;
           echo $S;
           echo $row->prevpayment* 0.01;
            echo $S;
            echo $row->newbalance* 0.01;
            echo $S;
            echo $row->batch_id;
		   echo "\r\n";
		}
		
		exit;
	}
	public function actionReportBillSummaryG()
	{
		$filename = 'billsummaryG.csv';
	  	header("Content-Type: text/csv; charset=utf-8");
		header("Pragma: public");
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Content-Type: application/force-download");
		
		header("Content-Type: application/download");;
		header("Content-Disposition: attachment;filename=$filename ");
		header("Content-Transfer-Encoding: binary ");
		$rows = CustBillsG::model()->with('cust')->findAll();
		$S = ",";
		echo "Name $S Number $S Corporate $S AccountId $S InvoiceAmount $S Period\r\n";
		foreach ($rows as $row)
		{
		   echo '"'.$row->cust->custname.'"';
		   echo $S;
		   echo $row->custnumber;
		   echo $S;
		   echo '"'.$row->cust->corporatename.'"';
		   echo $S;
		   echo $row->account_id;
		   echo $S;
		   echo $row->payamount * 0.01;
		   echo $S;
		   //echo $row->chargeamount * 0.01;
		   //echo $S;
		   echo $row->batch_id;
		   echo "\r\n";
		}
		
		exit;
	}

	public function actionReportInvoiceDetail()
	{
		set_time_limit(0);
		$pmodel = new PeriodForm();
		if (isset($_REQUEST['PeriodForm']))
			$pmodel->attributes = $_REQUEST['PeriodForm'];
		else
		{
			$this->layout='//layouts/column2';
			$this->render("reportBillSummary");
			return;
		}
		$c = new CDbCriteria();
		$filename = 'invoicedetails_all.csv';

		if ($pmodel->batch_id) 
		{
			$c->compare('batch_id',$pmodel->batch_id);
            $filename = 'invoicedetails_'.$pmodel->batch_id.'.csv';
		}
	  	header("Content-Type: text/csv; charset=utf-8");
		header("Pragma: public");
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Content-Type: application/force-download");
		$S = ',';
		header("Content-Type: application/download");;
		header("Content-Disposition: attachment;filename=$filename ");
		header("Content-Transfer-Encoding: binary ");
		$rows = VoiceCdr::model()->with('itemclass')->findAll($c);
		foreach ($rows as $row)
		{
		   echo $row->custnumber;
		   echo $S;
		   echo $row->charge_cent * 0.01;
		   echo $S;
		   echo $row->batch_id;
		   echo $S;
		   if (empty($row->itemclass))
		   {
			   echo 'Unknown';
			   echo $S;
			   echo 'Unknown';
		   }
		   else
		   {
			   echo '"'.$row->itemclass->groupname.'"';
			   echo $S;
			   echo '"'.$row->itemclass->itemname.'"';
		   }
		   echo $S;
		   echo '"'.$row->acode.'"';
		   echo $S;
		   /*
		   'start_time' => 'Start Time',
			'end_time' => 'End Time',
			'acode' => 'Acode',
			'custnumber' => 'Custnumber',
			'peernumber' => 'Peernumber',
			'duration' => 'Duration',
			'charge_cent' => 'Charge Cent',
			'chargerate_cent' => 'Chargerate Cent',
			'calltype_id' => 'Calltype',
			'cust_id' => 'Cust',*/
		   echo $row->start_time; 
		   echo $S;
		   echo $row->end_time; 
		   echo $S;
		   echo $row->peernumber;
		   echo "\r\n";
		}
		$rows = SmsCdr::model()->with('itemclass')->findAll();
		foreach ($rows as $row)
		{
		   echo $row->custnumber;
		   echo $S;
		   echo $row->charge_cent * 0.01;
		   echo $S;
		   echo $row->batch_id;
		   echo $S;
		   if (empty($row->itemclass))
		   {
			   echo 'Unknown';
			   echo $S;
			   echo 'Unknown';
		   }
		   else
		   {
			   echo '"'.$row->itemclass->groupname.'"';
			   echo $S;
			   echo '"'.$row->itemclass->itemname.'"';
		   }
		   echo $S;
		   echo '"'.$row->acode.'"';
		   echo $S;
		   /*
		  * @property string $custnumber
 * @property string $acode
 * @property string $tstamp
 * @property string $destnumber
 * @property integer $charge_cent
 * @property integer $status
 * @property integer $smflag
 * @property integer $batch_id
*/
		   echo $row->tstamp; 
		   echo $S;
		   echo ''; 
		   echo $S;
		   echo $row->destnumber;
		   echo "\r\n";
		}
		$rows = TrandetailCdr::model()->with('itemclass')->findAll();
		foreach ($rows as $row)
		{
		   echo $row->custnumber;
		   echo $S;
		   echo $row->charge * 0.01;
		   echo $S;
		   echo $row->batch_id;
		   echo $S;
		   if (empty($row->itemclass))
		   {
			   echo 'Unknown';
			   echo $S;
			   echo 'Unknown';
		   }
		   else
		   {
			   echo '"'.$row->itemclass->groupname.'"';
			   echo $S;
			   echo '"'.$row->itemclass->itemname.'"';
		   }
		   echo $S;
		   echo '"'.$row->tcode.'"';
		   echo $S;
		   /*
		  * @property string $custnumber
 * @property integer $batch_id
 * @property string $tcode
 * @property string $charge_cent

*/
		   echo $row->tstamp; 
		   echo $S;
		   echo ''; 
		   echo $S;
		   echo '';
		   echo "\r\n";
		}
		$rows = RoamingCdr::model()->with('itemclass')->findAll();
		foreach ($rows as $row)
		{
		   echo $row->custnumber;
		   echo $S;
		   echo $row->charge_cent * 0.01;
		   echo $S;
		   echo $row->batch_id;
		   echo $S;
		   if (empty($row->itemclass))
		   {
			   echo 'Unknown';
			   echo $S;
			   echo 'Unknown';
		   }
		   else
		   {
			   echo '"'.$row->itemclass->groupname.'"';
			   echo $S;
			   echo '"'.$row->itemclass->itemname.'"';
		   }
		   echo $S;
		   echo '"'.$row->acode.'"';
		   echo $S;
		   /*
		  * @property string $custnumber
 * @property integer $batch_id
 * @property string $tcode
 * @property string $charge_cent

*/
		   echo $row->tstamp; 
		   echo $S;
		   echo $row->duration; 
		   echo $S;
		   echo '';
		   echo "\r\n";
		}

		exit;
		$this->render('reportInvoiceDetail');
	}

	public function actionReportInvoiceItems()
	{
		set_time_limit(0);
		$pmodel = new PeriodForm();
		if (isset($_REQUEST['PeriodForm']))
			$pmodel->attributes = $_REQUEST['PeriodForm'];
		else
		{
			$this->layout='//layouts/column2';
			$this->render("reportBillSummary");
			return;
		}
		$c = new CDbCriteria();
		$filename = 'invoiceitems_all.csv';

		if ($pmodel->batch_id) 
		{
			$c->compare('batch_id',$pmodel->batch_id);
            $filename = 'invoiceitems_'.$pmodel->batch_id.'.csv';
		}
	  	header("Content-Type: text/csv; charset=utf-8");
		header("Pragma: public");
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Content-Type: application/force-download");
		
		header("Content-Type: application/download");;
		header("Content-Disposition: attachment;filename=$filename ");
		header("Content-Transfer-Encoding: binary ");
		$rows1 = TVoice::model()->with('itemclass')->findAll($c);
		$rows2 = TSms::model()->with('itemclass')->findAll($c);
		$rows3 = TRoaming::model()->with('itemclass')->findAll($c);
		$rows4 = TTrandetail::model()->with('itemclass')->findAll($c);
		$rows5 = TSurcharge::model()->with('itemclass')->findAll($c);
		$rows = array_merge ($rows1,$rows2,$rows3,$rows4,$rows5);
		$S = ",";
		echo "Number $S AccountId $S Corporate name $S Customer name $S Item category $S Item name $S code $S amount $S Period\r\n";
		foreach ($rows as $row)
		{
		   echo $row->custnumber;
		   echo $S;
		   echo $row->custnumber0->profile->account_id;
		   echo $S;
		   echo  '"'.$row->custnumber0->profile->corporatename.'"';
		   echo $S;
		   echo  '"'.$row->custnumber0->profile->custname.'"';
		   echo $S;
		   if (empty($row->itemclass))
		   {
			   echo 'Unknown';
			   echo $S;
			   echo 'Unknown';
		   }
		   else
		   {
			   echo '"'.$row->itemclass->groupname.'"';
			   echo $S;
			   echo '"'.$row->itemclass->itemname.'"';
		   }
		   echo $S;
		   echo '"'.$row->acode.'"';
		   echo $S;
		   echo $row->charge_cent * 0.01;
		   echo $S;
		   echo $row->batch_id;
		   echo "\r\n";
		}
		
		
		exit;
	}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}