<?php

/**
 * This is the model class for table "billing_log".
 *
 * The followings are the available columns in table 'billing_log':
 * @property integer $id
 * @property string $tstmp
 * @property string $code
 * @property string $ldesc
 * @property integer $batch_id
 */
class BillingLog extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return BillingLog the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'billing_log';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('tstmp', 'required'),
			array('batch_id', 'numerical', 'integerOnly'=>true),
			array('code', 'length', 'max'=>45),
			array('ldesc', 'length', 'max'=>128),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, tstmp, code, ldesc, batch_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'tstmp' => 'Tstmp',
			'code' => 'Code',
			'ldesc' => 'Ldesc',
			'batch_id' => 'Batch',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('tstmp',$this->tstmp,true);
		$criteria->compare('code',$this->code,true);
		$criteria->compare('ldesc',$this->ldesc,true);
		$criteria->compare('batch_id',$this->batch_id);
		$criteria->order = "id DESC";
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}