<?php

/**
 * This is the model class for table "billing_status".
 *
 * The followings are the available columns in table 'billing_status':
 * @property integer $batch_id
 * @property string $roaming
 * @property string $voice
 * @property string $sms
 * @property string $trans
 * @property string $lastupdated
 */
class BillingStatus extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return BillingStatus the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'billing_status';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('batch_id', 'required'),
			array('batch_id', 'numerical', 'integerOnly'=>true),
			array('roaming, voice, sms, trans', 'length', 'max'=>45),
			array('lastupdated', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('batch_id, roaming, voice, sms, trans, lastupdated', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'batch_id' => 'Batch',
			'roaming' => 'Roaming',
			'voice' => 'Voice',
			'sms' => 'Sms',
			'trans' => 'Trans',
			'lastupdated' => 'Lastupdated',
		);
	}
	public static function distinctBatchIds()
	{
		$c = new CDbCriteria();
		$c->select = "batch_id";
		$c->distinct = true;
		$results = BillingStatus::model()->findAll($c);
		$r = array();
		foreach ($results as $result)
			$r[] = $result->batch_id;
		return $r;
	}
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('batch_id',$this->batch_id);
		$criteria->compare('roaming',$this->roaming,true);
		$criteria->compare('voice',$this->voice,true);
		$criteria->compare('sms',$this->sms,true);
		$criteria->compare('trans',$this->trans,true);
		$criteria->compare('lastupdated',$this->lastupdated,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}