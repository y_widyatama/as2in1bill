<?php
/* @var $this GroupContractController */
/* @var $model GroupContract */

$this->breadcrumbs=array(
	'Group Contracts'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List GroupContract', 'url'=>array('index')),
	array('label'=>'Create GroupContract', 'url'=>array('create')),
	array('label'=>'Update GroupContract', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete GroupContract', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage GroupContract', 'url'=>array('admin')),
);
?>

<h1>View GroupContract #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
//		'id',
		'group.name',
		'minimum_usage',
		'startperiod',
		'endperiod',
		'initial_grace',
	),
)); ?>
