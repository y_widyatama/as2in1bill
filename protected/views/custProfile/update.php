<?php
/* @var $this CustProfileController */
/* @var $model CustProfile */

$this->breadcrumbs=array(
	'Cust Profiles'=>array('index'),
	$model->title=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List CustProfile', 'url'=>array('index')),
	array('label'=>'Create CustProfile', 'url'=>array('create')),
	array('label'=>'View CustProfile', 'url'=>array('view', 'id'=>
	$model->id)),
	array('label'=>'Manage CustProfile', 'url'=>array('admin')),
);
?>

<h1>Update CustProfile <?php echo  $model->id ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>