<?php

class m130728_205723_create_discounts_fk extends CDbMigration
{
	public function up()
	{
        $this->addForeignKey('fk_discount_cust','cust_discount','cust_id','cust_profile','id');
        $this->addForeignKey('fk_discount_cust2','cust_discount','code','itemclass','acode');
	}

	public function down()
	{
        $this->dropForeignKey('fk_discount_cust','cust_discount');
		$this->dropForeignKey('fk_discount_cust2','cust_discount');
		
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}