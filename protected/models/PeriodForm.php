<?php

/**
 * LoginForm class.
 * LoginForm is the data structure for keeping
 * user login form data. It is used by the 'login' action of 'SiteController'.
 */
class PeriodForm extends CFormModel
{
	public $batch_id;

	/**
	 * Declares the validation rules.
	 * The rules state that username and password are required,
	 * and password needs to be authenticated.
	 */
	public function rules()
	{
		return array(
			// username and password are required
			array('batch_id', 'numerical'),
		);
	}
	public static function getDropDown($withAll = false)
	{
		$r = array();
//		$z = array('201301','201302','201303','201304','201305','201306','201307','201308');
		$z = BillingStatus::distinctBatchIds();
		if ($withAll) $r['0']='ALL';
		foreach ($z as $zrow)
		  $r[$zrow] = $zrow;
		return $r;
	}
	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels()
	{
		return array(
			'batch_id'=>'Billing Period',
		);
	}

    public static function getPeriodList(){
        $startMonth = 'September 2012';
        $endPeriod = date('Ym',strtotime('+6 month'));
        $listPeriods = array();
        $i = 0;
        do{
            $periodStr = $startMonth.' '.$i++.' month';
            $period = date('Ym', strtotime($periodStr));
            $periodLabel = date('Y F',strtotime($periodStr));
            $listPeriods[intval($period)] = $periodLabel;
        }while($period <= $endPeriod);
        $listPeriods[999912] = '9999 December';
        return $listPeriods;
    }

}
