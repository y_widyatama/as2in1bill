<?php
/* @var $this CustContractController */
/* @var $model CustContract */

$this->breadcrumbs=array(
	'Cust Contracts'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List CustContract', 'url'=>array('index')),
	array('label'=>'Manage CustContract', 'url'=>array('admin')),
);
?>

<h1>Create CustContract</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>