<?php
/* @var $this ReportController */

$this->breadcrumbs=array(
	'Report'=>array('/report'),
);

$this->menu=array(
	array('label'=>'Bill Summary', 'url'=>array('reportBillSummary')),
	array('label'=>'Invoice Items', 'url'=>array('reportInvoiceItems')),
	array('label'=>'Invoice CDR Details', 'url'=>array('reportInvoiceDetail')),
	
);

?>
<h1>Report</h1>

<p>Choose any menu to download data.
</p>
