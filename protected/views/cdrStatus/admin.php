<?php
/* @var $this CdrStatusController */
/* @var $model CdrStatus */

$this->breadcrumbs=array(
	'Cdr Statuses'=>array('index'),
	'Manage',
);
$pmodel = new PeriodForm();
if (isset($_REQUEST['PeriodForm']))
	$pmodel->attributes = $_REQUEST['PeriodForm'];
else 
	$pmodel->batch_id = date('Y').date('m');

$this->menu=array(
	array('label'=>'Sms cdr status', 'url'=>array('cdrStatus/admin','CdrStatus[type]'=>'sms','PeriodForm'=>$pmodel->attributes)),
	array('label'=>'Voice cdr status', 'url'=>array('cdrStatus/admin','CdrStatus[type]'=>'voice','PeriodForm'=>$pmodel->attributes)),
	array('label'=>'Trans cdr status', 'url'=>array('cdrStatus/admin','CdrStatus[type]'=>'trans','PeriodForm'=>$pmodel->attributes)),
	array('label'=>'Roaming cdr status', 'url'=>array('cdrStatus/admin','CdrStatus[type]'=>'roaming','PeriodForm'=>$pmodel->attributes)),
//	array('label'=>'List CdrStatus', 'url'=>array('index')),
//	array('label'=>'Create CdrStatus', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#cdr-status-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");

$model->batch_id = $pmodel->batch_id;
?>

<h1>Check Cdr Status</h1>
<?php
$this->renderPartial('/periodform',array('model'=>$pmodel,'actionUrl'=>array('')));
?>


<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'cdr-status-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		//'id',
		'year',
		'month',
		'day',
		'statushtml:html',
		'type',
		
		'updated',
		'batch_id',
		
		array(
			'class'=>'CButtonColumn',
			'template'=>'{view}',
		),
	),
)); ?>
