<?php
/* @var $this GroupContractController */
/* @var $model GroupContract */

$this->breadcrumbs=array(
	'Group Contracts'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List GroupContract', 'url'=>array('index')),
	array('label'=>'Manage GroupContract', 'url'=>array('admin')),
);
?>

<h1>Create GroupContract</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>