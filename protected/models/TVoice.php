<?php

/**
 * This is the model class for table "t_voice".
 *
 * The followings are the available columns in table 't_voice':
 * @property string $custnumber
 * @property integer $batch_id
 * @property string $acode
 * @property string $charge_cent
 * @property integer $id
 *
 * The followings are the available model relations:
 * @property Numbertoprofile $custnumber0
 */
class TVoice extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return TVoice the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 't_voice';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('batch_id', 'numerical', 'integerOnly'=>true),
			array('custnumber, acode', 'length', 'max'=>45),
			array('charge_cent', 'length', 'max'=>32),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('custnumber, batch_id, acode, charge_cent, id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'custnumber0' => array(self::BELONGS_TO, 'NumberToProfile', 'custnumber'),
			'itemclass' => array(self::BELONGS_TO, 'Itemclass', 'acode'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'custnumber' => 'Custnumber',
			'batch_id' => 'Batch',
			'acode' => 'Acode',
			'charge_cent' => 'Charge Cent',
			'id' => 'ID',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('custnumber',$this->custnumber,true);
		$criteria->compare('batch_id',$this->batch_id);
		$criteria->compare('acode',$this->acode,true);
		$criteria->compare('charge_cent',$this->charge_cent,true);
		$criteria->compare('id',$this->id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}