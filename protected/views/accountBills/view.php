<?php
/* @var $this AccountBillsController */
/* @var $model AccountBills */

$this->breadcrumbs=array(
	'Account Bills'=>array('index'),
	$model->id,
);

$pmodel = new PeriodForm();
$pmodel->batch_id = $model->batch_id;

$this->menu=array(
	array('label'=>'List AccountBills', 'url'=>array('index')),
    array('label'=>'View Invoice', 'url'=>array('/account/viewInvoice',
        'id'=>$model->account_id,'PeriodForm'=>$pmodel->attributes)),

//	array('label'=>'Create AccountBills', 'url'=>array('create')),
//	array('label'=>'Update AccountBills', 'url'=>array('update',
//	   'id'=>$model->id)),
//	array('label'=>'Delete AccountBills', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage AccountBills', 'url'=>array('admin')),
);
?>

<h1>View AccountBills #<?php echo $model->id; ?></h1>

<?php
foreach(Yii::app()->user->getFlashes() as $key => $message) {
    echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
}
?>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
    'formatter'=> new MyFormatter(),
	'attributes'=>array(
		'id',
		'billdate',
		'account_id',
		'invoiceAmount:currency',
		'batch_id',
        'emailstatus',
        'invoicepdfcreated',
        'emailsent',
	),
)); ?>

<h1>Bills</h1>
<?php
    $bills= $model->bills;
    $billsProvider = new CArrayDataProvider($bills);
    $this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'cust-bills-grid',
    'dataProvider'=>$billsProvider,
    'columns'=>array(
        'custnumber',
        'cust.custname',
        'invoiceAmountStr',
        'paystatus',
        'batch_id',
          'billdate',
          'prevBalanceStr',
          'prevPaymentStr',
          'newBalanceStr',
        array(
            'class'=>'CButtonColumn',
            'template'=>'{view}',
            'viewButtonUrl'=>
            'array("/numberToProfile/view","id"=>$data->custnumber,"PeriodForm[batch_id]"=>$data->batch_id)'
        ),
    ),
)); ?>


