<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />

	<!-- blueprint CSS framework -->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/screen.css" media="screen, projection" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/print.css" media="print" />
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/ie.css" media="screen, projection" />
	<![endif]-->

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/main.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/form.css" />

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body>

<div class="container" id="page">

	<div id="header">
		<div id="logo"><?php echo CHtml::encode(Yii::app()->name); ?></div>
	</div><!-- header -->

	<div id="mainmenu">
		<?php $this->widget('zii.widgets.CMenu',array(
			'items'=>array(
				array('label'=>'Home', 'url'=>array('/site/index')),
				array('label'=>'Customer Profile', 'url'=>array('/custProfile/admin'),'visible'=>!Yii::app()->user->isGuest),
                array('label'=>'Accounts', 'url'=>array('/account/admin'),'visible'=>!Yii::app()->user->isGuest),
                array('label'=>'CustBills', 'url'=>array('/custBills/admin'),'visible'=>!Yii::app()->user->isGuest),
                array('label'=>'Group', 'url'=>array('/group/admin'),'visible'=>!Yii::app()->user->isGuest),
                array('label'=>'Account Billing', 'url'=>array('/accountBills/admin'),'visible'=>!Yii::app()->user->isGuest),
                array('label'=>'CustPayment', 'url'=>array('/custPayment/admin'),'visible'=>!Yii::app()->user->isGuest),
				array('label'=>'Billing Proc Status', 'url'=>array('/billingStatus/admin'),'visible'=>!Yii::app()->user->isGuest),
				array('label'=>'Billing Proc History', 'url'=>array('/billingLog/admin'),'visible'=>!Yii::app()->user->isGuest),
				array('label'=>'Cdr Status', 'url'=>array('/cdrStatus/admin'),'visible'=>!Yii::app()->user->isGuest),
				array('label'=>'Report', 'url'=>array('/report/index'),'visible'=>!Yii::app()->user->isGuest),
				array('label'=>'Login', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
				array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest),
				array('label'=>'Change password', 'url'=>array('/users/changePassword','id'=>Yii::app()->user->id), 'visible'=>!Yii::app()->user->isGuest)
					),
		)); ?>
	</div><!-- mainmenu -->

	<?php $this->widget('zii.widgets.CBreadcrumbs', array(
		'links'=>$this->breadcrumbs,
	)); ?><!-- breadcrumbs -->

	<?php echo $content; ?>

	<div id="footer">
		Copyright &copy; <?php echo date('Y'); ?> by Telekomunikasi Indonesia International (Hongkong) Limited<br/>
		All Rights Reserved.<br/>
		<?php echo Yii::powered(); ?>
	</div><!-- footer -->

</div><!-- page -->

</body>
</html>