<?php
/* @var $this NumberToProfileController */
/* @var $model NumberToProfile */
if (!isset($withDetails)) $withDetails=true;
?>
<?php 
$pmodel = new PeriodForm();
$pmodel->batch_id = $batchId;
$custBill = $model->getCustBill($batchId);

$this->widget('zii.widgets.CDetailView', array(
    'data'=>$custBill,
    'attributes'=> array(
        'billing_id',
        'account_id'
        )
));
$this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
        'custnumber:text:Phone number',
		'profile.corporatename:text:Company/Group',
		'profile.title',
		'profile.custname:text:Name',
		'profile.custaddr:text:Address',
		'profile.custemail:text:Email',
		
	),
)); ?>
<style type="text/css">
.rightalign {text-align: right; }
body {background-color: white; }
.grid-view table.items th { color: white; background: #6a6a7a no-repeat; }
</style>
<?php
if ($withDetails) {
?>
<div style="page-break-after: always">
    <?php
}
?>
<b></b>
<br/>
    <div >
        <table>
            <tbody>
            <tr>
                <td>Billing Period</td>
                <td>:</td>
                <td>
                    <?php echo $custBill->getPeriodStr();
                    ?>

                </td>
            </tr>
            <tr>
                <td>Customer Services</td>
                <td>:</td>
                <td>Plasa GraPARI (2890 1230)</td>
            </tr>
            <tr>
                <td>Account Manager</td>
                <td>:</td>
                <td>63755902</td>
            </tr>
            </tbody>
        </table>
    </div>
<br>
<div style="text-align:center; font-weight: bold; font-size: medium">Statement Summary</div>
<br/>

    <?php
  $dpArr = $model->getInvoiceList($batchId);
   $total = 0;
   foreach ($dpArr as $rec)
     $total += $rec->charge;


?>
<?php

  $dp = new CArrayDataProvider($dpArr,array(
	'id'=>'invlist',
	'pagination'=>array('pageSize'=>1000)));
  ?>

<?php //$this->widget('zii.widgets.grid.CGridView', array(
	$this->widget('ext.groupgridview.GroupGridView', array(
	'id'=>'tsms-grid',
	'dataProvider'=>$dp,
 'extraRowColumns' => array('groupname'),
      'extraRowPos' => 'above',
	  //	'filter'=>$models,
	'columns'=>array(
		//array('name'=>'groupname','footer'=>'Total for this billing period','header'=>'Category'),
		array('name'=>'itemname','header'=>'Item','footer'=>'Total'),

		array(
		   'name'=>'chargeStr',
		   'header'=>'Charge',
		   'footer'=>number_format($total,2),
            'footerHtmlOptions'=>array('style'=>'text-align: right; font-weight: bold;'),
		   'cssClassExpression'=>'"rightalign"',
		),		
	),
	'summaryText'=>'',
)); 
$minPay = $model->getMininumPayment();
if ($total<$minPay) {
echo '<div><b>Minimum payment commitment : HK $'.$minPay.'</b></div><br>';
$total = $minPay;
}

    $payBeforeLabel = 'Pay before'; 
    if ($model->profile->isAutoPay()) $payBeforeLabel = 'By Autopay On'; 
    $detailAttributes = array(
            'prevBalanceStr',
            'prevPaymentStr',
            'invoiceAmountStr',
            'newBalanceStr',
            'oddCents',
            'roundBalanceStr',
            'payBefore:text:'.$payBeforeLabel
        );
    $this->widget('zii.widgets.CDetailView', array(
        'data'=>$custBill,
        'itemTemplate'=>
        "<tr class=\"{class} leftalign\"><th style='width: 250px;'>{label}</th><td class=\"{class} rightalign\">{value}</td></tr>\n",
        'attributes'=>$detailAttributes
    ));
echo "<br>";
    $this->renderPartial('/numberToProfile/invoiceClosing');

    ?>




</div>
<?php
/* @var $this NumberToProfileController */
/* @var $model NumberToProfile */
if ($withDetails) {
?>
<h2 align="center">CHARGES DETAILS</h2>
<?php
  $modelvoice = new VoiceCdr();
  $modelvoice->custnumber = $model->custnumber; 
  if ($pmodel->batch_id) $modelvoice->batch_id = $pmodel->batch_id;
  ?>
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'tvoice-grid',
	'dataProvider'=>$modelvoice->search(true),
	//'filter'=>$modelvoice,
	'enablePagination'=>false,
	'enableSorting'=>false,
	'summaryText'=>'Voice Call Records',
    'emptyText'=>'No Voice Call Records',
    'columns'=>array(
		'start_time','end_time','duration::Duration(s)',
		'itemclass.itemname','peernumber','charge_cent',
	),
)); ?>
<?php
  $modelsms = new SmsCdr();
  $modelsms->custnumber = $model->custnumber; 
  if ($pmodel->batch_id) $modelsms->batch_id = $pmodel->batch_id;
  ?>
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'tsms-grid',
	'dataProvider'=>$modelsms->search(true),
	//'filter'=>$modelvoice,
	'enablePagination'=>false,
	'enableSorting'=>false,
	'summaryText'=>'SMS Records',
    'emptyText'=>'No SMS Records',
    'columns'=>array(
		'tstamp',
		'itemclass.itemname','destnumber','charge_cent',
	),
)); ?>

<?php
  $modeltrans = new TrandetailCdr();
  $modeltrans->custnumber = $model->custnumber; 
  if ($pmodel->batch_id) $modeltrans->batch_id = $pmodel->batch_id;
  ?>
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'trans-grid',
	'dataProvider'=>$modeltrans->search(true),
	//'filter'=>$modelvoice,
	'enablePagination'=>false,
	'enableSorting'=>false,
	'summaryText'=>'Transaction Records',
    'emptyText'=>'No Transaction Records',
    'columns'=>array(
		'tstamp',
		'itemclass.itemname','charge',
	),
)); ?>


<?php
  $modelroaming = new RoamingCdr();
  $modelroaming->custnumber = $model->custnumber; 
  if ($pmodel->batch_id) $modelroaming->batch_id = $pmodel->batch_id;
  ?>
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'roam-grid',
	'dataProvider'=>$modelroaming->search(true),
	//'filter'=>$modelvoice,
	'enablePagination'=>false,
	'enableSorting'=>false,
	'summaryText'=>'International Roaming Records',
    'emptyText'=>'No international roaming data',
	'columns'=>array(
		'tstamp','duration::Duration (s)','plmn.country','plmn.network_name',
		'itemclass.itemname',
		//'charge_cent',
		'actual_charge_cent',
		/*
		array (
			'header' => 'Charge Cent',
			'type' => 'raw',
			'value'=>function($data){
				return $data->charge_cent*1.25;
			}
		),
		*/
	),
));

$this->renderPartial('/numberToProfile/invoiceClosing');
} else
    echo "<div align='center'>--- END OF STATEMENT ----</div>";
?>

<br><br>
<b>Reminder:</b><br>
Your Kartu As 2in1 card can roam in every GSM network operator world wide and the normal voice and sms roaming rates will applied.
For roaming in Indonesia, please make sure to manually set your network to TELKOMSEL in order to avoid high charge roaming from other local network operators.