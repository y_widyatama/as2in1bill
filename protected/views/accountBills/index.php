<?php
/* @var $this AccountBillsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Account Bills',
);

$this->menu=array(
	array('label'=>'Create AccountBills', 'url'=>array('create')),
	array('label'=>'Manage AccountBills', 'url'=>array('admin')),
);
?>

<h1>Account Bills</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
