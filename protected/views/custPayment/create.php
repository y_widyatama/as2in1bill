<?php
/* @var $this CustPaymentController */
/* @var $model CustPayment */

$this->breadcrumbs=array(
	'Cust Payments'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List CustPayment', 'url'=>array('index')),
	array('label'=>'Manage CustPayment', 'url'=>array('admin')),
);
?>

<h1>Create CustPayment</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>