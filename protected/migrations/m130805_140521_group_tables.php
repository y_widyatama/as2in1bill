<?php

class m130805_140521_group_tables extends CDbMigration
{
	public function up()
	{
	    $this->createTable('group',array('id'=>'pk','name'=>'string'));
		$this->createTable('cust_group',array('id'=>'pk','cust_id'=>'integer','group_id'=>'integer','startperiod'=>'integer',
		'endperiod'=>'integer DEFAULT 999912'));
		$this->createTable('group_contract',array('id'=>'pk','group_id'=>'integer',
			'minimum_usage' => 'decimal default 100',
		    'startperiod'=>'integer',
			'endperiod'=>'integer DEFAULT 999912'));
        $this->createTable('group_discount',array('id'=>'pk','group_id'=>'integer','percentfactor'=>'float',
            'code'=>'string','startperiod'=>'integer','endperiod'=>'integer DEFAULT 999912'));
	}

	public function down()
	{
	    $this->dropTable('group_discount');
	    $this->dropTable('group_contract');
	    $this->dropTable('cust_group');
 	    $this->dropTable('group');
       
		return true;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}