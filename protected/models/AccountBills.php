<?php

/**
 * This is the model class for table "account_bills".
 * NOTE: id in account_bills is THE billing_id found in the Settlement File
 * combination of account_id , batch_id -> must be unique
 * The followings are the available columns in table 'account_bills':
 * @property integer $id
 * @property string $billdate
 * @property integer $account_id
 * @property integer $totalinvoice
 * @property integer $batch_id
 *
 * The followings are the available model relations:
 * @property Account $account
 * @property CustBills[] $bills
 */
class AccountBills extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return AccountBills the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'account_bills';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('account_id, totalinvoice, batch_id', 'numerical', 'integerOnly'=>true),
			array('billdate', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, billdate, account_id, totalinvoice, batch_id', 'safe', 'on'=>'search'),
		);
	}

    public function getInvoiceAmount()
    {
        $bills = $this->bills;
        $t = 0;
        foreach ($bills as $bill)
        {
            $t += $bill->payamount;
        }
        return $t * 0.01;
    }
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'account' => array(self::BELONGS_TO, 'Account', 'account_id'),
            'bills' => array(self::HAS_MANY, 'CustBills', 'billing_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'billdate' => 'Billdate',
			'account_id' => 'Account',
			'totalinvoice' => 'Totalinvoice',
			'batch_id' => 'Batch',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('billdate',$this->billdate,true);
		$criteria->compare('account_id',$this->account_id);
		$criteria->compare('totalinvoice',$this->totalinvoice);
		$criteria->compare('batch_id',$this->batch_id);
        $criteria->order = 'batch_id desc, account_id asc';
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination'=>array('pageSize'=>20)
		));
	}
    function getInvoiceURL()
    {
        $iscorporate = ($this->account_id < 20000000) ;
        $batch_id = $this->batch_id;
        if ($iscorporate)
        {
            $id = $this->account_id;
        }
        else
        {
            $bills = $this->bills;
            if (empty($bills)) return "javascript: alert('no invoice found');";
            $firstBill = $bills[0];
            $id = $firstBill->custnumber;
        }
        if (!$iscorporate)
            return Yii::app()->baseUrl. '/invoices/'.$batch_id.
                '_personalInvoice_'.$id.'.pdf';

        return Yii::app()->baseUrl. '/invoices/'.$batch_id.
            '_corporateInvoice_'.$id.'.pdf';

    }
    function getInvoiceURL2()
    {
        $iscorporate = ($this->account_id < 20000000) ;

        $a =
            array('/account/viewInvoice',
                'id'=>$this->account_id,'PeriodForm[batch_id]'=>$this->batch_id, 'isPdf'=>1);

        if ($iscorporate) return $a;
        $bills = $this->bills;
        if (empty($bills)) return "javascript: alert('no invoice found');";
        $firstBill = $bills[0];
        $id = $firstBill->custnumber;

        $b =
            array('/numberToProfile/viewInvoicePdf',
                'id'=>$id,'batchId'=>$this->batch_id,'noDetail'=>1);
        return $b;
    }
    function getInvoiceURL3()
    {
        $iscorporate = ($this->account_id < 20000000) ;

        $a =
            array('/account/callDetails',
                'id'=>$this->account_id,'PeriodForm[batch_id]'=>$this->batch_id,'isPdf'=>1);

        if ($iscorporate) return $a;
        $bills = $this->bills;
        if (empty($bills)) return "javascript: alert('no invoice found');";
        $firstBill = $bills[0];
        $id = $firstBill->custnumber;

        $b =
            array('/numberToProfile/viewInvoiceCallDetailsPdf',
                'id'=>$id,'batchId'=>$this->batch_id);
        return $b;
    }
}