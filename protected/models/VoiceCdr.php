<?php

/**
 * This is the model class for table "voice_cdr".
 *
 * The followings are the available columns in table 'voice_cdr':
 * @property integer $id
 * @property integer $batch_id
 * @property string $start_time
 * @property string $end_time
 * @property string $acode
 * @property string $custnumber
 * @property string $peernumber
 * @property integer $duration
 * @property integer $charge_cent
 * @property integer $chargerate_cent
 * @property integer $calltype_id
 * @property integer $cust_id
 */
class VoiceCdr extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return VoiceCdr the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'voice_cdr';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('batch_id, duration, charge_cent, chargerate_cent, calltype_id, cust_id', 'numerical', 'integerOnly'=>true),
			array('acode, custnumber, peernumber', 'length', 'max'=>45),
			array('start_time, end_time', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, batch_id, start_time, end_time, acode, custnumber, peernumber, duration, charge_cent, chargerate_cent, calltype_id, cust_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'custnumber0' => array(self::BELONGS_TO, 'NumberToProfile', 'custnumber'),
			'itemclass' => array(self::BELONGS_TO, 'Itemclass', 'acode'),
		);
	}
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'batch_id' => 'Batch',
			'start_time' => 'Start Time',
			'end_time' => 'End Time',
			'acode' => 'Acode',
			'custnumber' => 'Custnumber',
			'peernumber' => 'Peernumber',
			'duration' => 'Duration',
			'charge_cent' => 'Charge Cent',
			'chargerate_cent' => 'Chargerate Cent',
			'calltype_id' => 'Calltype',
			'cust_id' => 'Cust',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search($noPage=false)
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('batch_id',$this->batch_id);
		$criteria->compare('start_time',$this->start_time,true);
		$criteria->compare('end_time',$this->end_time,true);
		$criteria->compare('acode',$this->acode,true);
		$criteria->compare('custnumber',$this->custnumber,true);
		$criteria->compare('peernumber',$this->peernumber,true);
		$criteria->compare('duration',$this->duration);
		$criteria->compare('charge_cent',$this->charge_cent);
		$criteria->compare('chargerate_cent',$this->chargerate_cent);
		$criteria->compare('calltype_id',$this->calltype_id);
		$criteria->compare('cust_id',$this->cust_id);

		if ($noPage)
		return new CActiveDataProvider($this, array('criteria'=>$criteria, 'pagination'=>array('pageSize'=>100000)) );
		else
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
}