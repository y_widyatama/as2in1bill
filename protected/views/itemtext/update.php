<?php
/* @var $this ItemtextController */
/* @var $model Itemtext */

$this->breadcrumbs=array(
	'Itemtexts'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Itemtext', 'url'=>array('index')),
	array('label'=>'Create Itemtext', 'url'=>array('create')),
	array('label'=>'View Itemtext', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Itemtext', 'url'=>array('admin')),
);
?>

<h1>Update Itemtext <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>