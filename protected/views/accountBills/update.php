<?php
/* @var $this AccountBillsController */
/* @var $model AccountBills */

$this->breadcrumbs=array(
	'Account Bills'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List AccountBills', 'url'=>array('index')),
	array('label'=>'Create AccountBills', 'url'=>array('create')),
	array('label'=>'View AccountBills', 'url'=>array('view', 'id'=>
	$model->id)),
	array('label'=>'Manage AccountBills', 'url'=>array('admin')),
);
?>

<h1>Update AccountBills <?php echo  $model->id ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>