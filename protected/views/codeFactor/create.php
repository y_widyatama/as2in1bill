<?php
/* @var $this CodeFactorController */
/* @var $model CodeFactor */

$this->breadcrumbs=array(
	'Code Factors'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List CodeFactor', 'url'=>array('index')),
	array('label'=>'Manage CodeFactor', 'url'=>array('admin')),
);
?>

<h1>Create CodeFactor</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>