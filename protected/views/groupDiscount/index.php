<?php
/* @var $this GroupDiscountController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Group Discounts',
);

$this->menu=array(
	array('label'=>'Create GroupDiscount', 'url'=>array('create')),
	array('label'=>'Manage GroupDiscount', 'url'=>array('admin')),
);
?>

<h1>Group Discounts</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
