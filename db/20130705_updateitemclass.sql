UPDATE `as2in1bill`.`itemclass` SET `itemname`='Local Inter SMS' WHERE `acode`='INTER_SMS';
UPDATE `as2in1bill`.`itemclass` SET `itemname`='Local Intra SMS' WHERE `acode`='INTRA_SMS';
UPDATE `as2in1bill`.`itemclass` SET `itemname`='TSEL Roaming SMS' WHERE `acode`='ROAMING_SMS';
UPDATE `as2in1bill`.`itemclass` SET `itemname`='Outgoing Intl Roaming SMS' WHERE `acode`='MO_ROAM_SMS';
INSERT INTO `as2in1bill`.`itemclass` (`acode`, `itemname`, `groupname`) VALUES ('MT_ROAM_SMS', 'Incoming Intl Roaming SMS', 'International Roaming');
UPDATE `as2in1bill`.`itemclass` SET `itemname`='Data pay-per-use ' WHERE `acode`='TXN_DATA_PAY_PER_USE_DEBIT';
UPDATE `as2in1bill`.`itemclass` SET `itemname`='Data pay-per use Adjustment' WHERE `acode`='TXN_DATA_PAY_PER_USE_CREDIT';
UPDATE `as2in1bill`.`itemclass` SET `itemname`='Data Plan Subscription' WHERE `acode`='TXN_DATA_RENEW_DATA_PLAN';
UPDATE `as2in1bill`.`itemclass` SET `itemname`='Data Plan Subscription' WHERE `acode`='TXN_DATA_SUB_DATA_PLAN';
UPDATE `as2in1bill`.`itemclass` SET `itemname`='Ring Back Tone Subscription' WHERE `acode`='TXN_RBT_RBTFEE';
UPDATE `as2in1bill`.`itemclass` SET `itemname`='Ring Back Tone Subscription.' WHERE `acode`='TXN_VAS_RENEW';
UPDATE `as2in1bill`.`itemclass` SET `itemname`='Outgoing Intl Roaming Call' WHERE `acode`='MO_ROAM_VCE';
UPDATE `as2in1bill`.`itemclass` SET `itemname`='Incoming Intl Roaming Call' WHERE `acode`='MT_ROAM_VCE';
UPDATE `as2in1bill`.`itemclass` SET `itemname`='Incoming Roaming Call 1C2N' WHERE `acode`='ROAMING_1C2N_MT';
UPDATE `as2in1bill`.`itemclass` SET `itemname`='Hotline Call non-1C2N' WHERE `acode`='ADMIN_1C2N';
UPDATE `as2in1bill`.`itemclass` SET `itemname`='Incoming Local Call non-1C2N' WHERE `acode`='MT_LOCAL';
UPDATE `as2in1bill`.`itemclass` SET `itemname`='Outgoing Local Call non-1C2N' WHERE `acode`='MO_LOCAL';
UPDATE `as2in1bill`.`itemclass` SET `itemname`='Outgoing IDD Call non-1C2N' WHERE `acode`='MO_IDD';
UPDATE `as2in1bill`.`itemclass` SET `itemname`='Outgoing IDD Airtime non-1C2N' WHERE `acode`='MO_IDD_AIR_VCE';
INSERT INTO `as2in1bill`.`itemclass` (`acode`, `itemname`, `groupname`) VALUES ('MO_ROAM_HK_VCE', 'Outgoing Roaming Call HK non-1C2N', 'Voice');

UPDATE `as2in1bill`.`itemclass` SET `itemname`='Outgoing Roaming Call ID non-1C2N' WHERE `acode`='MO_ROAM_LOCAL_VCE';

INSERT INTO `as2in1bill`.`itemclass` (`acode`, `itemname`, `groupname`) VALUES ('MO_ROAM_3RD_VCE', 'Outgoing Roaming Call ROW non-1C2N', 'Voice');

UPDATE `as2in1bill`.`itemclass` SET `itemname`='Hotline Call non-1C2N' WHERE `acode`='ADMIN';

UPDATE `as2in1bill`.`itemclass` SET `itemname`='Hotline Call 1C2N' WHERE `acode`='ADMIN_1C2N';

UPDATE `as2in1bill`.`itemclass` SET `itemname`='Incoming Call 1C2N' WHERE `acode`='MT_1C2N_LOCAL';

UPDATE `as2in1bill`.`itemclass` SET `itemname`='Outgoing Call 1C2N' WHERE `acode`='MO_1C2N_LOCAL';

UPDATE `as2in1bill`.`itemclass` SET `itemname`='Outgoing IDD Call 1C2N' WHERE `acode`='MO_1C2N_IDD';

UPDATE `as2in1bill`.`itemclass` SET `itemname`='Outgoing IDD Airtime 1C2N' WHERE `acode`='MO_1C2N_IDD_AIR_VCE';

INSERT INTO `as2in1bill`.`itemclass` (`acode`, `itemname`, `groupname`) VALUES ('ROAMING_MT', 'Incoming Roaming Call non-1C2N', 'Voice');

UPDATE `as2in1bill`.`itemclass` SET `itemname`='Outgoing Roaming Call HK 1C2N' WHERE `acode`='MO_1C2N_ROAM_HK_VCE';

UPDATE `as2in1bill`.`itemclass` SET `itemname`='Outgoing Roaming Call ROW 1C2N' WHERE `acode`='MO_1C2N_ROAM_3RD_VCE';

UPDATE `as2in1bill`.`itemclass` SET `itemname`='Outgoing Roaming Call ID 1C2N' WHERE `acode`='MO_1C2N_ROAM_LOCAL_VCE';

UPDATE `as2in1bill`.`itemclass` SET `itemname`='Incoming Call from TSEL in ID' WHERE `acode`='MO_1C2N_AT_INDO';

UPDATE `as2in1bill`.`itemclass` SET `itemname`='Incoming Call from TSEL in HK' WHERE `acode`='MT_1C2N_AT_HK';