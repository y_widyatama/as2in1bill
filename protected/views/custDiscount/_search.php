<?php
/* @var $this CustDiscountController */
/* @var $model CustDiscount */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'cust_id'); ?>
		<?php echo $form->textField($model,'cust_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'percentfactor'); ?>
		<?php echo $form->textField($model,'percentfactor'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'code'); ?>
		<?php echo $form->textField($model,'code',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'startperiod'); ?>
		<?php echo $form->textField($model,'startperiod'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'endperiod'); ?>
		<?php echo $form->textField($model,'endperiod'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->