<?php
/* @var $this TTrandetailController */
/* @var $data TTrandetail */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('custnumber')); ?>:</b>
	<?php echo CHtml::encode($data->custnumber); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('batch_id')); ?>:</b>
	<?php echo CHtml::encode($data->batch_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tcode')); ?>:</b>
	<?php echo CHtml::encode($data->tcode); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('charge_cent')); ?>:</b>
	<?php echo CHtml::encode($data->charge_cent); ?>
	<br />


</div>