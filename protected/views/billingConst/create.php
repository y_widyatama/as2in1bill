<?php
/* @var $this BillingConstController */
/* @var $model BillingConst */

$this->breadcrumbs=array(
	'Billing Consts'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List BillingConst', 'url'=>array('index')),
	array('label'=>'Manage BillingConst', 'url'=>array('admin')),
);
?>

<h1>Create BillingConst</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>