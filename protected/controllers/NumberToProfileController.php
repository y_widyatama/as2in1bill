<?php

class NumberToProfileController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('admin','viewPdf','view','viewInvoice','viewInvoicePdf',
				'viewInvoiceCallDetails','viewInvoiceCallDetailsPdf'),
				'roles'=>array('viewBilling'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','create','update'),
				'roles'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	public function actionViewInvoice($id,$batchId,$noDetail=0)
	{
        $this->pageTitle = 'Billing Statement';
		$this->layout='nomenu';
        $viewName = 'viewInvoice2';
        if ($noDetail) $withDetails = false; else $withDetails = true;
		$this->render($viewName,array(
			'model'=>$this->loadModel($id),
			'batchId'=>$batchId,
            'withDetails'=>$withDetails,
		));
	}
	public function actionViewInvoiceCallDetails($id,$batchId)
	{
		$this->layout='nomenu';
        $this->pageTitle = 'CHARGE DETAILS';
        $model = $this->loadModel($id);
        $this->pageTitle = 'Call Details for '.$id.' period '.$batchId;
		
		$this->render('viewInvoiceCallDetails',array(
			'model'=>$model,
			'batchId'=>$batchId,
		//	'voiceCdrs'=>$voiceCdrs,
		));
	}
	public function actionViewInvoiceCallDetailsPdf($id,$batchId)
	{
		$this->layout='nomenu';
        $this->pageTitle = 'CHARGE DETAILS';
        $model = $this->loadModel($id);
	    $mPDF1 = Yii::app()->ePdf->mpdf('', 'A4');
        $mPDF1->SetFooter("|Page {PAGENO}|");

		$mPDF1->WriteHTML(
		
		
		$this->render('viewInvoiceCallDetails',array(
			'model'=>$model,
			'batchId'=>$batchId,
		//	'voiceCdrs'=>$voiceCdrs,
		),true));
		$mPDF1->Output();
	}
	public function actionViewPdf($id)
	{
		$mPDF1 = Yii::app()->ePdf->mpdf();
 
        # You can easily override default constructor's params
        $mPDF1 = Yii::app()->ePdf->mpdf('', 'A4');
		$mPDF1->WriteHTML(
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		),true));
		$mPDF1->Output();
	}
	public function actionViewInvoicePdf($id,$batchId,$noDetail=0)
	{
		$this->layout='nomenu';
		$mPDF1 = Yii::app()->ePdf->mpdf();
		/*$html2pdf = Yii::app()->ePdf->HTML2PDF();
        $html2pdf->WriteHTML($this->renderPartial('viewInvoice',array(
			'model'=>$this->loadModel($id),
			'batchId'=>$batchId,
		), true));
        $html2pdf->Output();*/
		$viewName = 'viewInvoice2';
        $withDetails = !$noDetail;
        //if ($noDetail) $viewName = 'viewInvoice';
        # You can easily override default constructor's params
        $this->pageTitle = 'Billing Statement';
        $mPDF1 = Yii::app()->ePdf->mpdf('', 'A4');
        $mPDF1->SetFooter("|Page {PAGENO}|");
		$mPDF1->WriteHTML(
		$this->render($viewName,array(
			'model'=>$this->loadModel($id),
			'batchId'=>$batchId,
            'withDetails'=>$withDetails,
		),true));
		$mPDF1->Output();	
	}
	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new NumberToProfile;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['NumberToProfile']))
		{
			$model->attributes=$_POST['NumberToProfile'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->custnumber));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['NumberToProfile']))
		{
			$model->attributes=$_POST['NumberToProfile'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->custnumber));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('NumberToProfile');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new NumberToProfile('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['NumberToProfile']))
			$model->attributes=$_GET['NumberToProfile'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return NumberToProfile the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		try
		{
			$model=NumberToProfile::model()->findByPk($id);
			if($model===null)
				throw new CHttpException(404,'The requested page does not exist.');
		}
		catch(CDbException $e)
		{
			throw new CHttpException(404,'The requested page does not exist.');
		}
		return $model;	
	}

	/**
	 * Performs the AJAX validation.
	 * @param NumberToProfile $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='number-to-profile-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
