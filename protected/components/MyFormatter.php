<?php
/**
 * Created by JetBrains PhpStorm.
 * User: yudhi
 * Date: 7/7/13
 * Time: 1:43 PM
 * To change this template use File | Settings | File Templates.
 */
class MyFormatter extends CFormatter
{
    public $currencyPrefix;
    public function __construct()
    {
        $this->currencyPrefix = "HK$";
        $nf =$this->numberFormat;
        $nf['decimals']=2;
        $this->numberFormat = $nf;

    }

    public function formatCurrency($v)
    {
        return $this->currencyPrefix . parent::formatNumber($v);
    }
    public function formatCent($v)
    {
        return parent::formatNumber($v * 0.01);
    }
}
