<?php
/* @var $this TrandetailCdrController */
/* @var $model TrandetailCdr */

$this->breadcrumbs=array(
	'Trandetail Cdrs'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List TrandetailCdr', 'url'=>array('index')),
	array('label'=>'Create TrandetailCdr', 'url'=>array('create')),
	array('label'=>'View TrandetailCdr', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage TrandetailCdr', 'url'=>array('admin')),
);
?>

<h1>Update TrandetailCdr <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>