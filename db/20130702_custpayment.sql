CREATE TABLE `cust_payment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `custnumber` varchar(45) DEFAULT NULL,
  `billing_id` int(11) DEFAULT NULL,
  `batch_id` int(11) DEFAULT NULL,
  `payamount` int(11) DEFAULT NULL,
  `paydate` datetime DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_bill_nbr` (`custnumber`,`billing_id`)
) ENGINE=InnoDB AUTO_INCREMENT=718 DEFAULT CHARSET=latin1;

CREATE TABLE `cust_surcharge` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) DEFAULT NULL,
  `startperiod` int(11) DEFAULT NULL,
  `monthcount` int(11) DEFAULT NULL,
  `amount` decimal(9,2) DEFAULT NULL,
  `permonth` decimal(9,2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_sur_cust` (`customer_id`),
  CONSTRAINT `fk_sur_cust` FOREIGN KEY (`customer_id`) REFERENCES `cust_profile` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

CREATE TABLE `t_surcharge` (
  `custnumber` varchar(45) DEFAULT NULL,
  `acode` varchar(45) DEFAULT NULL,
  `batch_id` int(11) DEFAULT NULL,
  `charge_cent` decimal(32,0) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `surcharge_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_surcharge` (`surcharge_id`),
  CONSTRAINT `fk_surcharge` FOREIGN KEY (`surcharge_id`) REFERENCES `cust_surcharge` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

ALTER TABLE `as2in1bill`.`cust_bills` 
ADD INDEX `uniq_bill_id` (`billing_id` ASC) ;

ALTER TABLE `as2in1bill`.`cust_bills` 
ADD UNIQUE INDEX `uniq_cust_id_bill` (`custnumber` ASC, `billing_id` ASC) ;

ALTER TABLE `as2in1bill`.`cust_payment` 

  ADD CONSTRAINT `fk_cust`

  FOREIGN KEY (`customer_id` )

  REFERENCES `as2in1bill`.`cust_profile` (`id` )

  ON DELETE NO ACTION

  ON UPDATE NO ACTION, 

  ADD CONSTRAINT `fk_num`

  FOREIGN KEY (`custnumber` )

  REFERENCES `as2in1bill`.`numbertoprofile` (`custnumber` )

  ON DELETE NO ACTION

  ON UPDATE NO ACTION

, ADD INDEX `fk_cust` (`customer_id` ASC) 

, ADD INDEX `fk_num` (`custnumber` ASC) ;

ALTER TABLE `as2in1bill`.`cust_bills_2` ADD COLUMN `chargeamount` INT NULL   ;

