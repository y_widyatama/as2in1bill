<?php
/* @var $this CustBillsController */
/* @var $model CustBills */

$this->breadcrumbs=array(
	'Cust Bills'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List CustBills', 'url'=>array('index')),
	array('label'=>'Create CustBills', 'url'=>array('create')),
	array('label'=>'View CustBills', 'url'=>array('view', 'id'=>
	$model->id)),
	array('label'=>'Manage CustBills', 'url'=>array('admin')),
);
?>

<h1>Update CustBills <?php echo  $model->id ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>