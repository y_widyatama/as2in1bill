CREATE TABLE `itemtext` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `itemname` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
ALTER TABLE `as2in1bill`.`t_trandetail` 
ADD COLUMN `itemtext_id` INT NULL AFTER `discount_flag`;

CREATE TABLE `trandetail_adj` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `batch_id` int(11) DEFAULT NULL,
  `custnumber` varchar(45) DEFAULT NULL,
  `tcode` varchar(45) DEFAULT NULL,
  `charge` int(11) DEFAULT NULL,
  `tstamp` datetime DEFAULT NULL,
  `ttype` int(11) DEFAULT NULL,
  `cust_id` int(11) DEFAULT NULL,
  `itemtext_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
ALTER TABLE `as2in1bill`.`trandetail_adj` 
ADD INDEX `fk_trandetail_adj_custnbr_idx` (`custnumber` ASC);
ALTER TABLE `as2in1bill`.`trandetail_adj` 
ADD CONSTRAINT `fk_trandetail_adj_custnbr`
  FOREIGN KEY (`custnumber`)
  REFERENCES `as2in1bill`.`numbertoprofile` (`custnumber`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;
