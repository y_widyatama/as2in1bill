<?php
$this->breadcrumbs=array(
	'Cust Discounts'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List CustDiscount', 'url'=>array('index')),
	array('label'=>'Manage CustDiscount', 'url'=>array('admin')),
);
?>

<h1>Create CustDiscount</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>