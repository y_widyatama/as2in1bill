<?php
/* @var $this TrandetailAdjController */
/* @var $model TrandetailAdj */

$this->breadcrumbs=array(
	'Trandetail Adjs'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List TrandetailAdj', 'url'=>array('index')),
	array('label'=>'Create TrandetailAdj', 'url'=>array('create')),
	array('label'=>'Update TrandetailAdj', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete TrandetailAdj', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage TrandetailAdj', 'url'=>array('admin')),
);
?>

<h1>View TrandetailAdj #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'batch_id',
		'custnumber',
		'tcode',
		'charge',
		'tstamp',
		'ttype',
		'cust_id',
		'itemtext_id',
		'discount_flag',
	),
)); ?>
