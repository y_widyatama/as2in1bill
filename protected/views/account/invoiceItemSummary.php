<?php
/* @var $this NumberToProfileController */
/* @var $model NumberToProfile */
?>
<?php
$pmodel = new PeriodForm();
$pmodel->batch_id = $batchId;
$this->widget('zii.widgets.CDetailView', array(
    'data'=>$model,
    'attributes'=>array(
        'custnumber:text:Phone number',
        'profile.corporatename:text:Company/Group',
        'profile.title',
        'profile.custname:text:Name',
        'profile.custaddr:text:Address',
        'profile.custemail:text:Email',
/*
        'custnumber:text:Phone number',
        'profile.title',
        'profile.custname:text:Name',
        'profile.custemail:text:Email',*/

    ),
)); ?>
<?php
$yr = floor($batchId / 100.0);
$mon = $batchId % 100;
$t = mktime(0,0,0,$mon,1,$yr);
//echo date('F Y',$t); ?>
<?php
$dpArr = $model->getInvoiceList($batchId);
$total = 0;
foreach ($dpArr as $rec)
    $total += $rec->charge;


?>
<?php

$dp = new CArrayDataProvider($dpArr,array(
    'id'=>'invlist',
    'pagination'=>array('pageSize'=>1000)));
?>

<?php //$this->widget('zii.widgets.grid.CGridView', array(
   $this->widget('ext.groupgridview.GroupGridView', array(
    'id'=>'tsms-grid',
    'dataProvider'=>$dp,
    'extraRowColumns' => array('groupname'),
    'extraRowPos' => 'above',
    //	'filter'=>$models,
    'columns'=>array(
        //array('name'=>'groupname','footer'=>'Total for this billing period','header'=>'Category'),
        array('name'=>'itemname','header'=>'Item','footer'=>'Total'),

        array(
            'name'=>'chargeStr',
            'header'=>'Charge',
            'footer'=>number_format($total,2),
            'footerHtmlOptions'=>array('style'=>'text-align: right; font-weight: bold;'),
            'cssClassExpression'=>'"rightalign"',
        ),
    ),
    'summaryText'=>'',
));

if ($total<100) {
    echo '<div><b>Minimum payment commitment : HK $'.$model->getMininumPayment().'</b></div><br>';
    $total = 100;
}
?>
