<?php
/**
 * Created by JetBrains PhpStorm.
 * User: yudhi
 * Date: 7/6/13
 * Time: 12:43 PM
 * To change this template use File | Settings | File Templates.
 */
?>
Please pay using one of these methods: <br/>

1. Cash/Octopus card : <br/>
<div style="margin-left: 16px">Please bring along the statement and settle the accounts at Plasa GraPARI, Portion 1A2 of Shop 1A, G/F, Lok Sing Centre, 19-31 Yee Wo Street & 2-8 Sugar Street, Causeway Bay, HK (Tel: 2890 1230) </div>

2. Bank transfer :
<div style="margin-left: 16px">
Please transfer the amount due to HSBC (Hong Kong) A/C No. 015-666209-838<br/>
Beneficiary: Telekomunikasi Indonesia International (HongKong) Limited<br/>
Please email the bank slip with the invoice number to postpaid@telin.hk<br/>
or fax to 31023306 for our reference.<br/>
</div>
3. Mail:
<div style="margin-left: 16px">
Mail a crossed check payable to "Telekomunikasi Indonesia International (HongKong) Limited" to Suite 905 9/F Ocean Centre, 5 Canton Road, Tsim Sha Tsui, Kowloon<br/>
Write your Invoice No. on the back of the check<br/>
Post-dated check will not be accepted<br/>
No receipt will be issued<br/>
</div>


Please pay before the payment due date shown on the bill to avoid disconnection of services. If you have any disputes or queries on your statement, please contact 63755902.
