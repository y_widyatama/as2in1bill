<?php

/**
 * This is the model class for table "cust_contract".
 *
 * The followings are the available columns in table 'cust_contract':
 * @property integer $id
 * @property integer $startperiod
 * @property integer $endperiod
 * @property integer $minimumpayment
 * @property integer $cust_id
 *
 * The followings are the available model relations:
 * @property CustProfile $cust
 */
class CustContract extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CustContract the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'cust_contract';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('startperiod, endperiod, minimumpayment, cust_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, startperiod, endperiod, minimumpayment, cust_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'cust' => array(self::BELONGS_TO, 'CustProfile', 'cust_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'startperiod' => 'Startperiod',
			'endperiod' => 'Endperiod',
			'minimumpayment' => 'Minimumpayment',
			'cust_id' => 'Cust',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('startperiod',$this->startperiod,true);
		$criteria->compare('endperiod',$this->endperiod,true);
		$criteria->compare('minimumpayment',$this->minimumpayment);
		$criteria->compare('cust_id',$this->cust_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    public static function getPeriodList(){
        $startMonth = 'September 2012';
        $endPeriod = date('Ym',strtotime('+6 month'));
        $listPeriods = array();
        $i = 0;
        do{
            $periodStr = $startMonth.' '.$i++.' month';
            $period = date('Ym', strtotime($periodStr));
            $periodLabel = date('Y F',strtotime($periodStr));
            $listPeriods[intval($period)] = $periodLabel;
        }while($period <= $endPeriod);
        $listPeriods[999912] = '9999 December';
        return $listPeriods;
    }

}