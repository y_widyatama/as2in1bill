<?php
/* @var $this ReportController */

$this->breadcrumbs=array(
	'Report'=>array('/report'),
	'ReportBillSummary',
);
$this->menu=array(
	array('label'=>'Bill Summary', 'url'=>array('reportBillSummary')),
	array('label'=>'Invoice Items', 'url'=>array('reportInvoiceItems')),
	array('label'=>'Invoice CDR Details', 'url'=>array('reportInvoiceDetail')),
	
);

?>
<h1><?php echo $this->id . '/' . $this->action->id; ?></h1>

Select billing period
<?php

$pmodel = new PeriodForm();
if (isset($_REQUEST['PeriodForm']))
	$pmodel->attributes = $_REQUEST['PeriodForm'];

$this->renderPartial('/periodform',array('model'=>$pmodel,'actionUrl'=>array(''),'method'=>'post','autosubmit'=>false,'withAll'=>true));
?>
